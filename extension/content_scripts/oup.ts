import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('widget-ArticleLevelMetrics', injectPlaudit);
}

function injectPlaudit(altMetricWidget: HTMLElement) {
  const container = document.createElement('div');
  container.classList.add('widget');
  const title = document.createElement('h3');
  title.textContent = 'Endorsements';
  title.classList.add('contents-title');
  container.appendChild(title);

  const widget = generateWidget();
  container.appendChild(widget);

  altMetricWidget.insertAdjacentElement('beforebegin', container);
}
