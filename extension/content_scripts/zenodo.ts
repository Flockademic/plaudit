import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('col-right', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const container = document.createElement('div');
  container.classList.add('well');
  const widget = generateWidget();
  container.appendChild(widget);

  sidebar.insertAdjacentElement('afterbegin', container);
}
