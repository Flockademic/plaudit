import { generateWidget } from './generateWidget';
import { detectElementById } from './detectElement';

export function inject() {
  detectElementById('left-column', injectPlaudit);
}

function injectPlaudit() {
  const widget = generateWidget();

  const altMetrics = document.getElementById('counts-wrapper');
  if (altMetrics) {
    altMetrics.insertAdjacentElement('beforebegin', widget);
  }
}
