import { generateWidget } from './generateWidget';
import { detectElementByClassName, detectElementById } from './detectElement';

export function inject() {
  detectElementByClassName('c-page-layout__side', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const widgetContainer = document.createElement('div');
  widgetContainer.classList.add('c-separator');
  const heading = document.createElement('h3');
  heading.textContent = 'Endorsements';
  heading.classList.add('p-section-title');
  widgetContainer.appendChild(heading);

  const downloadButtons = sidebar.getElementsByClassName('c-separator')[0];
  downloadButtons.insertAdjacentElement('afterend', widgetContainer);
  widgetContainer.appendChild(widget);
}
