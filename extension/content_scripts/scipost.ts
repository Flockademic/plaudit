import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('card', injectPlaudit);
}

function injectPlaudit(_firstCard: HTMLElement) {
  const widget = generateWidget();
  const card = document.createElement('div');
  card.classList.add('card');

  const cardHeader = document.createElement('div');
  cardHeader.classList.add('card-header');
  cardHeader.textContent = 'Endorsements';
  card.appendChild(cardHeader);

  const cardBody = document.createElement('div');
  cardBody.classList.add('card-body');
  card.appendChild(cardBody);
  cardBody.appendChild(widget);

  const existingCards = document.getElementsByClassName('card');
  existingCards[existingCards.length - 1].insertAdjacentElement('afterend', card);
}
