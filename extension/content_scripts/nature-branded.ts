import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('c-article-header', injectPlaudit);
}

function injectPlaudit(articleHeader: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  // This is the margin Nature seems to apply to other elements in this header:
  container.style.marginTop = '16px';
  container.appendChild(widget);

  const headerElements = articleHeader.getElementsByTagName('header');
  if (headerElements.length > 0) {
    headerElements[0].appendChild(container);
  }
}
