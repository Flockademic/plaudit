import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('article__sections', injectPlaudit);
}

function injectPlaudit(articleBlock: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  // This is the same margin as the header's:
  container.style.marginTop = '30px';
  container.appendChild(widget);

  articleBlock.insertAdjacentElement('afterbegin', container);
}
