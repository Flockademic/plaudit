let initialised = false;

export function detectElementById(
  id: string,
  onIsPresent: (element: HTMLElement) => void,
): void {
  // Check whether the document has already loaded before this function is called:
  const element = document.getElementById(id);
  if (element !== null) {
    // If the document has already loaded, run the callback:
    onIsPresent(element);
  } else {
    // Otherwise, wait until the document has loaded:
    window.addEventListener('load', onLoad, false);
  }

  function onLoad() {
    if (initialised) {
      return;
    }
    initialised = true;
    const body = document.body;
    const element = document.getElementById(id);

    if (element !== null) {
      onIsPresent(element);
    } else {
      const observer = new MutationObserver(setupCallback);
      observer.observe(body, { childList: true, subtree: true })
    }
  }

  function setupCallback(mutationsList: MutationRecord[], observer: MutationObserver) {
    const elementMutation = mutationsList.find((mutation) => (mutation.target as HTMLElement).id === id);

    if (typeof elementMutation !== 'undefined') {
      onIsPresent(elementMutation.target as HTMLElement);
      observer.disconnect();
    }
  }
}

export function detectElementByClassName(
  className: string,
  onIsPresent: (element: HTMLElement) => void,
): void {
  // Check whether the document has already loaded before this function is called:
  const elements = document.getElementsByClassName(className);
  if (elements.length > 0) {
    // If the document has already loaded, run the callback:
    onIsPresent(elements.item(0) as HTMLElement);
  } else {
    // Otherwise, wait until the document has loaded:
    window.addEventListener('load', onLoad, false);
  }

  function onLoad() {
    if (initialised) {
      return;
    }
    initialised = true;
    const body = document.body;
    const elements = document.getElementsByClassName(className);

    if (elements.length > 0) {
      onIsPresent(elements.item(0) as HTMLElement);
    } else {
      const observer = new MutationObserver(setupCallback);
      observer.observe(body, { childList: true, subtree: true })
    }
  }

  function setupCallback(mutationsList: MutationRecord[], observer: MutationObserver) {
    const elementMutation = mutationsList.find((mutation) => (mutation.target as HTMLElement).classList.contains(className));

    if (typeof elementMutation !== 'undefined') {
      onIsPresent(elementMutation.target as HTMLElement);
      observer.disconnect();
    }
  }
}
