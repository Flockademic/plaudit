import { generateWidget } from './generateWidget';
import { detectElementById, detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('article-stats', injectPlaudit);
}

function injectPlaudit(stats: HTMLElement) {
  const container = document.createElement('div');
  container.className = 'article-credits-block';
  const widget = generateWidget();
  container.appendChild(widget);

  stats.insertAdjacentElement('beforebegin', container);
}
