import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('right-container', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const widgetContainer = document.createElement('div');
  widgetContainer.classList.add('side-article-impact');
  const heading = document.createElement('h5');
  heading.textContent = 'Endorsements';
  heading.classList.add('like-h4');
  widgetContainer.appendChild(heading);

  const altMetrics = sidebar.getElementsByClassName('side-article-impact')[0];
  altMetrics.insertAdjacentElement('beforebegin', widgetContainer);
  widgetContainer.appendChild(widget);
}
