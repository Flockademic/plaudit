import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('RelatedContent', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const panel = document.createElement('section');
  panel.classList.add('SidePanel');
  const heading = document.createElement('header');
  heading.classList.add('side-panel-header');
  const title = document.createElement('h2');
  title.textContent = 'Endorsements';
  title.classList.add('section-title', 'u-h4');
  heading.appendChild(title);
  panel.appendChild(heading);

  const container = document.createElement('div');
  container.classList.add('SidePanelItem');
  const widget = generateWidget();
  container.appendChild(widget);
  panel.appendChild(container);

  const panels = sidebar.getElementsByClassName('SidePanel');
  const metricsPanel = panels[panels.length - 1];
  if (metricsPanel) {
    metricsPanel.insertAdjacentElement('beforebegin', panel);
  }
}
