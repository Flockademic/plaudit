import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('side-content', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const widgetContainer = document.createElement('div');
  widgetContainer.style.paddingTop = '20px';

  sidebar.insertAdjacentElement('afterbegin', widgetContainer);
  widgetContainer.appendChild(widget);
}
