import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('box-paper-statics', injectPlaudit);
}

function injectPlaudit(statisticsBox: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  container.classList.add('row');
  container.appendChild(widget);

  statisticsBox.insertAdjacentElement('afterend', container);
}
