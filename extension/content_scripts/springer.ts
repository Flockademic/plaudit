import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('main-context__container', injectPlaudit);
}

function injectPlaudit(infobar: HTMLElement) {
  const container = document.createElement('div');
  container.classList.add('main-context__column');
  container.style.flexGrow = '1';
  const widget = generateWidget();
  container.appendChild(widget);

  infobar.insertAdjacentElement('beforeend', container);
}
