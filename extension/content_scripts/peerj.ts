import { generateWidget } from './generateWidget';
import { detectElementById, detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('article-meta', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const container = document.createElement('div');
  container.style.marginTop = '20px';
  const widget = generateWidget();
  container.appendChild(widget);

  const articleInfo = document.getElementById('article-item-article-information-container');
  if (articleInfo) {
    articleInfo.insertAdjacentElement('beforebegin', container);
  }
}
