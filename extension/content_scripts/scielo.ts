import { generateWidget } from './generateWidget';
import { detectElementByClassName, detectElementById } from './detectElement';

export function inject() {
  detectElementById('toolBox', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();

  sidebar.insertAdjacentElement('beforeend', widget);
}
