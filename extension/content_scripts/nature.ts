import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('reading-companion', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  container.classList.add('grid', 'grid-12', 'mb10', 'clear', 'cleared');
  const heading = document.createElement('h1');
  heading.textContent = 'Endorsements';
  heading.classList.add('h3', 'tighten-line-height', 'small-space-below');
  container.appendChild(heading);
  container.appendChild(widget);

  const floatingToc = sidebar.querySelector('[data-component="reading-companion-placeholder"]');
  if (floatingToc) {
    floatingToc.insertAdjacentElement('beforebegin', container);
  }
}
