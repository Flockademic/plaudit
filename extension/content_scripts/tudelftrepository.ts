import { generateWidget } from './generateWidget';
import { detectElementById } from './detectElement';

export function inject() {
  // We add a container to the sidebar that will hold the widget.
  detectElementById('sidebar-first', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  container.appendChild(widget);

  sidebar.insertAdjacentElement('afterbegin', container);
}
