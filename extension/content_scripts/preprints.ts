import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('manuscript-bookmark', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const hr = document.createElement('hr');

  const container = document.createElement('div');
  const widget = generateWidget();
  container.appendChild(widget);

  sidebar.insertAdjacentElement('beforebegin', hr);
  hr.insertAdjacentElement('beforebegin', container);
}
