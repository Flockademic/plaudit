import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('page-article', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  // The page includes the content twice: once for tablets and above, and once for mobile.
  const metricsBox = sidebar.querySelector(".is-hidden-mobile .metrics-area");
  const container = document.createElement('div');
  container.classList.add('mt-5-d');
  const heading = document.createElement('h4');
  heading.classList.add('pb-5', 'caps')
  heading.textContent = 'Endorsements';
  container.appendChild(heading);
  const widget = generateWidget();
  container.appendChild(widget);

  metricsBox?.insertAdjacentElement('beforebegin', container);
}
