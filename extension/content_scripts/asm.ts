import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('panel-region-sidebar-right', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const outerContainer = document.createElement('div');
  outerContainer.classList.add('panel-pane', 'pane-style-alt-content');
  const innerContainer = document.createElement('div');
  innerContainer.classList.add('pane-content');
  const widget = generateWidget();
  outerContainer.appendChild(innerContainer);
  innerContainer.appendChild(widget);

  const separator = document.createElement('div');
  separator.classList.add('panel-separator');

  const articleTools = sidebar.getElementsByClassName('pane-jnl-asm-art-tools')[0];
  articleTools.insertAdjacentElement('afterend', outerContainer);
  articleTools.insertAdjacentElement('afterend', separator);
}
