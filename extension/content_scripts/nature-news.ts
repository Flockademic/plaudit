import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('article__aside', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  container.classList.add('hide-print');
  container.appendChild(widget);

  const relatedSubjectsElements = sidebar.getElementsByClassName('article__subjects');
  if (relatedSubjectsElements.length > 0) {
    relatedSubjectsElements[0].insertAdjacentElement('afterend', container);
  }
}
