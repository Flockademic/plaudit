import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('ojLayout3col-2Top', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const citeBox = sidebar.querySelector(".hcBasicSideMargin:last-child .columnContent:last-child");
  const container = document.createElement('div');
  container.classList.add('columnContent', 'hcMarginBottom2');
  const widget = generateWidget();
  container.appendChild(widget);

  citeBox?.insertAdjacentElement('beforebegin', container);
}
