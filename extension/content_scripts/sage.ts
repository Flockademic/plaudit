import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('article-toc-widget', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('li');
  container.appendChild(widget);

  sidebar.insertAdjacentElement('afterbegin', container);
}
