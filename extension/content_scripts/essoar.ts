import { generateWidget } from './generateWidget';
import { detectElementById } from './detectElement';

export function inject() {
  detectElementById('pane-pcw-details', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const separator = document.createElement('hr');
  separator.classList.add('section__separator');
  separator.appendChild(widget);

  const firstSeparator = sidebar.getElementsByClassName('section__separator')[0];
  firstSeparator.insertAdjacentElement('afterend', separator);
  separator.insertAdjacentElement('beforebegin', widget);
}
