import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('panel-region-sidebar-right', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  container.classList.add('panel-pane', 'pane-panels-mini', 'max-margin-bottom');
  container.appendChild(widget);
  const separator = document.createElement('div');
  separator.classList.add('panel-separator');

  const firstSeparator = sidebar.querySelector('.inside > .panel-separator');
  if (firstSeparator) {
    firstSeparator.insertAdjacentElement('afterend', separator);
    separator.insertAdjacentElement('beforebegin', container);
  }
}
