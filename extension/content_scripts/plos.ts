import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('aside-container', injectPlaudit);
}

function injectPlaudit(downloadLinks: HTMLElement) {
  const widget = generateWidget();
  const widgetContainer = document.createElement('div');

  downloadLinks.insertAdjacentElement('afterend', widgetContainer);
  widgetContainer.appendChild(widget);
}
