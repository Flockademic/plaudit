import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('info-tab-wrapper', injectPlaudit);
}

function injectPlaudit(sidebar: HTMLElement) {
  const section = document.createElement('section');
  const heading = document.createElement('p');
  heading.classList.add('info-header');
  heading.textContent = 'Endorsements';
  section.appendChild(heading);

  const container = document.createElement('div');
  container.classList.add('section__body');
  const widget = generateWidget();
  container.appendChild(widget);
  section.appendChild(container);

  sidebar.insertAdjacentElement('afterbegin', section);
}
