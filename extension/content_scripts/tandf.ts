import { generateWidget } from './generateWidget';
import { detectElementByClassName } from './detectElement';

export function inject() {
  detectElementByClassName('combinedRecommendationsWidget', injectPlaudit);
}

function injectPlaudit(recommendationsWidget: HTMLElement) {
  const widget = generateWidget();
  const container = document.createElement('div');
  // The .combinedRecommendationsWidget makes the heading receive the same styling as that element:
  container.classList.add('widget', 'widget-compact-vertical', 'combinedRecommendationsWidget');
  const heading = document.createElement('h1');
  heading.textContent = 'Endorsements';
  heading.classList.add('widget-header', 'header-compact-vertical');
  const containerBody = document.createElement('div');
  containerBody.classList.add('widget-body', 'body-compact-vertical');
  // Other sidebar widgets appear to have separate received this width:
  containerBody.style.width = '85.5%';
  containerBody.appendChild(widget);
  container.appendChild(heading);
  container.appendChild(containerBody);

  recommendationsWidget.insertAdjacentElement('beforebegin', container);
}
