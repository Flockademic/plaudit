// Unfortunately, the polyfill cannot be auto-injected with ProvidePlugin, so we manually import it:
// https://github.com/mozilla/webextension-polyfill/issues/156
import browser from 'webextension-polyfill';
import { getDois } from 'get-dois';
import { inject as injectToArxiv } from './arxiv';
import { inject as injectToEssoar } from './essoar';
import { inject as injectToScipost } from './scipost';
import { inject as injectToF1000research } from './f1000research';
import { inject as injectToNature } from './nature';
import { inject as injectToNatureNews } from './nature-news';
import { inject as injectToNatureBrandedJournal } from './nature-branded';
import { inject as injectToScienceDirect } from './sciencedirect';
import { inject as injectToWiley } from './wiley';
import { inject as injectToSpringer } from './springer';
import { inject as injectToTandF } from './tandf';
import { inject as injectToOUP } from './oup';
import { inject as injectToBiorXivAndMedRxiv } from './biorxiv';
import { inject as injectToPreprintsOrg } from './preprints';
import { inject as injectToZenodo } from './zenodo';
import { inject as injectToMdpi } from './mdpi';
import { inject as injectToPubMedCentral } from './pmc';
import { inject as injectToPnas } from './pnas';
import { inject as injectToFrontiers } from './frontiersin';
import { inject as injectToPlos } from './plos';
import { inject as injectToBmc } from './bmc';
import { inject as injectToAsm } from './asm';
import { inject as injectToPeerJ } from './peerj';
import { inject as injectToPubpub } from './pubpub';
import { inject as injectToCollabra } from './collabra';
import { inject as injectToCell } from './cell';
import { inject as injectToSsrn } from './ssrn';
import { inject as injectToScielo } from './scielo';
import { inject as injectToSage } from './sage';
import { inject as injectToOpenJournals } from './openjournals';
import { inject as injectToResearchSquare } from './researchsquare';
import { inject as injectToTuDelftRepository } from './tudelftrepository';
import { FoundDoisMessage, ProvideDoiMessage, isRequestDoiMessage, ExtensionMessage } from '../interfaces';

if (document.location !== null) {
  initialise();
}

const scieloDomains = [
  'scielo.org',
  'www.scielo.org',
  'www.scielo.org.ar',
  'www.scielo.org.bo',
  'scielo.org.bo',
  'www.scielo.br',
  'scielo.br',
  'scielo.conicyt.cl',
  'www.scielo.sa.cr',
  'scielo.sld.cu',
  'www.scielo.sld.cu',
  'www.scielo.org.mx',
  'scielo.org.mx',
  'scielo.iics.una.py',
  'www.scielo.org.pe',
  'www.scielo.mec.pt',
  'scielo.mec.pt',
  'www.scielosp.org',
  'scielosp.org',
  'www.scielo.org.za',
  'scielo.isciii.es',
  'www.scielo.edu.uy',
  'scielo.edu.uy',
  'scielo.senescyt.gob.ec',
  've.scielo.org',
  'westindies.scielo.org',
  'books.scielo.org',
  'cienciaecultura.bvs.br',
];

browser.runtime.onMessage.addListener<ExtensionMessage>((
  message,
  _sender,
  sendResponse,
) => {
  if (isRequestDoiMessage(message as ExtensionMessage)) {
    const foundDois = getDois(document);

    // Usually, we only want to show the popup when there's just *one* DOI on the page
    // (so as to prevent it from appearing on e.g. listings of multiple DOI's, where it's unclear
    // to which DOI it applies).
    // However, on the Open Science Framework, sometimes their own and the eventual publisher's DOIs
    // are listed - in which case we're fine with endorsing the OSF's DOI:
    // (This is also the reason that the embed Javascript is fine with more than one DOI.)
    if (foundDois.length === 1 || (document.location !== null && document.location.host === 'osf.io')) {
      const response: ProvideDoiMessage = {
        type: 'provideDoi',
        doi: foundDois[0],
        url: (document.location) ? document.location.toString() : undefined,
      };
      sendResponse(response);
    }
  }

  return false;
});

function initialise() {
  const foundDois = getDois(document);

  const message: FoundDoisMessage = {
    type: 'foundDois',
    dois: foundDois,
  };
  browser.runtime.sendMessage(message);
  const includedWidgets = document.querySelectorAll(`script[src="https://plaudit.pub/embed/endorsements.js"],script[src="${process.env.APP_URL}/embed/endorsements.js"]`);
  if (document.location === null || includedWidgets.length > 0 || foundDois.length === 0) {
    // Do nothing; the widget is already included in the current page.
    // This is a safeguard to stop adding the widget through the extension
    // once the platform itself incorporates Plaudit.
    // Alternatively, no DOI could be found on the page,
    // in which case the widget will not work either.
    return;
  } else if (document.location.protocol !== 'https:') {
    // Do nothing; we only inject on https websites.
    return;
  } else if (document.location.host === 'arxiv.org') {
    injectToArxiv(foundDois);
  } else if (document.location.host === 'www.essoar.org') {
    injectToEssoar();
  } else if (
    [
      // All these platforms are run by F1000 and have the same DOM structure and similar styles:
      'f1000research.com',
      'wellcomeopenresearch.org',
      'gatesopenresearch.org',
      'mniopenresearch.org',
      'hrbopenresearch.org',
      'aasopenresearch.org',
      'amrcopenresearch.org',
      'isfopenresearch.org',
    ].indexOf(document.location.host) !== -1
  ) {
    injectToF1000research();
  } else if (document.location.host === 'scipost.org') {
    injectToScipost();
  } else if (document.location.host === 'www.nature.com') {
    injectToNature();
    injectToNatureNews();
    injectToNatureBrandedJournal();
  } else if (document.location.host === 'www.sciencedirect.com') {
    injectToScienceDirect();
  } else if (
    // Wiley journals are on onlinelbrary.wiley.com, or one of its subdomains.
    // 'onlinelibrary.wiley.com'.length === 23:
    document.location.host.substring(document.location.host.length - 23) === 'onlinelibrary.wiley.com'
  ) {
    injectToWiley();
  } else if (document.location.host === 'link.springer.com') {
    injectToSpringer();
  } else if (document.location.host === 'www.tandfonline.com') {
    injectToTandF();
  } else if (document.location.host === 'academic.oup.com') {
    injectToOUP();
  } else if (
    document.location.host === 'www.biorxiv.org' ||
    // medRxiv has the same DOM structure as biorXiv, even using classes like `pane-biorxiv-art-tools`
    document.location.host === 'www.medrxiv.org'
  ) {
    injectToBiorXivAndMedRxiv();
  } else if (document.location.host === 'www.preprints.org') {
    injectToPreprintsOrg();
  } else if (document.location.host === 'www.zenodo.org' ||
    document.location.host === 'zenodo.org') {
    injectToZenodo();
  } else if (document.location.host === 'www.mdpi.com' || document.location.host === 'mdpi.com') {
    injectToMdpi();
  } else if (document.location.host === 'www.ncbi.nlm.nih.gov') {
    injectToPubMedCentral(foundDois);
  } else if (document.location.host === 'www.pnas.org') {
    injectToPnas();
  } else if (document.location.host === 'www.frontiersin.org') {
    injectToFrontiers();
  } else if (document.location.host === 'journals.plos.org') {
    injectToPlos();
  } else if (
    // BMC journals are on a subdomain of biomedcentral.com.
    // 'biomedcentral.com'.length === 17:
    document.location.host.substring(document.location.host.length - 17) === 'biomedcentral.com') {
    injectToBmc();
  } else if (
    // ASM journals are on a subdomain of asm.org.
    // 'asm.org'.length === 7:
    document.location.host.substring(document.location.host.length - 7) === 'asm.org') {
    injectToAsm();
  } else if (document.location.host === 'peerj.com') {
    injectToPeerJ();
  } else if (document.location.host.substring(document.location.host.length - 'pubpub.org'.length) === 'pubpub.org') {
    injectToPubpub();
  } else if (document.location.host === 'collabra.org' || document.location.host === 'www.collabra.org') {
    injectToCollabra();
  } else if (document.location.host === 'www.cell.com') {
    injectToCell();
  } else if (document.location.host === 'papers.ssrn.com') {
    injectToSsrn();
  } else if (document.location.host === 'journals.sagepub.com') {
    injectToSage();
  } else if (document.location.host === 'www.researchsquare.com') {
    injectToResearchSquare();
  } else if (document.location.host === 'openjournals.nl') {
    injectToOpenJournals();
  } else if (document.location.host === 'repository.tudelft.nl') {
    injectToTuDelftRepository();
  } else if (scieloDomains.indexOf(document.location.host) !== -1) {
    // Note that many of SciELO's domains are non-HTTPS, at the time of writing, and will then not work:
    injectToScielo();
  }
}
