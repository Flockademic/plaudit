const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    content_script: path.resolve(__dirname, 'content_scripts/index.ts'),
    background_script: path.resolve(__dirname, 'background_scripts/index.ts'),
    popup: path.resolve(__dirname, 'popup/popup.ts'),
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: "ts-loader" }
    ]
  },
  optimization: {
    minimize: false,
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      APP_URL: 'https://plaudit.pub',
    }),
  ],
  // Make sure that the output does not include eval()s (used for source maps),
  // as the WebExtension Content Security Policy forbids those:
  devtool: process.env.NODE_ENV !== 'production' ? 'hidden-source-map' : undefined,
};
