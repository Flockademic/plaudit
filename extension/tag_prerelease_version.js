if (!process.env.CI_JOB_ID || process.env.CI_COMMIT_REF_NAME === 'master') {
  console.error('No job ID found or trying to release a prelease version on `master`.');
  process.exit(1);
}

const fs = require('fs');
const semver = require('semver');
const manifestJson = require('./manifest.json');
const packageJson = require('./package.json');

const prereleaseTag = `build${process.env.CI_JOB_ID}`;
// Make sure the version is at least higher than the currently published version.
// This will not be problematic when we actually release that version, since we append `prereleaseTag`.
const prereleaseVersion = semver.inc(manifestJson.version, 'patch') + prereleaseTag;
manifestJson.version = prereleaseVersion;
packageJson.version = prereleaseVersion;

fs.writeFileSync('./manifest.json', JSON.stringify(manifestJson));
fs.writeFileSync('./package.json', JSON.stringify(packageJson));
