# [Unreleased]

# [0.1.9] - 2018-12-10

## Bugfixes

- The widget was not actually being inserted.

# [0.1.8] - 2018-12-10

## New features

- Display the widget on arXiv.org pages that list a DOI.
