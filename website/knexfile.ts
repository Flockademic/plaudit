import type { Knex } from "knex";

const config: Knex.Config = {
  client: 'pg',
  connection: process.env.DATABASE_URL ?? 'postgres://plaudit:plaudit@localhost:5432/plaudit',
  // Note that the default value of min is 2 only for historical reasons. It can
  // result in problems with stale connections, despite tarn's default idle
  // connection timeout of 30 seconds, which is only applied when there are more
  // than min active connections. It is recommended to set min: 0 so all idle
  // connections can be terminated.
  // https://knexjs.org/guide/#pool
  pool: { min: 0, max: 10 },
  debug: process.env.NODE_ENV === 'development',
  asyncStackTraces: process.env.NODE_ENV === 'development',
  migrations: {
    tableName: 'knex_migrations',
    extension: 'ts',
    directory: './src/db/knex_migrations/'
  },
};

export default config;
