// This file configures the initialization of Sentry on the server.
// The config you add here will be used whenever the server handles a request.
// https://docs.sentry.io/platforms/javascript/guides/nextjs/

import * as Sentry from "@sentry/nextjs";

Sentry.init({
  dsn: process.env.NEXT_PUBLIC_SENTRY_DSN,

  environment: process.env.NODE_ENV === "production" && process.env.RENDER_EXTERNAL_HOSTNAME !== "plaudit.onrender.com"
    ? process.env.NEXT_PUBLIC_BRANCH
    : process.env.NODE_ENV,

  release: process.env.NEXT_PUBLIC_RELEASE
    ? process.env.NEXT_PUBLIC_RELEASE
    : "local",

  // Adjust this value in production, or use tracesSampler for greater control
  tracesSampleRate: 1,

  // Setting this option to true will print useful information to the console while you're setting up Sentry.
  debug: false,

  ignoreErrors: [
    /Error: Fetching data from the CrossRef API failed for DOI \[(.*?)\]: \[404\] \[Not Found\]./
  ],
});
