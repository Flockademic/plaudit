import { Knex } from "knex";

declare global {
  var connection: Knex | undefined;
}

declare module 'knex/types/tables' {
  type OrcidId = `${number}-${number}-${number}-${number}${number | 'X'}`;
  type Doi = `10.${number}/${string}`;
  type Pid = `doi:${Doi}`;

  interface EndorsementRow {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    version: 1;
    userOrcid: OrcidId;
    pid: Pid;
    uuid: string;
    /**
     * plaudit: via the widget
     * website: via the website
     * scielo: via SciELO's API
     */
    source: 'widget' | 'website' | 'scielo';
  }

  interface TagRow {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    version: 1;
    tag: 'robust' | 'clear' | 'exciting';
  }

  interface EndorsementTagsRow {
    endorsementId: number;
    tagId: number;
  }

  interface WorkDataRow {
    pid: Pid;
    createdAt: Date;
    updatedAt: Date;
    version: 1;
    url: string;
    title: string;
  }

  interface RetractedEndorsementRow {
    id: number;
    source: 'admin' | 'undo' | 'retraction';
    createdAt: Date;
    updatedAt: Date;
    version: 1;
    pid: null;
    endorsementId: number;
    endorsementCreatedAt: Date;
    endorsementUpdatedAt: Date;
    endorsementVersion: 1;
    endorsementUserOrcid: OrcidId;
    endorsementPid: Pid;
    endorsementUuid: string;
    // TODO: Add to migration
    // endorsementSource: 'plaudit' | 'scielo';
  }

  interface UserRow {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    version: number;
    name?: string;
    email?: string;
  }

  interface OrcidAccountRow {
    orcid: OrcidId;
    userId: number;
    refreshToken: string;
    accessToken: string;
    expiresAt: number;
    createdAt: Date;
    updatedAt: Date;
    version: number;
  }

  interface CrossRefExportRow {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    version: number;
    exportedEndorsements: number;
    exportedUntilId?: number;
  }

  interface Tables {
    endorsement: Knex.CompositeTableType<
      EndorsementRow,
      // Insert type:
      Omit<EndorsementRow, "id" | "uuid" | "createdAt" | "updatedAt" | "source"> & Partial<Pick<EndorsementRow, "source">>,
      // Update type:
      Partial<Omit<EndorsementRow, "id" | "uuid" | "createdAt">> & Pick<EndorsementRow, "updatedAt" | "version">,
    >;

    tag: Knex.CompositeTableType<
      TagRow,
      // Insert type:
      Omit<TagRow, "id" | "createdAt" | "updatedAt">,
      // Update type:
      Partial<Omit<TagRow, "id" | "createdAt">> & Pick<TagRow, "updatedAt" | "version">,
    >;

    endorsement_tags_tag: EndorsementTagsRow;

    work_data: Knex.CompositeTableType<
      WorkDataRow,
      // Insert type:
      Omit<WorkDataRow, "createdAt" | "updatedAt">,
      // Update type:
      Partial<Omit<WorkDataRow, "pid" | "createdAt">> & Pick<WorkDataRow, "updatedAt" | "version">,
    >;

    retracted_endorsement: Knex.CompositeTableType<
      RetractedEndorsementRow,
      // Insert type:
      Omit<RetractedEndorsementRow, "id" | "pid" | "createdAt" | "updatedAt">,
      // Update type:
      Partial<Omit<RetractedEndorsementRow, "id" | "pid" | "createdAt">> & Pick<RetractedEndorsementRow, "updatedAt" | "version">,
    >;

    user: Knex.CompositeTableType<
      UserRow,
      // Insert type:
      Omit<UserRow, "id" | "createdAt" | "updatedAt" | "name" | "email"> & Partial<Pick<UserRow, "name" | "email">>,
      // Update type:
      Partial<Omit<UserRow, "id" | "createdAt">> & Pick<UserRow, "updatedAt" | "version">,
    >;

    orcid_account: Knex.CompositeTableType<
      OrcidAccountRow,
      // Insert type:
      Omit<OrcidAccountRow, "createdAt" | "updatedAt">,
      // Update type:
      Partial<Omit<OrcidAccountRow, "orcid" | "createdAt">> & Pick<OrcidAccountRow, "updatedAt" | "version">,
    >;

    cross_ref_export: Knex.CompositeTableType<
      CrossRefExportRow,
      // Insert type:
      Omit<CrossRefExportRow, "id" | "createdAt" | "updatedAt">,
      // Update type:
      Partial<Omit<CrossRefExportRow, "id" | "createdAt">> & Pick<CrossRefExportRow, "updatedAt" | "version">,
    >;
  }
}
