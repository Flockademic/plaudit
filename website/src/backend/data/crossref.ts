import { Doi, OrcidId, Pid } from "knex/types/tables";
import { FetchError } from "../../functions/fetchError";

interface ExportResult {
  sentEndorsements: Array<{
    id: number;
    pid: Pid;
    createdAt: Date;
    orcid: OrcidId;
    uuid: string;
  }>;
  crossrefIds: string[];
  error?: Error;
}

export async function sendEndorsementsToCrossRefEventData(
  endorsements: Array<{
    id: number;
    pid: Pid;
    createdAt: Date;
    orcid: OrcidId;
    uuid: string;
  }>,
): Promise<ExportResult> {
  // We send endorsements to CrossRef Event Data in sequence rather than in parallel, in order to
  // be able to recover gracefully from failed requests (i.e. to be able to resume the export later).
  const exportPromise = endorsements.reduce(
    async (promiseSoFar, endorsement) => {
      const exportSoFar: ExportResult = await promiseSoFar;
      const requestBody = convertEndorsementToCrossRefEvent(endorsement);

      try {
        const response = await fetch(process.env.CROSSREF_URL + "events", {
          method: "POST",
          body: JSON.stringify(requestBody),
          headers: new Headers({
            "Content-Type": "application/json",
            Authorization: `Bearer ${process.env.CROSSREF_CLIENT_SECRET}`,
          }),
        });
        if (!response.ok) {
          const errorMessage = `Error sending endorsement by [${endorsement.orcid}] of [${endorsement.pid}] ([${endorsement.createdAt.toISOString()}]) to CrossRef: [${response.status}] [${response.statusText}] [${await response.text()}].`;
          throw new Error(errorMessage);
        }
      } catch (e: any) {
        exportSoFar.error = e;
        throw exportSoFar;
      }

      exportSoFar.sentEndorsements.push(endorsement);
      exportSoFar.crossrefIds.push(requestBody.id);
      return exportSoFar;
    },
    Promise.resolve({ sentEndorsements: [], crossrefIds: [] } as ExportResult),
  );

  try {
    const exportResult = await exportPromise;

    return exportResult;
  } catch (e) {
    return e as ExportResult & { error: Error };
  }
}

export function hasError(
  response: ExportResult,
): response is ExportResult & { error: Error } {
  return response.error instanceof Error;
}

function convertEndorsementToCrossRefEvent(endorsement: {
  pid: Pid;
  createdAt: Date;
  orcid: OrcidId;
  uuid: string;
}) {
  const objectId =
    endorsement.pid.substr(0, "doi:".length) === "doi:"
      ? `https://doi.org/${endorsement.pid.substring("doi:".length)}`
      : endorsement.pid;

  const body = {
    license: "https://creativecommons.org/publicdomain/zero/1.0/",
    obj_id: objectId,
    source_token: process.env.CROSSREF_CLIENT_ID,
    occurred_at: endorsement.createdAt.toISOString(),
    subj_id: process.env.ORCID_URL + endorsement.orcid,
    id: endorsement.uuid,
    action: "add",
    source_id: "plaudit",
    relation_type_id: "recommends",
  };

  return body;
}

export type CrossRefWorkResponse = { status: unknown } | CrossRefWorkSuccessResponse;

/**
 * @see https://api.crossref.org/swagger-ui/index.html#/Works
 */
export type CrossRefWorkSuccessResponse = {
  status: "ok";
  "message-type": "work";
  "message-version": "1.0.0";
  message: {
    DOI: Doi;
    URL: string;
    resource: {
      primary: {
        URL: string;
      };
    };
    link: Array<{
      URL: string;
      "content-type": "application/pdf" | "text/html" | string;
    }>;
    title: string[];
    author: Array<{
      ORCID?: `http://orcid.org/${OrcidId}`;
      suffix?: string;
      given?: string;
      family: string;
      name: string;
      "authenticated-orcid": boolean;
      prefix?: string;
      sequence?: "first" | "additional";
    }>;
  };
};

export async function fetchCrossRefWorkMetadata(doi: Doi): Promise<CrossRefWorkSuccessResponse | Error> {
  // Note: I did not reuse process.env.CROSSREF_URL, because that is the URL to
  //       the _Event Data_ API, not the general API. And since we're only
  //       fetching data (i.e. not writing), we don't need to use a sandbox API
  //       anyway, so we can just hardcode the URL.
  const workApiUrl = new URL("https://api.crossref.org/v1/");
  workApiUrl.pathname = `works/${doi}`;
  try {
    const response = await fetch(workApiUrl.href);
    if (!response.ok) {
      throw new FetchError(
        `Fetching data from the CrossRef API failed for DOI [${doi}]: [${response.status}] [${response.statusText}].`,
        response
      );
    }
    const content: CrossRefWorkResponse = await response.json();
    if (!isCrossRefSuccessResponse(content)) {
      throw new Error(`Received invalid data from the CrossRef API for DOI [${doi}]: [${content}].`);
    }
    return content;
  } catch (e) {
    return e as Error;
  }
}

function isCrossRefSuccessResponse(response: CrossRefWorkResponse): response is CrossRefWorkSuccessResponse {
  return response.status === "ok";
}

export function normaliseCrossRefName(person: CrossRefWorkSuccessResponse["message"]["author"][number]): string {
  if (typeof person.name === "string" && person.name.length > 0) {
    return person.name;
  }

  if (person.given) {
    return `${person.given} ${person.family}`;
  }

  return person.family;
}
