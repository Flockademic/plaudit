import { Doi } from "knex/types/tables";
import { fetchCrossRefWorkMetadata, normaliseCrossRefName } from "./crossref";
import { FetchError, isFetchError } from "../../functions/fetchError";

// See https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html#date-fields
export type CiteProcDate =
  // [year, month, day of month]:
  | [[number, number, number]]
  // [start year, start month, day of start month], [end year, end month, day of end month]:
  | [[number, number, number], [number, number, number]];

interface SplitNameCiteProcPerson {
  family: string;
  given?: string;
  suffix?: string;
  "non-dropping-particle"?: string;
}

interface FlatNameCiteProcPerson {
  name: string;
}

// See https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html#name-fields
export type CiteProcPerson = {
  /**
   * The property `ORCID` is supported by at least CrossRef, but not mentioned in the CiteProc docs.
   * Even when supported, it might not always be included - not every publisher collects ORCID iDs.
   */
  ORCID?: string;
} & (SplitNameCiteProcPerson | FlatNameCiteProcPerson);
export interface CiteProcInstitution {
  literal: string;
}
export type CiteProcName = CiteProcPerson | CiteProcInstitution;
export function isCiteProcInstitution(
  person: CiteProcName,
): person is CiteProcInstitution {
  return typeof (person as CiteProcInstitution).literal !== "undefined";
}
export function isSplitNameCiteProcPerson(
  person: CiteProcPerson,
): person is CiteProcPerson & SplitNameCiteProcPerson {
  return typeof (person as SplitNameCiteProcPerson).family !== "undefined";
}
export function normaliseCiteProcName(person: CiteProcName): string {
  if (isCiteProcInstitution(person)) {
    return person.literal;
  }

  if (!isSplitNameCiteProcPerson(person)) {
    return person.name;
  }

  if (person.given) {
    return `${person.given} ${person.family}`;
  }

  return person.family;
}

/**
 * @param orcidId An ORCID ID, potentially formatted as a link
 * @returns The ORCID iD formatted as the naked ID, e.g. 0000-0001-7237-0797, or
 *          undefined if not an ORCID iD.
 */
export function normaliseOrcidId(orcidId?: string) {
  if (typeof orcidId === "undefined") {
    return undefined;
  }

  // Remove something like `https://orcid.org/` when present:
  orcidId = orcidId.replace(/^(https?:\/\/(www\.)?)?orcid.org\//, "");

  if (/^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X)$/.test(orcidId)) {
    return orcidId;
  }

  return undefined;
}

// See https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html#items
interface CiteProcItem {
  id: string;
  DOI: string;
  type: "article-journal" | "article" | "book" | "dataset" | string;
  title: string;
  author?: CiteProcName[];
  issued: { "date-parts": CiteProcDate };
  URL?: string;
  abstract?: string;
}

// See https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html
export type CslJson = CiteProcItem;

// See https://crosscite.org/docs.html
export async function fetchCiteProcMetadata(doi: Doi) {
  try {
    // Note: some example DOIs from different registration agencies can be found at
    //       https://www.doi.org/demos.html
    const response = await fetch(`https://doi.org/${doi}`, {
      headers: {
        // CrossRef, DataCite and mEDRA all supposedly support CSL-JSON, so that should cover most works:
        // https://crosscite.org/docs.html#sec-4
        Accept: "application/vnd.citationstyles.csl+json",
      },
    });

    if (!response.ok) {
      throw new FetchError(
        `Fetching data from the CiteProc API failed for DOI [${doi}]: [${response.status}] [${response.statusText}].`,
        response
      );
    }
    const data: CslJson = await response.json();

    return data;
  } catch (e) {
    return e as Error;
  }
}

export type DoiMetadata = {
  title: string;
  authors: Array<{ name: string; orcid?: string }>;
  url: string;
};

export async function fetchDoiMetadata(doi: Doi): Promise<DoiMetadata | Error> {
  const fetchedMetadata = await fetchCiteProcMetadata(doi);
  if (fetchedMetadata instanceof Error) {
    if (!isFetchError(fetchedMetadata) || fetchedMetadata.status !== 404) {
      // If we made some kind of parsing mistake, log it, but if it's just a
      // matter of no DOI metadata having been deposited, we can't help that:
      console.log(`Could not fetch DOI metadata for DOI [${doi}]. Error: [${fetchedMetadata.message}]. Trying again with the CrossRef API.`);
    }

    // Sometimes the CrossRef resolved for the DOI API returns an error, but
    // CrossRef's direct metadata API doesn't, e.g. for (at the time of writing)
    // https://doi.org/10.1038/s41562-022-01319-5
    const fetchedCrossRefMetadata = await fetchCrossRefWorkMetadata(doi);
    if (fetchedCrossRefMetadata instanceof Error) {
      (fetchCrossRefWorkMetadata as unknown as { follows: Error }).follows = fetchedMetadata;
      return fetchedCrossRefMetadata;
    }

    return {
      authors: (fetchedCrossRefMetadata.message.author || []).map((author) => ({
        name: normaliseCrossRefName(author),
        orcid: author.ORCID,
      })),
      title: fetchedCrossRefMetadata.message.title[0],
      url: `https://doi.org/${doi}`,
    };
  }
  return {
    authors: (fetchedMetadata.author || []).map((author) => ({
      name: normaliseCiteProcName(author),
      orcid: !isCiteProcInstitution(author) ? author.ORCID : undefined,
    })),
    title: fetchedMetadata.title,
    url: fetchedMetadata.URL || `https://doi.org/${doi}`,
  };
}
