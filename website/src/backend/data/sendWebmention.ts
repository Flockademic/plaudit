import { Doi, OrcidId } from "knex/types/tables";
import { webmention } from "webmen";

export async function sendWebmention(data: {
  identifierType: string;
  doi: Doi;
  orcid: OrcidId;
}) {
  try {
    const webmentionAccepted = await webmention(
      process.env.APP_URL + `/endorsements/${data.identifierType}:${data.doi}`,
      `https://doi.org/${data.doi}`,
    );
    console.log(
      webmentionAccepted
        ? `Successfully sent a WebMention of an endorsement by ${data.orcid} to https://doi.org/${data.doi}.`
        : `Tried to send a WebMention of the endorsement by ${data.orcid} to https://doi.org/${data.doi}, but it does not support receiving WebMentions.`,
    );
  } catch (e: unknown) {
    // Not sending a WebMention is less bad than not letting the user know
    // the endorsement was succesful, so just log the error and move on.
    console.log(
      `Could not send WebMention about endorsement of ${data.doi} by ${data.orcid}:`,
      e instanceof Error ? e.message : e,
    );
  }
}
