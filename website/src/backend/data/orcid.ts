let accessToken: string;

async function getAccessToken() {
  if (typeof accessToken === "string") {
    return accessToken;
  }

  const response = await fetch(process.env.ORCID_API_URL + "oauth/token", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body:
      `client_id=${process.env.ORCID_CLIENT_ID}` +
      `&client_secret=${process.env.ORCID_CLIENT_SECRET}` +
      `&grant_type=client_credentials` +
      `&scope=/read-public`,
  });
  const data: { access_token: string } = await response.json();

  accessToken = data.access_token;
  return accessToken;
}

export async function fetchFromOrcid(path: string, init?: RequestInit) {
  const accessToken = await getAccessToken();
  const url = process.env.ORCID_API_URL + path;
  const headers = new Headers(init?.headers);
  headers.set("Accept", "application/json");
  headers.set("Authorization", `Bearer ${accessToken}`);

  const requestInit = init ?? {};
  requestInit.headers = headers;

  return fetch(url, requestInit);
}
