import { Feed } from "feed";
import { NextRequest } from "next/server";
import { GetEndorserResponse } from "../endpoints";

const appUrl = process.env.APP_URL || "https://www.plaudit.pub";

export function stripRssExtensions(path: string): string {
  return path.replace(/\.(atom|rss)$/, "");
}

export function isRssRequest(req: NextRequest): boolean {
  const url = req.url;
  const acceptHeader = req.headers.get("Accept");
  return (
    (typeof acceptHeader === "string" &&
      acceptHeader === "application/rss+xml") ||
    (typeof url !== "undefined" && url.substring(url.length - 4) === ".rss")
  );
}

export function isAtomRequest(req: NextRequest): boolean {
  const url = req.url;
  const acceptHeader = req.headers.get("Accept");
  return (
    (typeof acceptHeader === "string" &&
      acceptHeader === "application/atom+xml") ||
    (typeof url !== "undefined" && url.substring(url.length - 5) === ".atom")
  );
}

export function endorserToFeed(response: GetEndorserResponse): Feed {
  const rssUrl = `${appUrl}/endorsers/${response.endorser.orcid}.rss`;
  const atomUrl = `${appUrl}/endorsers/${response.endorser.orcid}.atom`;
  const feed = new Feed({
    favicon: `https://plaudit.pub/favicon-32x32.png`,
    image: `https://plaudit.pub/apple-touch-icon-1024x1024.png`,
    generator: process.env.APP_URL,
    id: `plaudit-endorser-${response.endorser.orcid}`,
    title: `Endorsements by ${response.endorser.name || response.endorser.orcid}`,
    feed: atomUrl,
    feedLinks: {
      rss: rssUrl,
      atom: atomUrl,
    },
    author: {
      name: response.endorser.name || response.endorser.orcid,
      link: process.env.ORCID_URL + response.endorser.orcid,
    },
    copyright: response.endorser.name || response.endorser.orcid,
  });

  response.endorser.endorsements.forEach((endorsement) => {
    const description =
      `<p>${response.endorser.name || response.endorser.orcid} endorsed <em>${endorsement.title}</em>. <a href="${endorsement.url}" title="${endorsement.title}">View this article.</a></p>` +
      `<p><a href="${appUrl + "/endorser/" + response.endorser.orcid}" title="Other endorsements by ${response.endorser.name || response.endorser.orcid}">Other endorsements by ${response.endorser.name || response.endorser.orcid}.</a></p>`;
    feed.addItem({
      title: endorsement.title,
      id: `${endorsement.identifierType}:${endorsement.identifier}`,
      link: endorsement.url,
      date: new Date(endorsement.timestamp),
      description: description,
      content: description,
    });
  });

  return feed;
}
