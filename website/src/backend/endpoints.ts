import { Doi, OrcidId } from "knex/types/tables";

export type EndorsementType = "robust" | "exciting" | "clear";
export interface Endorsement {
  orcid: string;
  doi: string;
  tags: EndorsementType[];
}

export interface GetEndorsementsResponse {
  endorsements: Array<Endorsement & { userName?: string }>;
  workData: {
    title: string;
    url: string;
    authors?: Array<{
      name: string;
      orcid?: string;
    }>;
  };
  user?: {
    name?: string;
    orcid: OrcidId;
  };
}
export interface GetEndorsementsWithAuthorsResponse
  extends GetEndorsementsResponse {
  workData: {
    title: string;
    url: string;
    authors: Array<{
      name: string;
      orcid?: string;
    }>;
  };
}

export interface GetEndorsersResponse {
  endorsers: Array<{
    orcid: string;
    name?: string;
    endorsements: Array<{
      identifierType: "doi";
      identifier: string;
      createdAt: number;
      source: "widget" | "website" | "scielo";
    }>;
  }>;
}

export interface GetStatsResponse {
  total: number;
  nrOfEndorsers: number;
  nrOfEndorsedWorks: number;
  nrOfUndos: number;
  nrOfAdminRetractions: number;
  endorsersByMonth: Array<{
    month: string;
    nr: number;
  }>;
  endorsementsByMonth: Array<{
    month: string;
    nr: number;
  }>;
  endorsementsByPrefix: Array<{
    prefix: string;
    nr: number;
  }>;
}

export interface GetEndorserResponse {
  endorser: {
    orcid: string;
    name?: string;
    endorsements: Array<{
      identifierType: "doi";
      identifier: string;
      title: string;
      url: string;
      tags: EndorsementType[];
      /** ISO 8601 timestamp */
      timestamp: string;
    }>;
  };
}

export interface PostEndorsementRequest {
  tags: EndorsementType[];
}
export interface PostEndorsementResponse {
  endorsements: Array<Endorsement & { userName?: string }>;
  user: {
    name?: string;
    orcid: OrcidId;
  };
}

export interface PostEndorsementRetractionResponse {
  retraction: { pid: string; originalEndorsementTimestamp: string };
  endorsements: Array<Endorsement & { userName?: string }>;
}

export interface PostExternalEndorsementRequest {
  secret_key: string;
  doi: Doi;
  orcid: OrcidId;
}

export interface PostExternalEndorsementResponse {
  endorsements: Array<Omit<Endorsement, "tags">>;
}

export interface GetOrcidResponse {
  "activities-summary": {
    educations: {
      "education-summary": Array<{
        "role-title": string;
        organization: {
          name: string;
        };
        "start-date": {
          year: { value: string };
          month: { value: string };
          day: { value: string };
        };
        "end-date": null | {
          year: { value: string };
          month: { value: string };
          day: { value: string };
        };
      }>;
    };
    employments: {
      "employment-summary": Array<{
        "department-name"?: string;
        "role-title"?: string;
        organization: {
          name: string;
        };
        "start-date": {
          year: { value: string };
          month: { value: string };
          day: { value: string };
        };
        "end-date": null | {
          year: { value: string };
          month: { value: string };
          day: { value: string };
        };
      }>;
    };
    works: {
      group: Array<{
        "work-summary": [OrcidWorkSummary, OrcidWorkSummary?];
      }>;
    };
  };
  person: {
    biography: null | {
      content: null | string;
    };
    name: null | {
      "family-name": null | {
        value: string;
      };
      "given-names": {
        value: string;
      };
      path: string;
    };
    keywords: {
      keyword: Array<{
        content: string;
      }>;
    };
    "researcher-urls": {
      "researcher-url": Array<{
        "url-name": string;
        url: { value: string };
      }>;
    };
  };
}

interface OrcidWorkSummary {
  title: null | {
    title: { value: string };
    subtitle: null | { value: string };
  };
  "external-ids": null | {
    "external-id": Array<{
      "external-id-type": "doi" | string;
      "external-id-value": string;
      "external-id-url": null | { value: string };
    }>;
  };
  "publication-date": null | {
    year: { value: string };
    month: { value: string };
    day: { value: string };
  };
  "put-code": number;
}
