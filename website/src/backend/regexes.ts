import { Doi, OrcidId } from "knex/types/tables";

// A DOI is `10.`, followed by at least four digits, then a slash, and then any number of characters
export const doiRegex = /^10\.\d{4,}\/.*$/;
export const orcidRegex = /^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X)$/;

export function isValidDoi(doi: string | null): doi is Doi {
  return typeof doi === "string" && doiRegex.test(doi);
}

export function isValidOrcidId(orcidId: string | null): orcidId is OrcidId {
  return typeof orcidId === "string" && orcidRegex.test(orcidId);
}
