"use server";

import { getServerSession } from "next-auth";
import { Doi } from "knex/types/tables";
import { authOptions } from "../../app/api/auth/[...nextauth]/authOptions";
import { deleteEndorsement } from "../queries/deleteEndorsement";
import { isValidOrcidId } from "../regexes";

export type FormStatus = FormStatusError | FormStatusOk;

export type FormStatusError = { ok: false; errorCode: "ERR_UNAUTHENTICATED" | "ERR_DB" }

export type FormStatusOk = {
  ok: true;
};

export async function retractEndorsement(doi: Doi): Promise<FormStatus> {
  const identifierType = "doi";
  const session = await getServerSession(authOptions);

  if (!session?.user.id || !isValidOrcidId(session.user.id)) {
    const unauthError: FormStatusError = {
      ok: false,
      errorCode: "ERR_UNAUTHENTICATED",
    };
    return unauthError;
  }

  const pid = `${identifierType}:${doi}`;
  try {
    await deleteEndorsement(pid, session.user.id);
  } catch (e) {
    const db_err: FormStatusError = {
      ok: false,
      errorCode: "ERR_DB",
    };
    return db_err;
  }

  return {
    ok: true,
  };
}
