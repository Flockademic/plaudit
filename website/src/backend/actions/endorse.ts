"use server";

import { getServerSession } from "next-auth";
import { Doi, TagRow } from "knex/types/tables";
import { captureException } from "@sentry/nextjs";
import { authOptions } from "../../app/api/auth/[...nextauth]/authOptions";
import { getConnection } from "../database";
import { isValidOrcidId } from "../regexes";
import { sendWebmention } from "../data/sendWebmention";

export type FormStatus = null | FormStatusError | FormStatusOk;

export type FormStatusError = { ok: false; errorCode: "ERR_UNAUTHENTICATED" | "ERR_DB" }

export type FormStatusOk = {
  ok: true;
};

export async function endorse(doi: Doi, formStatus: FormStatus, formData: FormData): Promise<FormStatus> {
  const identifierType = "doi";
  const session = await getServerSession(authOptions);

  if (!session?.user.id || !isValidOrcidId(session.user.id)) {
    const unauthError: FormStatusError = {
      ok: false,
      errorCode: "ERR_UNAUTHENTICATED",
    };
    return unauthError;
  }

  const tags: TagRow["tag"][] = [];
  if (formData.get("tag_robust")) {
    tags.push("robust");
  }
  if (formData.get("tag_clear")) {
    tags.push("clear");
  }
  if (formData.get("tag_exciting")) {
    tags.push("exciting");
  }

  try {
    const connection = await getConnection();
    await connection.transaction(async (transaction) => {
      const savedEndorsement = await transaction("endorsement")
        .insert({
          pid: `${identifierType}:${doi}`,
          userOrcid: session.user.id,
          version: 1,
          source: 'website',
        })
        .onConflict(["pid", "userOrcid"])
        .ignore()
        .returning("id");
  
      const savedEndorsementId = savedEndorsement[0]?.id;
  
      if (tags.length > 0 && typeof savedEndorsementId === "number") {
        await transaction("tag")
          .insert(tags.map((tag) => ({ tag: tag, version: 1 })))
          .onConflict("tag")
          .ignore();
  
        const tagRows = await transaction("tag")
          .select("id")
          .whereIn("tag", tags);
  
        await transaction("endorsement_tags_tag").insert(
          tagRows.map((tagRow) => ({
            endorsementId: savedEndorsementId,
            tagId: tagRow.id,
          })),
        );
      }
    });
  } catch (e) {
    captureException(e);
    const db_err: FormStatusError = {
      ok: false,
      errorCode: "ERR_DB",
    };
    return db_err;
  }

  // Start sending a WebMention, but don't delay the response to do so,
  // i.e. no `await`:
  void sendWebmention({
    identifierType: identifierType,
    doi: doi,
    orcid: session.user.id,
  });

  return {
    ok: true,
  };
}
