import { Doi } from "knex/types/tables";
import {
  fetchDoiMetadata,
} from "../data/doi";
import { Endorsement } from "../endpoints";
import { getConnection } from "../database";
import { groupBy } from "../../functions/groupBy";

export type Result = {
  endorsements: Array<Endorsement & { userName?: string }>;
  workData: {
    title: string;
    url: string;
    authors?: Array<{
      name: string;
      orcid?: string;
    }>;
  };
};

export type Options = {
  withAuthors?: boolean;
};

export const getEndorsementsForDoi = (
  inputDoi: Doi,
  options: Partial<Options> = {},
): Promise<Result> => {
  const doi = inputDoi.toLowerCase() as Doi;
  return new Promise(async (resolve, reject) => {
    const connection = await getConnection();

    const workDataFromDb = await connection("work_data")
      .select([
        connection.ref("title").withSchema("work_data"),
        connection.ref("url").withSchema("work_data"),
        connection.ref("id").withSchema("endorsement").as("endorsementId"),
        connection.ref("orcid").withSchema("orcid_account"),
        connection.ref("name").withSchema("user").as("userName"),
        connection.ref("tag").withSchema("tag"),
      ])
      .leftJoin("endorsement", "work_data.pid", "endorsement.pid")
      .leftJoin("orcid_account", "endorsement.userOrcid", "orcid_account.orcid")
      .leftJoin("user", "orcid_account.userId", "user.id")
      .leftJoin(
        "endorsement_tags_tag",
        "endorsement.id",
        "endorsement_tags_tag.endorsementId",
      )
      .leftJoin("tag", "endorsement_tags_tag.tagId", "tag.id")
      .where("work_data.pid", `doi:${doi}`)
      .orderBy("endorsement.createdAt", "desc");

    const endorsementRows = groupBy(workDataFromDb, (row) => row.orcid);

    let doiMetadata: {
      title: string;
      authors?: Array<{ name: string; orcid?: string }>;
      url: string;
    };

    if (workDataFromDb.length === 0 || options.withAuthors) {
      const fetchedMetadata = await fetchDoiMetadata(doi);
      if (fetchedMetadata instanceof Error) {
        return reject(fetchedMetadata);
      }
      doiMetadata = fetchedMetadata;
    } else {
      doiMetadata = {
        title: workDataFromDb[0].title,
        url: workDataFromDb[0].url,
      };
    }

    const endorsements = Object.entries(endorsementRows).map(
      ([orcid, rows]) => {
        return {
          doi: doi,
          orcid: orcid,
          userName: rows[0].userName,
          tags: rows.map((row) => row.tag).filter((tag) => tag !== null),
        };
      },
    );

    const returnValue: Result = {
      endorsements,
      workData: doiMetadata,
    };
    // Return them as json
    resolve(returnValue);

    // Save this work's metadata to the database, if it wasn't present already
    if (workDataFromDb.length === 0) {
      await connection("work_data")
        .insert({
          pid: `doi:${doi}`,
          title: doiMetadata.title,
          url: doiMetadata.url,
          version: 1,
        })
        .onConflict("pid")
        .ignore();
    }
  });
};
