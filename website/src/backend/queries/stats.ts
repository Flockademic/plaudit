import { OrcidId, Pid } from "knex/types/tables";
import { EndorsementType } from "../endpoints";
import { getConnection } from "../database";
import { groupBy } from "../../functions/groupBy";

export type Result = {
  orcid: OrcidId;
  name?: string;
  endorsements: Array<{
    pid: Pid;
    title: string;
    url: string;
    tags: EndorsementType[];
    createdAt: Date;
  }>;
};

export const getEndorsersByMonth = async () => {
  const connection = await getConnection();

  const endorsersByMonthQueryResults = (await connection("endorsement")
    .select([
      connection.raw('COUNT(DISTINCT "userOrcid") as nr'),
      connection.raw(`date_trunc('month', endorsement."createdAt") as month`),
    ])
    .groupBy("month")
    .orderBy("month", "desc")) as any as Array<{ nr: string; month: string }>;

  const endorsersByMonth = endorsersByMonthQueryResults.map((rawRow) => ({
    nr: parseInt(rawRow.nr, 10),
    month: new Date(rawRow.month).toISOString(),
  }));

  return endorsersByMonth;
};

export const getEndorsementssByMonth = async () => {
  const connection = await getConnection();

  const endorsementsByMonthQueryResults = (await connection("endorsement")
    .select([
      connection.raw("COUNT(*) as nr"),
      connection.raw(`date_trunc('month', endorsement."createdAt") as month`),
    ])
    .groupBy("month")
    .orderBy("month", "desc")) as any as Array<{ nr: string; month: string }>;

  const endorsementsByMonth = endorsementsByMonthQueryResults.map((rawRow) => ({
    nr: parseInt(rawRow.nr, 10),
    month: new Date(rawRow.month).toISOString(),
  }));

  return endorsementsByMonth;
};

export const getNumberOfEndorsers = async () => {
  const connection = await getConnection();

  const nrOfEndorsersQueryResult = (await connection("endorsement").first(
    connection.raw('COUNT(DISTINCT endorsement."userOrcid") as nr'),
  )) as { nr: string };
  const nrOfEndorsers = parseInt(nrOfEndorsersQueryResult.nr, 10);

  return nrOfEndorsers;
};

export const getNumberOfEndorsedWorks = async () => {
  const connection = await getConnection();

  const nrOfEndorsedWorksQuery = (await connection("endorsement").first(
    connection.raw("COUNT(DISTINCT endorsement.pid) as nr"),
  )) as { nr: string };
  const nrOfEndorsedWorks = parseInt(nrOfEndorsedWorksQuery.nr, 10);

  return nrOfEndorsedWorks;
};

export const getNumberOfAdminRetractions = async () => {
  const connection = await getConnection();

  const nrOfAdminRetractionsQueryResult = await connection(
    "retracted_endorsement",
  )
    .count("*", { as: "nr" })
    .where("source", "admin");
  const nrOfAdminRetractions =
    typeof nrOfAdminRetractionsQueryResult[0].nr === "string"
      ? parseInt(nrOfAdminRetractionsQueryResult[0].nr, 10)
      : nrOfAdminRetractionsQueryResult[0].nr;

  return nrOfAdminRetractions;
};

export const getNumberOfUndos = async () => {
  const connection = await getConnection();

  const nrOfUndosQueryResult = await connection("retracted_endorsement")
    .count("*", { as: "nr" })
    .where("source", "undo");
  const nrOfUndos =
    typeof nrOfUndosQueryResult[0].nr === "string"
      ? parseInt(nrOfUndosQueryResult[0].nr, 10)
      : nrOfUndosQueryResult[0].nr;

  return nrOfUndos;
};
