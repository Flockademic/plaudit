import { Tables } from "knex/types/tables";
import { getConnection } from "../database";
import { getEndorsementsForDoi } from "./endorsementsForDoi";
import { isValidDoi } from "../regexes";

export async function deleteEndorsement(
  pid: string,
  orcid: string,
  overrideKey?: string,
) {
  const connection = await getConnection();

  const retraction = await connection.transaction(async (transaction) => {
    let query = transaction("endorsement")
      .first()
      .where("endorsement.pid", pid)
      .andWhere("endorsement.userOrcid", orcid);

    if (typeof overrideKey !== "string") {
      query = query.andWhereRaw(
        "endorsement.\"createdAt\" > NOW() - INTERVAL '10 minutes'",
      );
    } else if (
      typeof process.env.RETRACTION_ADMIN_KEY !== "string" ||
      process.env.RETRACTION_ADMIN_KEY.length < 36 ||
      overrideKey !== process.env.RETRACTION_ADMIN_KEY
    ) {
      throw new Error("Deleting endorsements is not allowed.");
    } else {
      // If this is an admin retracting an endorsement,
      // make sure that if we've already exported them to CrossRef,
      // and this endorsement was the last one in that batch,
      // that we point to the previous endorsement as the last in that batch:
      const exportsUntilThisEndorsement = await transaction("cross_ref_export")
        .select([
          connection.ref("id").withSchema("cross_ref_export").as("exportId"),
          connection.ref("id").withSchema("endorsement").as("endorsementId"),
        ])
        .leftJoin(
          "endorsement",
          "cross_ref_export.exportedUntilId",
          "endorsement.id",
        )
        .where("endorsement.pid", pid);

      if (exportsUntilThisEndorsement.length > 0) {
        const previousEndorsement = await transaction("endorsement")
          .first("id")
          .where("id", "<", exportsUntilThisEndorsement[0].endorsementId)
          .orderBy("id", "desc");

        if (typeof previousEndorsement !== "undefined") {
          await transaction("cross_ref_export")
            .update("exportedUntilId", previousEndorsement.id)
            .whereIn(
              "id",
              exportsUntilThisEndorsement.map(
                (exportData) => exportData.exportId,
              ),
            );
        }
      }
    }

    const foundEndorsementData = await query;
    if (typeof foundEndorsementData === "undefined") {
      throw new Error("Endorsement to retract not found.");
    }

    const retraction: Tables["retracted_endorsement"]["insert"] = {
      source: typeof overrideKey === "string" ? "admin" : "undo",
      version: 1,
      endorsementCreatedAt: foundEndorsementData.createdAt,
      endorsementUpdatedAt: foundEndorsementData.updatedAt,
      endorsementId: foundEndorsementData.id,
      endorsementPid: foundEndorsementData.pid,
      endorsementUserOrcid: foundEndorsementData.userOrcid,
      endorsementUuid: foundEndorsementData.uuid,
      endorsementVersion: foundEndorsementData.version,
      // TODO: Add this table column:
      // endorsementSource: foundEndorsementData.source,
    };
    await transaction("retracted_endorsement").insert(retraction);

    await transaction("endorsement").where("id", foundEndorsementData.id).del();

    return retraction;
  });

  const doi = pid.substring("doi:".length);
  const endorsements = isValidDoi(doi)
    ? await getEndorsementsForDoi(doi)
    : ({} as never);

  return { ...endorsements, retraction };
}
