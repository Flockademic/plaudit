import { OrcidId, Pid } from "knex/types/tables";
import { EndorsementType } from "../endpoints";
import { getConnection } from "../database";
import { groupBy } from "../../functions/groupBy";

export type Result = {
  orcid: OrcidId;
  name?: string;
  endorsements: Array<{
    pid: Pid;
    title: string;
    url: string;
    tags: EndorsementType[];
    createdAt: Date;
  }>;
};

export const getEndorsementsForOrcid = async (
  orcidId: OrcidId,
): Promise<Result> => {
  const connection = await getConnection();

  const queryResult = await connection("user")
    .select([
      connection.ref("name").withSchema("user").as("userName"),
      connection.ref("title").withSchema("work_data"),
      connection.ref("url").withSchema("work_data"),
      connection.ref("pid").withSchema("endorsement"),
      connection.ref("createdAt").withSchema("endorsement"),
      connection.ref("tag").withSchema("tag"),
    ])
    .leftJoin("orcid_account", "user.id", "orcid_account.userId")
    .leftJoin("endorsement", "orcid_account.orcid", "endorsement.userOrcid")
    .leftJoin(
      "endorsement_tags_tag",
      "endorsement.id",
      "endorsement_tags_tag.endorsementId",
    )
    .leftJoin("tag", "endorsement_tags_tag.tagId", "tag.id")
    .leftJoin("work_data", "endorsement.pid", "work_data.pid")
    .orderBy("endorsement.updatedAt", "desc")
    .where("orcid_account.orcid", orcidId);

  const endorsementRows = groupBy(queryResult, (row) => row.pid);
  const endorsements: Result["endorsements"] = Object.entries(
    endorsementRows,
  ).map(([pid, rows]) => {
    return {
      pid: pid as Pid,
      title: rows[0].title,
      url: rows[0].url,
      tags: rows.map((row) => row.tag).filter((tag) => tag !== null),
      createdAt: rows[0].createdAt,
    };
  });

  return {
    orcid: orcidId,
    name: queryResult[0]?.userName,
    endorsements: endorsements,
  };
};
