import { getConnection } from "../database";
import { groupBy } from "../../functions/groupBy";

export type Result = Array<{
  orcid: string;
  name?: string;
  endorsements: Array<{
    identifierType: "doi";
    identifier: string;
    createdAt: number;
    source: "widget" | "website" | "scielo";
  }>;
}>;

export const getRecentEndorsers = async (): Promise<Result> => {
  const connection = await getConnection();

  console.log("QUERY");
  const queryResults = await connection("user")
    .select([
      connection.ref("orcid").withSchema("orcid_account"),
      connection.ref("name").withSchema("user"),
      connection.ref("pid").withSchema("endorsement"),
      connection.ref("createdAt").withSchema("endorsement"),
      connection.ref("source").withSchema("endorsement"),
    ])
    .leftJoin("orcid_account", "user.id", "orcid_account.userId")
    .leftJoin("endorsement", "orcid_account.orcid", "endorsement.userOrcid")
    .groupBy([
      "orcid_account.orcid",
      "user.name",
      "user.createdAt",
      "user.updatedAt",
      "user.version",
      "endorsement.id",
    ])
    .havingRaw("COUNT(endorsement.*) > 0")
    .orderBy("endorsement.createdAt", "desc")
    .whereRaw(
      "endorsement.\"createdAt\" > (CURRENT_DATE - INTERVAL '1 month')",
    );
  console.log({ queryResults });

  const resultsByEndorser = groupBy(
    queryResults,
    (row) => row.orcid ?? "unknown-endorser",
  );
  const endorsers = Object.entries(resultsByEndorser).map(([orcid, rows]) => {
    return {
      orcid: orcid,
      name: rows[0].name,
      endorsements: rows.map((row) => ({
        identifierType: row.pid!.substring(0, row.pid!.indexOf(":")) as "doi",
        identifier: row.pid!.substring(row.pid!.indexOf(":") + 1),
        createdAt: row.createdAt!.getTime(),
        source: row.source!,
      })),
    };
  });

  return endorsers;
};
