import knex, { Knex } from "knex";
import config from "../../knexfile";

export async function getConnection(): Promise<Knex> {
  if (!globalThis.connection) {
    globalThis.connection = knex(config);
    const [completedMigrations, pendingMigrations] =
      (await globalThis.connection.migrate.list()) as [
        Array<{ name: string }>,
        Array<{ file: string; directory: string }>,
      ];
    console.log(
      "# Initialised database.\n",
      `Version ${await globalThis.connection.migrate.currentVersion()}, with migrations:\n`,
      completedMigrations.map((m) => `- ${m.name}`).join("\n"),
    );
    if (pendingMigrations.length > 0) {
      console.log(
        `Applying ${pendingMigrations.length} pending migrations:\n`,
        pendingMigrations.map((m) => `- ${m.file}`).join("\n"),
      );
      require("ts-node").register();
      await globalThis.connection.migrate.latest();
      console.log(
        `Done, now at version ${await globalThis.connection.migrate.currentVersion()}`,
      );
    }
  }

  return globalThis.connection;
}
