import React from "react";
import { StoryFn, Meta } from "@storybook/react";

import { SignIn, Props } from "../components/widget/SignIn";
import { screenshot, viewports } from "./viewports";

export default {
  title: "Widget/SignIn",
  component: SignIn,
  argTypes: {
    handleSignIn: { action: "Initiate sign in" },
  },
  parameters: {
    //👇 The viewports object from the Essentials addon
    viewport: viewports,
    screenshot: screenshot,
  },
} as Meta;

const Template: StoryFn<Props> = (args) => <SignIn {...args} />;

export const Default = Template.bind({});
Default.args = {};
