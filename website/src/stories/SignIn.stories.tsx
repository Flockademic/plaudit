import React from "react";
import { StoryFn, Meta } from "@storybook/react";

import { Tag, Props } from "../components/widget/Tag";
import { screenshot, viewports } from "./viewports";

export default {
  title: "Widget/Tag",
  component: Tag,
  argTypes: {
    setTags: { action: "Submit" },
  },
  parameters: {
    //👇 The viewports object from the Essentials addon
    viewport: viewports,
    screenshot: screenshot,
  },
} as Meta;

const Template: StoryFn<Props> = (args) => <Tag {...args} />;

export const Default = Template.bind({});
Default.args = {};
