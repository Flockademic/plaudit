import React from "react";
import { StoryFn, Meta } from "@storybook/react";
import { OrcidId } from "knex/types/tables";

import { View, Props } from "../components/widget/View";
import { screenshot, viewports } from "./viewports";

const currentUser = {
  orcid: "0000-0002-6288-9867" as OrcidId,
  name: "Vincent",
};

const doi = "10.31219/osf.io/khbvy";

const endorsements = [
  { doi: doi, orcid: currentUser.orcid, tags: [], userName: currentUser.name },
  { doi: doi, orcid: "0000-0000-0000-0001", tags: [], userName: "Alice Doe" },
  { doi: doi, orcid: "0000-0000-0000-0002", tags: [], userName: "Bob Doe" },
];

export default {
  title: "Widget/View",
  component: View,
  args: {
    doi: doi,
    loggedIn: false,
    justEndorsed: false,
    workData: {
      title: "Example article",
      url: `https://doi.org/${doi}`,
    },
  },
  argTypes: {
    handleEndorsement: { action: "Endorse" },
    handleUndo: { action: "Undo endorsement" },
  },
  parameters: {
    //👇 The viewports object from the Essentials addon
    viewport: viewports,
    screenshot: screenshot,
  },
} as Meta;

const Template: StoryFn<Props> = (args) => {
  const userData = (args as any).loggedIn ? currentUser : undefined;
  const handleUndo =
    (args as any).loggedIn === true && args.justEndorsed === true
      ? args.handleUndo
      : undefined;
  delete (args as any).loggedIn;
  return <View {...args} user={userData} handleUndo={handleUndo} />;
};

function getStory(options: {
  nrOfEndorsements: number;
  loggedIn: boolean;
  justEndorsed: boolean;
}) {
  const Story = Template.bind({});
  Story.storyName = `Endorsements: ${options.nrOfEndorsements}`;
  if (options.loggedIn) {
    Story.storyName += ", logged in";
    if (options.justEndorsed) {
      Story.storyName += ", just endorsed";
    }
  }
  Story.args = {
    endorsements: endorsements.slice(0, options.nrOfEndorsements),
    justEndorsed: options.loggedIn && options.justEndorsed,
  };
  (Story.args as any).loggedIn = options.loggedIn;
  return Story;
}

export const Endorsements0 = getStory({
  nrOfEndorsements: 0,
  loggedIn: false,
  justEndorsed: false,
});
export const Endorsements1 = getStory({
  nrOfEndorsements: 1,
  loggedIn: false,
  justEndorsed: false,
});
export const Endorsements1LoggedIn = getStory({
  nrOfEndorsements: 1,
  loggedIn: true,
  justEndorsed: false,
});
export const Endorsements1JustEndorsed = getStory({
  nrOfEndorsements: 1,
  loggedIn: true,
  justEndorsed: true,
});
export const Endorsements2 = getStory({
  nrOfEndorsements: 2,
  loggedIn: false,
  justEndorsed: false,
});
export const Endorsements2LoggedIn = getStory({
  nrOfEndorsements: 2,
  loggedIn: true,
  justEndorsed: false,
});
export const Endorsements2JustEndorsed = getStory({
  nrOfEndorsements: 2,
  loggedIn: true,
  justEndorsed: true,
});
export const Endorsements3 = getStory({
  nrOfEndorsements: 3,
  loggedIn: false,
  justEndorsed: false,
});
export const Endorsements3LoggedIn = getStory({
  nrOfEndorsements: 3,
  loggedIn: true,
  justEndorsed: false,
});
export const Endorsements3JustEndorsed = getStory({
  nrOfEndorsements: 3,
  loggedIn: true,
  justEndorsed: true,
});
