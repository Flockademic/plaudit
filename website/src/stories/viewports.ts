export const viewports = {
  //👇 The viewports you want to use
  viewports: {
    wide: {
      name: "Wide",
      styles: {
        width: "450px",
        height: "200px",
      },
    },
    narrow: {
      name: "Narrow",
      styles: {
        width: "230px",
        height: "400px",
      },
    },
  },
  //👇 Your own default viewport
  // defaultViewport: 'wide'
};

export const screenshot = {
  viewports: {
    narrow: {
      width: 230,
      height: 400,
    },
    wide: {
      width: 400,
      height: 400,
    },
  },
};
