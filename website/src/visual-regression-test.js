const { imgDiff } = require("img-diff-js");
const { readdir, mkdir } = require("fs/promises");
const { resolve } = require("path");

async function visualRegressionTest() {
  const components = await readdir(
    resolve(__dirname, "../reference-screenshots/Widget"),
  );
  const componentPromises = components.map(async (component) => {
    const storyReferences = await readdir(
      resolve(__dirname, "../reference-screenshots/Widget", component),
    );
    const storyPromises = storyReferences.map(async (storyReference) => {
      const result = await imgDiff({
        actualFilename: resolve(
          __dirname,
          "../__screenshots__/Widget",
          component,
          storyReference,
        ),
        expectedFilename: resolve(
          __dirname,
          "../reference-screenshots/Widget",
          component,
          storyReference,
        ),
        diffFilename: resolve(
          __dirname,
          "../diffs/Widget",
          component,
          storyReference,
        ),
      });
      return result;
    });
    return await Promise.all(storyPromises);
  });

  const results = await Promise.all(componentPromises);
  console.log(results);
  if (
    !results.every((componentResults) =>
      componentResults.every((storyResult) => storyResult.imagesAreSame),
    )
  ) {
    process.exit(1);
  }
}

visualRegressionTest();
