"use client";

import { signIn } from "next-auth/react";
import { HTMLAttributes } from "react";

export const SignInLink = (props: HTMLAttributes<HTMLAnchorElement>) => {
  return <a onClick={() => signIn("orcid")} {...props} className={`underline cursor-pointer text-blue-500 hover:text-blue-700 ${props.className ?? ""}`} />;
};
