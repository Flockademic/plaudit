import React from "react";
import { GetEndorserResponse, GetOrcidResponse } from "../backend/endpoints";
import { RssIcon } from "./icons";
import { WorkBadge } from "./WorkBadge";
import { OrcidProfile } from "./OrcidProfile";
import { formatName } from "../functions/formatName";

interface Props {
  endorser: GetEndorserResponse["endorser"];
  orcidInfo: GetOrcidResponse | null;
}

export const EndorserDetails = (props: Props) => (
  <div>
    <header className="flex flex-col gap-y-3 mx-10 lg:mx-20 xl:mx-32 my-10 border-l-8 border-blue-500 pl-5">
      <h1 className="text-2xl md:text-3xl text-blue-500">
        Endorsements by{" "}
        <bdi>{formatName(props.endorser.name || props.endorser.orcid)}</bdi>
      </h1>
      <p className="text-xl">
        <a
          href={`${process.env.NEXT_PUBLIC_ORCID_URL}${props.endorser.orcid}`}
          title={`View the ORCID profile of ${formatName(props.endorser.name || props.endorser.orcid)}`}
          className="flex items-center gap-2"
        >
          <img
            src="/icons/orcid.svg"
            className="orcidIcon"
            alt=""
            width={20}
            height={20}
          />
          {props.endorser.orcid}
        </a>
      </p>
      <OrcidProfile orcidInfo={props.orcidInfo} orcid={props.endorser.orcid} />
    </header>
    <section className="px-10 lg:px-20 xl:px-32 py-10">
      {renderEndorsements(props.endorser)}
      {props.orcidInfo ? renderWorks(props.orcidInfo) : null}
    </section>
  </div>
);

function renderEndorsements(data: GetEndorserResponse["endorser"]) {
  if (data.endorsements.length === 0) {
    return (
      <div className="w-full max-w-2xl">
        <div className="flex items-center">
          <h2 className="text-xl font-semibold leading-loose flex-grow">
            No endorsements
          </h2>
          <a
            href={`${process.env.REACT_APP_URL}/api/endorsers/${data.orcid}.rss`}
            title={`Subscribe to the RSS feed of ${formatName(data.name || data.orcid)}`}
            className="rounded-md hover:bg-blue-100 w-10 h-10 flex items-center justify-center"
            rel="alternate"
            type="application/rss+xml"
          >
            <RssIcon
              alt={`RSS feed of endorsements by ${formatName(data.name || data.orcid)}`}
            />
          </a>
        </div>
        <p className="py-3">This person has not endorsed any works yet.</p>
      </div>
    );
  }

  const list = data.endorsements.map((endorsement) => {
    return (
      <li
        key={endorsement.identifierType + endorsement.identifier}
        className="bg-blue-50 px-4 py-4 rounded-md"
      >
        <WorkBadge endorsement={endorsement} />
      </li>
    );
  });

  return (
    <div className="w-full max-w-2xl">
      <div className="flex items-center">
        <h2 className="text-xl font-semibold leading-loose flex-grow">
          Endorsements
        </h2>
        <a
          href={`${process.env.NEXT_PUBLIC_BACKEND_URL}/api/endorsers/${data.orcid}.rss`}
          title={`Subscribe to the RSS feed of ${formatName(data.name || data.orcid)}`}
          className="rounded-md hover:bg-blue-100 w-10 h-10 flex items-center justify-center"
          rel="alternate"
          type="application/rss+xml"
        >
          <RssIcon
            alt={`RSS feed of endorsements by ${formatName(data.name || data.orcid)}`}
          />
        </a>
      </div>
      <ul className="py-3 flex flex-col gap-3">{list}</ul>
    </div>
  );
}

function renderWorks(orcidInfo: GetOrcidResponse) {
  const works = orcidInfo["activities-summary"].works.group
    .filter((work) =>
      work["work-summary"][0]["external-ids"]?.["external-id"].some(
        (id) => id["external-id-type"] === "doi",
      ),
    )
    .map((work) => {
      const doiIdentifier = work["work-summary"][0]["external-ids"]?.[
        "external-id"
      ].find((id) => id["external-id-type"] === "doi")!;
      return {
        identifierType: "doi" as const,
        identifier: doiIdentifier["external-id-value"],
        title:
          work["work-summary"][0].title?.title.value ??
          work["work-summary"][1]?.title?.title.value ??
          doiIdentifier["external-id-value"],
        url: `https://doi.org/${doiIdentifier["external-id-value"]}`,
        tags: [],
      };
    });

  if (works.length === 0) {
    return null;
  }

  const list = works.map((work) => {
    return (
      <li
        key={work.identifierType + work.identifier}
        className="bg-blue-50 px-4 py-4 rounded-md"
      >
        <WorkBadge endorsement={work} />
      </li>
    );
  });

  return (
    <div className="mt-w-full max-w-2xl">
      <h2 className="text-xl font-semibold leading-loose flex-grow">
        Own works
      </h2>
      <ul className="py-3 flex flex-col gap-3">{list}</ul>
    </div>
  );
}
