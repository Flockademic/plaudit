import React from "react";
// import { detect as detectBrowser } from 'detect-browser';

const firefoxLink = "https://addons.mozilla.org/firefox/addon/plauditpub/";
const chromiumLink =
  "https://chrome.google.com/webstore/detail/plaudit/bmiognkendaelpjffodmfimmbcllbcen";
// const browserInfo = detectBrowser();

interface Props {
  analyticsSuffix: string;
  location?: "header" | "content";
}

export const ExtensionButtons: React.FC<Props> = (props) => {
  // Unfortunately, if the layout with Chrome first is prerendered,
  // then the Chrome link will be present in the Firefox button when the page is loaded in Firefox.
  // Presumably React does not manage to properly reconcile the actual links inside of the
  // <OutboundLink>. This might need a fix here, or in react-ga-donottrack,
  // but for now, we'll make do without browser-detecting trickery:
  // Update: I removed react-ga, but did not dive into the above issue yet;
  // it might no longer be relevant.
  return <EqualButtons {...props} />;
  // if (!browserInfo) {
  //   return <ChromeFirst analyticsSuffix={props.analyticsSuffix}/>;
  // }

  // switch (browserInfo.name) {
  //   case 'firefox':
  //     return <FirefoxFirst {...props}/>;
  //   default:
  //     return <ChromeFirst {...props}/>;
  // }
};

function getPrimaryClass(location: "header" | "content" = "content") {
  return location === "header"
    ? "button is-medium"
    : "button is-large is-primary";
}
function getSecondaryClass(location: "header" | "content" = "content") {
  return location === "header"
    ? "button is-medium is-primary is-inverted is-outlined"
    : "button";
}
function getNeutralClass(location: "header" | "content" = "content") {
  return location === "header"
    ? "button is-medium"
    : "button is-large is-primary";
}

const FirefoxFirst: React.FC<Props> = (props) => (
  <span className="buttons">
    <a href={firefoxLink} className={getPrimaryClass(props.location)}>
      Add to Firefox
    </a>
    <a href={chromiumLink} className={getSecondaryClass(props.location)}>
      Add to Chrome
    </a>
  </span>
);

const ChromeFirst: React.FC<Props> = (props) => (
  <span className="buttons">
    <a href={chromiumLink} className={getPrimaryClass(props.location)}>
      Add to Chrome
    </a>
    <a href={firefoxLink} className={getSecondaryClass(props.location)}>
      Add to Firefox
    </a>
  </span>
);

const EqualButtons: React.FC<Props> = (props) => (
  <span className="buttons">
    <a href={chromiumLink} className={getNeutralClass(props.location)}>
      Add to Chrome
    </a>
    <a href={firefoxLink} className={getNeutralClass(props.location)}>
      Add to Firefox
    </a>
  </span>
);
