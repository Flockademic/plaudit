import React, { ReactNode } from "react";
import { GetOrcidResponse } from "../backend/endpoints";

interface Props {
  orcidInfo: GetOrcidResponse | null;
  orcid: string;
}

/**
 * Display _who_ made a particular endorsement
 *
 * @param props The endorsement made by this person
 */
export const OrcidProfile = (props: Props) => {
  if (props.orcidInfo === null) {
    return null;
  }

  const person = props.orcidInfo.person;
  const activities = props.orcidInfo["activities-summary"];

  const name =
    person.name === null
      ? props.orcid
      : person.name["given-names"].value +
        (person.name["family-name"]?.value
          ? " " + person.name["family-name"].value
          : "");

  const employmentRows = activities.employments["employment-summary"].map(
    (employment, i) => {
      const role = employment["role-title"]
        ? employment["role-title"] + ", "
        : null;
      const department = employment["department-name"]
        ? employment["department-name"] + ", "
        : null;
      return (
        <li className="flex gap-1 items-center" key={`employment${i}`}>
          <img
            src="/icons/material-design/action/work-black-18dp.svg"
            alt="Employment:"
          ></img>
          <Bdi>{role}</Bdi>
          <Bdi>{department}</Bdi>
          <Bdi>{employment.organization.name}</Bdi>
        </li>
      );
    },
  );
  const educationRows = activities.educations["education-summary"].map(
    (education, i) => {
      const role = education["role-title"]
        ? education["role-title"] + ", "
        : null;
      return (
        <li className="flex gap-1 items-center" key={`education${i}`}>
          <img
            src="/icons/material-design/social/school-black-18dp.svg"
            alt="Education:"
          ></img>
          <Bdi>{role}</Bdi>
          <Bdi>{education.organization.name}</Bdi>
        </li>
      );
    },
  );

  const keywords = person.keywords.keyword.map((keyword) => (
    <Bdi key={keyword.content}>{keyword.content}</Bdi>
  ));
  const keywordRow =
    person.keywords.keyword.length > 0 ? (
      <li className="flex gap-1 items-center">
        <img
          src="/icons/material-design/action/label-black-18dp.svg"
          alt="Keywords:"
        ></img>
        {keywords}
      </li>
    ) : null;

  const urls = person["researcher-urls"]["researcher-url"];
  const urlRows = urls.map((url) => {
    return (
      <a
        href={url.url.value}
        rel="me"
        className="flex gap-1 items-center underline hover:no-underline"
        key={url.url.value}
      >
        <img
          src="/icons/material-design/content/link-black-18dp.svg"
          alt="Website:"
        ></img>
        <Bdi>{url["url-name"] ?? url.url.value}</Bdi>
      </a>
    );
  });

  return (
    <ul className="flex flex-wrap gap-8">
      {employmentRows}
      {educationRows}
      {keywordRow}
      {urlRows}
    </ul>
  );
};

/**
 * Wraps non-empty elements in a <bdi>, but doesn't render empty <bdi> tags for empty elements
 */
const Bdi = (props: { children?: ReactNode }) => {
  if (
    !props.children ||
    (typeof props.children === "string" && props.children.trim().length === 0)
  ) {
    return null;
  }

  return <bdi>{props.children}</bdi>;
};
