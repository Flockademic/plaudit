import React from "react";
import Link from "next/link";
import { Session } from "next-auth";
import { Endorsement as EndorsementType } from "../backend/endpoints";
import { formatName } from "../functions/formatName";

interface Props {
  endorsement: EndorsementType & { userName?: string };
  session?: Session;
}

const TagList: React.FC<{ tags?: EndorsementType["tags"] }> = (props) => {
  if (!props.tags) {
    return null;
  }

  return (
    <p className="flex space-x-2">
      {props.tags.map((tag) => (
        <span
          key={tag}
          className="bg-blue-100 px-3 py-2 leading-none rounded-full font-semibold"
        >
          {tag}
        </span>
      ))}
    </p>
  );
};

/**
 * Display _who_ made a particular endorsement
 *
 * @param props The endorsement made by this person
 */
export const EndorserBadge: React.FC<Props> = (props) => (
  <div className="flex flex-col justify-start space-y-3">
    <Link
      href={`/endorser/${props.endorsement.orcid}`}
      title={`View other endorsements by ${props.endorsement.userName || props.endorsement.orcid}`}
      className={
        "content is-medium" +
        (props.session?.user.id === props.endorsement.orcid
          ? " has-text-weight-bold"
          : "")
      }
    >
      <bdi>
        {formatName(props.endorsement.userName ?? props.endorsement.orcid)}
      </bdi>
    </Link>
    <TagList tags={props.endorsement.tags} />
  </div>
);
