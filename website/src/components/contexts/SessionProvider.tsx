// See https://nextjs.org/docs/getting-started/react-essentials#rendering-third-party-context-providers-in-server-components
"use client";

import { SessionProvider as OgSessionProvider } from "next-auth/react";

export const SessionProvider: typeof OgSessionProvider = (props) => (
  <OgSessionProvider {...props} />
);
