import { ButtonHTMLAttributes } from "react";
import { useFormStatus } from "react-dom";
import { PiSpinnerBold } from "react-icons/pi";

export const SubmitButton = (props: ButtonHTMLAttributes<HTMLButtonElement>) => {
  const formStatus = useFormStatus();

  return (
    <button
      type="submit"
      className={`min-h-14 border-4 border-blue-500 bg-blue-500 text-white text-lg font-bold hover:bg-transparent hover:text-blue-500 focus:outline outline-4 outline-blue-500 outline-offset-2 py-2 px-10 rounded-md`}
      aria-live={formStatus.pending ? "polite" : undefined}
      disabled={formStatus.pending}
      {...props}
    >
      {formStatus.pending ? <PiSpinnerBold aria-label="Loading" className="animate-spin" /> : props.children}
    </button>
  );
};
