import React from "react";
import { Session } from "next-auth";
import { GetEndorsementsWithAuthorsResponse } from "../backend/endpoints";
import { EndorserBadge } from "./EndorserBadge";
import { formatName } from "../functions/formatName";
import { EndorseBanner } from "./EndorseBanner";
import { Doi } from "knex/types/tables";

interface Props {
  endorsements: GetEndorsementsWithAuthorsResponse["endorsements"];
  workData: GetEndorsementsWithAuthorsResponse["workData"];
  doi: Doi;
  session?: Session;
}

export const WorkDetails = (props: Props) => (
  <div>
    <header className="flex flex-col gap-y-3 mx-10 lg:mx-20 xl:mx-32 my-10 border-l-8 border-blue-500 pl-5">
      <h1 className="text-2xl md:text-3xl text-blue-500">
        <a href={props.workData.url}>
          <bdi>{props.workData.title}</bdi>
        </a>
      </h1>
      <ul className="text-xl inline" aria-live="polite">
        {props.workData.authors.map((author, i) => {
          return (
            <li key={author.orcid + author.name} className="inline">
              {i > 0 && ", "}
              <bdi>{formatName(author.name)}</bdi>
            </li>
          );
        })}
      </ul>
    </header>
    <section className="px-10 lg:px-20 xl:px-32 py-10">
      {renderEndorsements(props.endorsements, props.session)}
    </section>
    <section className="px-10 lg:px-20 xl:px-32 py-10">
      <EndorseBanner
        doi={props.doi}
        session={props.session}
        hasEndorsed={props.endorsements.findIndex(
          (endorsement) => endorsement.orcid === props.session?.user.id
        ) !== -1}
      />
    </section>
  </div>
);

function renderEndorsements(
  endorsements: GetEndorsementsWithAuthorsResponse["endorsements"],
  session?: Session,
) {
  if (endorsements.length === 0) {
    return (
      <div>
        <h2 className="text-xl font-semibold leading-loose">No endorsements</h2>
        <p className="text-lg leading-relaxed">
          This article has not been endorsed yet.
        </p>
      </div>
    );
  }

  const list = endorsements.map((endorsement) => {
    return (
      <li
        key={`${endorsement.doi}-${endorsement.orcid}`}
        className="bg-blue-50 px-4 py-4 rounded-md"
      >
        <EndorserBadge endorsement={endorsement} session={session} />
      </li>
    );
  });

  return (
    <>
      <h2 className="text-xl font-semibold leading-loose">Endorsed by:</h2>
      <ul className="grid lg:grid-cols-2 xl:grid-cols-3 gap-5 py-3">{list}</ul>
    </>
  );
}
