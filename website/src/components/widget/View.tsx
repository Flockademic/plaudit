import { FC, MouseEventHandler } from "react";
import styles from "./View.module.scss";
import { Logo } from "./Logo";
import {
  Endorsement,
  EndorsementType,
  GetEndorsementsResponse,
} from "../../backend/endpoints";
import { formatName } from "../../functions/formatName";

export type Props = {
  endorsements: GetEndorsementsResponse["endorsements"];
  workData: GetEndorsementsResponse["workData"];
  user: GetEndorsementsResponse["user"];
  handleEndorsement: () => void;
  doi: string;
  // The containing component can flip this to true to trigger an "applause" animation
  justEndorsed?: boolean;
  handleUndo?: () => void;
};

const appUrl =
  process.env.STORYBOOK_APP_URL ||
  process.env.NEXT_PUBLIC_WEBSITE_URL ||
  "https://plaudit.pub";
export const View: FC<Props> = (props) => {
  const onClickEndorse: MouseEventHandler<HTMLButtonElement> = (event) => {
    event.preventDefault();

    props.handleEndorsement();
  };

  // Zero endorsements, not endorsed by the current user:
  if (Object.keys(props.endorsements).length === 0) {
    return (
      <section>
        <button
          onClick={onClickEndorse}
          tabIndex={0}
          title={`Endorse "${props.workData.title}"`}
          className={`${styles.Widget} ${styles["Widget--no_endorsements"]}`}
          // The data-iframe-height attribute tells iFrameResizer to fit the iframe to this element:
          data-iframe-height="data-iframe-height"
        >
          {/* This is needed because <button> elements cannot be flex containers. */}
          {/* See https://stackoverflow.com/a/35466231 */}
          <span className={styles.Widget__FlexContainer}>
            <span
              aria-hidden="true"
              className={styles["Widget__ButtonContainer"]}
            >
              <span className={styles["Widget__Button"]}>
                <Logo className={styles["Widget__logo"]} />
              </span>
            </span>
            <span className={styles["Widget__Tagline"]}>
              <span className={styles["Widget__TaglineContent"]}>
                Be the first to endorse <Work workData={props.workData} />
              </span>
            </span>
          </span>
        </button>
      </section>
    );
  }

  const userOrcid = props.user?.orcid;
  const tagline = renderTagline(
    mapEndorsementsToOrcids(props.endorsements),
    props.workData,
    `doi:${props.doi}`,
    userOrcid,
    props.handleUndo,
  );
  const buttonOrBrand =
    typeof userOrcid !== "undefined" &&
    props.endorsements.findIndex((e) => e.orcid === userOrcid) !== -1 ? (
      <span aria-hidden="true" className={styles.Widget__Brand}>
        <Logo className={styles["Widget__logo"]} />
      </span>
    ) : (
      <span className={styles.Widget__ButtonContainer}>
        <button
          onClick={onClickEndorse}
          tabIndex={0}
          title={`Endorse "${props.workData.title}"`}
          className={styles.Widget__Button}
        >
          <Logo className={styles["Widget__logo"]} />
        </button>
      </span>
    );

  const className =
    props.justEndorsed === true
      ? `${styles.Widget} ${styles["Widget--just-endorsed"]}`
      : styles.Widget;
  return (
    // The data-iframe-height attribute tells iFrameResizer to fit the iframe to this element:
    <section className={className} data-iframe-height="data-iframe-height">
      {/* When there are no endorsements, the <section> above is a <button>. */}
      {/* That requires the .Widget__FlexContainer as a workaround. */}
      {/* We also include that here to be able to apply consistent styling. */}
      <span className={styles.Widget__FlexContainer}>
        {buttonOrBrand}
        <span className={styles.Widget__Tagline}>
          <span className={styles.Widget__TaglineContent}>{tagline}</span>
        </span>
      </span>
    </section>
  );
};

interface EndorsementsByOrcid {
  [orcid: string]: { endorserName?: string; tags: EndorsementType[] };
}
function mapEndorsementsToOrcids(
  endorsements: Array<Endorsement & { userName?: string }>,
): EndorsementsByOrcid {
  const endorsementsByOrcid = endorsements.reduce<EndorsementsByOrcid>(
    (soFar, endorsement) => {
      soFar[endorsement.orcid] = soFar[endorsement.orcid] || {};
      soFar[endorsement.orcid].endorserName = endorsement.userName;
      soFar[endorsement.orcid].tags = endorsement.tags;

      return soFar;
    },
    {},
  );

  return endorsementsByOrcid;
}

const Work: FC<{ workData: GetEndorsementsResponse["workData"] }> = (props) => (
  <abbr className={styles["WorkDetail"]} title={props.workData.title}>
    this work
  </abbr>
);

const renderTagline = (
  endorsements: EndorsementsByOrcid,
  workData: GetEndorsementsResponse["workData"],
  pid: string,
  userOrcid?: string,
  handleUndo?: () => void,
) => {
  const orcids = Object.keys(endorsements);

  const undoButton =
    typeof handleUndo === "function" ? (
      <>
        &nbsp;
        <button
          className={styles.Widget__UndoButton}
          onClick={() => handleUndo()}
        >
          (undo endorsement)
        </button>
      </>
    ) : null;

  // If the current user has endorsed this work, show them first:
  if (
    typeof userOrcid !== "undefined" &&
    typeof endorsements[userOrcid] !== "undefined"
  ) {
    if (orcids.length === 1) {
      return (
        <>
          {renderUser(userOrcid, endorsements)} have endorsed{" "}
          <Work workData={workData} />
          {undoButton}.
        </>
      );
    }

    const otherEndorsers = orcids.filter(
      (endorserOrcid) => endorserOrcid !== userOrcid,
    );
    if (orcids.length === 2) {
      return (
        <>
          {renderUser(userOrcid, endorsements)} and{" "}
          {renderEndorser(otherEndorsers[0], endorsements)} have endorsed{" "}
          <Work workData={workData} />
          {undoButton}.
        </>
      );
    }

    return (
      <>
        {renderUser(userOrcid, endorsements)} and{" "}
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={`${appUrl}/endorsements/${pid}`}
          title="View all endorsements"
          className={styles.Widget__Endorser}
        >
          {otherEndorsers.length} others
        </a>{" "}
        have endorsed <Work workData={workData} />
        {undoButton}.
      </>
    );
  }

  // If the current user has not endorsed this work, list other endorsements:
  if (orcids.length === 1) {
    return (
      <>
        {renderEndorser(orcids[0], endorsements)} has endorsed{" "}
        <Work workData={workData} />.
      </>
    );
  }
  if (orcids.length === 2) {
    return (
      <>
        {renderEndorser(orcids[0], endorsements)} and{" "}
        {renderEndorser(orcids[1], endorsements)} have endorsed{" "}
        <Work workData={workData} />.
      </>
    );
  }

  return (
    <>
      {renderEndorser(orcids[0], endorsements)} and{" "}
      <a
        target="_blank"
        rel="noopener noreferrer"
        href={`${appUrl}/endorsements/${pid}`}
        title="View all endorsements"
        className={styles.Widget__Endorser}
      >
        {orcids.length - 1} others
      </a>{" "}
      have endorsed <Work workData={workData} />.{undoButton}
    </>
  );
};

const renderUser = (userOrcid: string, endorsements: EndorsementsByOrcid) => {
  const endorserName = endorsements[userOrcid].endorserName || userOrcid;
  const tagsTitle =
    endorsements[userOrcid].tags.length > 0
      ? `You (${endorserName}) thought this work was: ${endorsements[userOrcid].tags.join(", ")}. `
      : "";
  return (
    <a
      target="_blank"
      rel="noopener noreferrer"
      href={`${appUrl}/endorser/${userOrcid}`}
      className={styles.Widget__Endorser}
      title={tagsTitle + "View your other endorsements."}
    >
      You
    </a>
  );
};

const renderEndorser = (
  endorserOrcid: string,
  endorsements: EndorsementsByOrcid,
) => {
  const endorserName = formatName(
    endorsements[endorserOrcid].endorserName || endorserOrcid,
  );
  const tagsTitle =
    endorsements[endorserOrcid].tags.length > 0
      ? `${endorserName} thought this work was: ${endorsements[endorserOrcid].tags.join(", ")}. `
      : "";
  return (
    <a
      target="_blank"
      rel="noopener noreferrer"
      href={`${appUrl}/endorser/${endorserOrcid}`}
      className={styles.Widget__Endorser}
      title={tagsTitle + `View other endorsements by ${endorserName}.`}
    >
      <bdi>{endorserName}</bdi>
    </a>
  );
};
