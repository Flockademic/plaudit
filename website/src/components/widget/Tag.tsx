import { FC, FormEventHandler, useState } from "react";
import styles from "./Tag.module.scss";
import { EndorsementType } from "../../backend/endpoints";

export type Props = {
  setTags: (tags: EndorsementType[]) => void;
};

export const Tag: FC<Props> = (props) => {
  const [tags, setTags] = useState<EndorsementType[]>([]);

  const handleSubmit: FormEventHandler<HTMLFormElement> = (event) => {
    event.preventDefault();

    props.setTags(tags);
  };

  const toggleTag = (tag: EndorsementType) => {
    setTags((currentTags) => {
      return currentTags.includes(tag)
        ? currentTags.filter((existingTag) => existingTag !== tag)
        : currentTags.concat(tag);
    });
  };

  return (
    // The data-iframe-height attribute tells iFrameResizer to fit the iframe to this element:
    <aside
      className={styles.DetailView}
      data-iframe-height="data-iframe-height"
    >
      <form
        id="endorsementDetails"
        onSubmit={handleSubmit}
        className={styles.DetailView__Form}
      >
        {/* .DetailView__Tags would intuitively be a <fieldset> and span#tagLegend a <legend>, */}
        {/* but unfortunately those cannot be a flex container. */}
        {/* See https://stackoverflow.com/a/35466231 */}
        <span className={styles.DetailView__Tags}>
          <span id="tagLegend" className={styles.DetailView__TagLine}>
            I found this work to be&hellip;
          </span>
          <div
            role="group"
            aria-labelledby="tagLegend"
            className={styles.DetailView__TagOptions}
          >
            <label htmlFor="robust" className={styles.DetailView__Field}>
              <input
                type="checkbox"
                name="robust"
                id="robust"
                checked={tags.includes("robust")}
                onChange={() => toggleTag("robust")}
              />
              <span>robust</span>
            </label>
            <label htmlFor="clear" className={styles.DetailView__Field}>
              <input
                type="checkbox"
                name="clear"
                id="clear"
                checked={tags.includes("clear")}
                onChange={() => toggleTag("clear")}
              />
              <span>clear</span>
            </label>
            <label htmlFor="exciting" className={styles.DetailView__Field}>
              <input
                type="checkbox"
                name="exciting"
                id="exciting"
                checked={tags.includes("exciting")}
                onChange={() => toggleTag("exciting")}
              />
              <span>exciting</span>
            </label>
          </div>
        </span>
        <div className={styles.DetailView__Submit}>
          <input
            type="submit"
            value="✔"
            aria-label="Submit your additional remarks"
            form="endorsementDetails"
          />
        </div>
      </form>
    </aside>
  );
};
