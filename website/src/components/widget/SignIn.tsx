import { FC, FormEventHandler } from "react";
import styles from "./SignIn.module.scss";
import { Logo } from "./Logo";

export type Props = {
  handleSignIn: () => void;
};

export const SignIn: FC<Props> = (props) => {
  const handleSignIn: FormEventHandler<HTMLFormElement> = (event) => {
    event.preventDefault();

    props.handleSignIn();
  };
  return (
    <form
      onSubmit={handleSignIn}
      className={styles.Widget}
      data-iframe-height="data-iframe-height"
    >
      <span className={styles.Widget__FlexContainer}>
        <span aria-hidden="true" className={styles.Widget__Brand}>
          <Logo className={styles["Widget__logo"]} />
        </span>
        <span className={styles.Widget__Tagline}>
          <span className={styles.Widget__TaglineContent}>
            Please{" "}
            <button type="submit" className={styles.Widget__SignInButton}>
              sign in with ORCID
            </button>{" "}
            to endorse this work.
          </span>
        </span>
      </span>
    </form>
  );
};
