import React from "react";

interface Props {
  alt?: string;
}

export const RssIcon: React.FC<
  Props & React.HTMLAttributes<HTMLImageElement>
> = (props = {}) => {
  const rssIcon = "/icons/material-design/communication/ic_rss_feed_48px.svg";

  return (
    <span className="icon">
      <img
        src={rssIcon}
        alt={props.alt || "Subscribe to feed"}
        width={24}
        height={24}
      />
    </span>
  );
};
