import React from "react";
import Link from "next/link";
import { EndorsementType } from "../backend/endpoints";

interface Props {
  endorsement: {
    identifierType: "doi";
    identifier: string;
    title: string;
    url: string;
    tags: EndorsementType[];
  };
}

const TagList: React.FC<{ tags?: EndorsementType[] }> = (props) => {
  if (!props.tags) {
    return null;
  }

  return (
    <p className="flex space-x-2">
      {props.tags.map((tag) => (
        <span
          key={tag}
          className="bg-blue-100 px-3 py-2 leading-none rounded-full font-semibold"
        >
          {tag}
        </span>
      ))}
    </p>
  );
};

/**
 * Display _what work_ was endorsed
 *
 * @param props The endorsement
 */
export const WorkBadge = (props: Props) => (
  <div
    key={`${props.endorsement.identifierType}:${props.endorsement.identifier}`}
    className="flex flex-col justify-start space-y-3"
  >
    <p>
      <a
        href={props.endorsement.url}
        title={`View "${props.endorsement.title}"`}
        className="text-xl underline hover:no-underline"
      >
        <bdi>{props.endorsement.title}</bdi>
      </a>
    </p>
    <div className="flex items-center">
      <div className="flex-grow">
        <TagList tags={props.endorsement.tags} />
      </div>
      <small>
        <Link
          href={`/endorsements/${props.endorsement.identifierType}:${props.endorsement.identifier}`}
          className="flex items-center text-sm hover:underline"
        >
          <span className="icon">
            <img
              src="/icons/material-design/communication/read_more-black-18dp.svg"
              alt=""
            />
          </span>
          <span>All endorsers</span>
        </Link>
      </small>
    </div>
  </div>
);
