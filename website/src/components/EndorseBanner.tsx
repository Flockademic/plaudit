"use client";

import { ReactNode, useEffect, useState, useActionState } from "react";
import Image from "next/image";
import { Session } from "next-auth";
import { useRouter } from "next/navigation";
import { Doi } from "knex/types/tables";
import Icon from "../../public/favicon.svg";
import { SignInLink } from "./SignInLink";
import { FormStatus, endorse, type FormStatusError } from "../backend/actions/endorse";
import { retractEndorsement } from "../backend/actions/retractEndorsement";
import { SubmitButton } from "./SubmitButton";

interface Props {
  doi: Doi,
  session?: Session;
  hasEndorsed: boolean;
}

export const EndorseBanner = (props: Props): ReactNode => {
  const router = useRouter();
  const endorseThisWork = endorse.bind(null, props.doi)
  const [justUndid, setJustUndid] = useState(false);
  const [formState, formAction] = useActionState<FormStatus, FormData>(endorseThisWork, null);

  useEffect(() => {
    if (formState?.ok) {
      setJustUndid(false);
      router.refresh();
    }
  }, [formState, router]);

  useEffect(() => {
    // If the user's session has expired, reload the page state so that the user
    // is prompted to log in again:
    if (formState?.ok === false && formState.errorCode === "ERR_UNAUTHENTICATED") {
      router.refresh();
    }
    // The deps are actually exhaustive here, but the lint rule doesn't
    // understand type assertions:
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [(formState as FormStatusError)?.errorCode, formState?.ok, router]);

  if (props.hasEndorsed && !formState?.ok) {
    // If the user has already endorsed it, and hasn't just endorsed it in this
    // session (in which case they can still undo), don't prompt for an
    // endorsement:
    return null;
  }

  if (!props.session?.user) {
    return (
      <div className="flex gap-5 items-center">
        <Image src={Icon} alt="" width={50} />
        <div>
          <h2 className="text-xl font-semibold leading-loose">
            Like this work?
          </h2>
          <p>
            Let others know. <SignInLink>Sign in</SignInLink> to add your endorsement.
          </p>
        </div>
      </div>
    );
  }

  return (
    <div className="flex flex-col gap-5 items-start">
      <h2 className="text-xl font-semibold leading-loose">
        Add your endorsement
      </h2>
      <div className="flex items-center gap-5">
        <Image src={Icon} alt="" width={50} />
        {formState?.ok && !justUndid ? (
          <p>
            Endorsed! (
            <button
              onClick={async () => {
                const result = await retractEndorsement(props.doi);
                if (result.ok) {
                  router.refresh();
                  setJustUndid(true);
                }
              }}
              className="text-blue-500 underline"
            >
              Undo
            </button>
            )
          </p>
        ) : (
          <form
            action={formAction}
            className="flex flex-wrap justify-start items-center gap-x-5 text-lg"
          >
            <fieldset className="accent-blue-500 flex flex-col gap-5 text-center">
              <legend className="float-start">
                I found this work to be&hellip;
              </legend>
              <div className="flex gap-10 px-5">
                <label
                  htmlFor="tag_robust"
                  className="flex flex-col items-center gap-2"
                >
                  <input
                    type="checkbox"
                    name="tag_robust"
                    id="tag_robust"
                    value="checked"
                    className="scale-150"
                  />
                  robust
                </label>
                <label
                  htmlFor="tag_clear"
                  className="flex flex-col items-center gap-2"
                >
                  <input
                    type="checkbox"
                    name="tag_clear"
                    id="tag_clear"
                    value="checked"
                    className="scale-150"
                  />
                  clear
                </label>
                <label
                  htmlFor="tag_exciting"
                  className="flex flex-col items-center gap-2"
                >
                  <input
                    type="checkbox"
                    name="tag_exciting"
                    id="tag_exciting"
                    value="checked"
                    className="scale-150"
                  />
                  exciting
                </label>
              </div>
            </fieldset>
            <SubmitButton type="submit">Endorse</SubmitButton>
          </form>
        )}
      </div>
      {formState?.ok === false ? (
        <div aria-live="polite" className="flex items-center gap-2 text-lg text-red-800 font-bold">
          Something went wrong — we&apos;re on it. In the meantime, please try
          again.
        </div>
      ) : null}
    </div>
  );
};
