import { NextRequest, NextResponse } from "next/server";
import { GetEndorserResponse } from "../../../../backend/endpoints";
import {
  endorserToFeed,
  isAtomRequest,
  isRssRequest,
  stripRssExtensions,
} from "../../../../backend/data/toFeed";
import { isValidOrcidId } from "../../../../backend/regexes";
import { getEndorsementsForOrcid } from "../../../../backend/queries/endorsementsForOrcid";

type Params = {
  orcid: string;
};

export const GET = async (request: NextRequest, props: { params: Promise<Params> }) => {
  const orcidId =
    typeof (await props.params).orcid === "string"
      ? stripRssExtensions((await props.params).orcid)
      : "";
  if (!isValidOrcidId(orcidId)) {
    return new NextResponse("Not a valid ORCID iD.", { status: 400 });
  }

  const result = await getEndorsementsForOrcid(orcidId);

  if (typeof result === "undefined") {
    return new NextResponse("Not Found", { status: 404 });
  }

  const endorser = {
    orcid: result.orcid,
    name: result.name,
    endorsements: result.endorsements.map((endorsement) => ({
      identifierType: endorsement.pid.substring(
        0,
        endorsement.pid.indexOf(":"),
      ) as "doi",
      identifier: endorsement.pid.substring(endorsement.pid.indexOf(":") + 1),
      title: endorsement.title,
      url: endorsement.url,
      tags: endorsement.tags,
      timestamp: endorsement.createdAt.toISOString(),
    })),
  };

  const response: GetEndorserResponse = {
    endorser: endorser,
  };

  if (isAtomRequest(request)) {
    return new NextResponse(endorserToFeed(response).atom1(), {
      headers: { "Content-Type": "application/atom+xml" },
    });
  }
  if (isRssRequest(request)) {
    return new NextResponse(endorserToFeed(response).rss2(), {
      headers: { "Content-Type": "application/rss+xml" },
    });
  }
  return NextResponse.json(response);
};
