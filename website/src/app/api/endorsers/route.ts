import { NextRequest, NextResponse } from "next/server";
import { GetEndorsersResponse } from "../../../backend/endpoints";
import { getConnection } from "../../../backend/database";
import { groupBy } from "../../../functions/groupBy";

/**
 * Prevent Next.js from serving a cached version of this route
 *
 * There is nothing to indicate that this route should be dynamically rendered
 * to Next.js: there are no path parameters, no fetch requests with `{ cache:
 * "no-store" }`, … However, we don't want to show the results from the database
 * queries as executed at the time of the last deploy. Thus, we have to force
 * Next.js to dynamically render.
 * See https://nextjs.org/docs/app/building-your-application/caching#opting-out-1
 */
export const dynamic = "force-dynamic";

export const GET = async (request: NextRequest) => {
  const connection = await getConnection();

  try {
    const queryResults = await connection("user")
      .select([
        connection.ref("orcid").withSchema("orcid_account"),
        connection.ref("name").withSchema("user"),
        connection.ref("pid").withSchema("endorsement"),
        connection.ref("createdAt").withSchema("endorsement"),
        connection.ref("source").withSchema("endorsement"),
      ])
      .leftJoin("orcid_account", "user.id", "orcid_account.userId")
      .leftJoin("endorsement", "orcid_account.orcid", "endorsement.userOrcid")
      .groupBy([
        "orcid_account.orcid",
        "user.name",
        "user.createdAt",
        "user.updatedAt",
        "user.version",
        "endorsement.id",
      ])
      .havingRaw("COUNT(endorsement.*) > 0")
      .orderBy("endorsement.createdAt", "desc")
      .whereRaw(
        "endorsement.\"createdAt\" > (CURRENT_DATE - INTERVAL '1 month')",
      );

    const resultsByEndorser = groupBy(
      queryResults,
      (row) => row.orcid ?? "unknown-endorser",
    );
    const endorsers = Object.entries(resultsByEndorser).map(([orcid, rows]) => {
      return {
        orcid: orcid,
        name: rows[0].name,
        endorsements: rows.map((row) => ({
          identifierType: row.pid!.substring(0, row.pid!.indexOf(":")) as "doi",
          identifier: row.pid!.substring(row.pid!.indexOf(":") + 1),
          createdAt: row.createdAt!.getTime(),
          source: row.source!,
        })),
      };
    });

    const response: GetEndorsersResponse = {
      endorsers: endorsers,
    };

    return NextResponse.json(response);
  } catch (e) {
    console.log(e);
    return new NextResponse("Internal Server Error", { status: 500 });
  }
};
