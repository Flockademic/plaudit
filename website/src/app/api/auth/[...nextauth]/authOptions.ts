import { OrcidId } from "knex/types/tables";
import { AuthOptions, Profile } from "next-auth";
import { getConnection } from "../../../../backend/database";
import { getNextAuthAdapter } from "../../../../db/nextAuthAdapter";

export const authOptions: AuthOptions = {
  debug: process.env.NEXT_PUBLIC_AUTH_DEBUG === "true",
  secret: process.env.NEXTAUTH_SECRET,
  session: {
    strategy: "jwt",
  },
  adapter: getNextAuthAdapter(),
  cookies: {
    // We need to set `sameSite` to `none` to be able to share our cookies with
    // our iframe in another domain. See
    // https://next-auth.js.org/configuration/options#cookies
    sessionToken: {
      name: "next-auth.session-token",
      options: {
        sameSite: "none",
        secure: true,
        path: "/",
        httpOnly: true,
      },
    },
    csrfToken: {
      name: "next-auth.csrf-token",
      options: {
        sameSite: "none",
        secure: true,
        path: "/",
        httpOnly: true,
      },
    },
  },
  providers: [
    {
      id: "orcid",
      name: "ORCID",
      type: "oauth",
      wellKnown: `${process.env.NEXT_PUBLIC_ORCID_URL}.well-known/openid-configuration`,
      authorization: { params: { scope: "openid" } },
      clientId: process.env.ORCID_CLIENT_ID!,
      clientSecret: process.env.ORCID_CLIENT_SECRET!,
      profile(profile: Profile) {
        return {
          id: profile.sub!,
          given_name: profile.given_name,
          family_name: profile.family_name,
        };
      },
    },
  ],
  callbacks: {
    // Unused destructured parameters indicate what other values are available:
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async jwt({ token, account, profile, trigger }) {
      if (profile) {
        token.id = profile.sub as OrcidId;
        token.given_name = profile.given_name;
        token.family_name = profile.family_name;
      }
      if (
        typeof account?.refresh_token === "string" &&
        typeof account.access_token === "string" &&
        typeof account.expires_at === "number"
      ) {
        getConnection().then(async (knex) => {
          await knex("orcid_account")
            .where("orcid", account.providerAccountId)
            .update({
              refreshToken: account.refresh_token,
              accessToken: account.access_token,
              expiresAt: account.expires_at,
              updatedAt: knex.fn.now() as unknown as Date,
              version: knex.raw("version + 1") as unknown as number,
            });
        });
      }
      return token;
    },
    async session({ session, token }) {
      session.user = {
        id: token.id,
        given_name: token.given_name,
        family_name: token.family_name,
      };
      return session;
    },
  },
};
