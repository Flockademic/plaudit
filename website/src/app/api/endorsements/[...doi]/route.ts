import { after, NextRequest, NextResponse } from "next/server";
import { getServerSession } from "next-auth";
import {
  Endorsement,
  GetEndorsementsResponse,
  PostEndorsementRequest,
  PostEndorsementResponse,
} from "../../../../backend/endpoints";
import { isValidDoi } from "../../../../backend/regexes";
import { getEndorsementsForDoi } from "../../../../backend/queries/endorsementsForDoi";
import { getConnection } from "../../../../backend/database";
import { groupBy } from "../../../../functions/groupBy";
import { authOptions } from "../../auth/[...nextauth]/authOptions";
import { sendWebmention } from "../../../../backend/data/sendWebmention";

type Params = {
  doi: string[];
};

export const GET = async (request: NextRequest, props: { params: Promise<Params> }) => {
  const doi = Array.isArray((await props.params).doi)
    ? (await props.params).doi.join("/").toLowerCase()
    : null;

  if (!isValidDoi(doi)) {
    return new NextResponse("Not a valid DOI.", { status: 400 });
  }

  const searchParams = request.nextUrl.searchParams;
  const withAuthorsParam = searchParams.get("with_authors");

  const data = await getEndorsementsForDoi(doi, {
    withAuthors: !!withAuthorsParam,
  });
  const session = await getServerSession(authOptions);

  const response: GetEndorsementsResponse = {
    endorsements: data.endorsements,
    workData: {
      ...data.workData,
      authors: data.workData.authors?.map((author) => ({
        orcid: author.orcid ?? undefined,
        name: author.name,
      })),
    },
    user: session?.user
      ? { orcid: session.user.id, name: session.user.name ?? undefined }
      : undefined,
  };
  // Return them as json
  return NextResponse.json(response);
};

export const POST = async (request: NextRequest, props: { params: Promise<Params> }) => {
  const session = await getServerSession(authOptions);
  if (!session?.user) {
    return new NextResponse("Please log in to add an endorsement.", {
      status: 403,
    });
  }

  const orcid = session.user.id;
  const doi = Array.isArray((await props.params).doi)
    ? (await props.params).doi.join("/").toLowerCase()
    : null;
  const identifierType = "doi";

  const body: Partial<PostEndorsementRequest> = await request.json();

  if (!body.tags || !Array.isArray(body.tags) || !isValidDoi(doi)) {
    return new NextResponse("Invalid request.", { status: 400 });
  }
  const tags = body.tags;

  const connection = await getConnection();
  const endorsementsById = await connection.transaction(async (transaction) => {
    const savedEndorsement = await transaction("endorsement")
      .insert({
        pid: `${identifierType}:${doi}`,
        userOrcid: orcid,
        version: 1,
      })
      .onConflict(["pid", "userOrcid"])
      .ignore()
      .returning("id");

    const savedEndorsementId = savedEndorsement[0]?.id;

    if (tags.length > 0 && typeof savedEndorsementId === "number") {
      await transaction("tag")
        .insert(tags.map((tag) => ({ tag: tag, version: 1 })))
        .onConflict("tag")
        .ignore();

      const tagRows = await transaction("tag")
        .select("id")
        .whereIn("tag", tags);

      await transaction("endorsement_tags_tag").insert(
        tagRows.map((tagRow) => ({
          endorsementId: savedEndorsementId,
          tagId: tagRow.id,
        })),
      );
    }

    const endorsementsFromDb = await transaction("endorsement")
      .select([
        connection.ref("id").withSchema("endorsement").as("endorsementId"),
        connection.ref("orcid").withSchema("orcid_account"),
        connection.ref("name").withSchema("user").as("userName"),
        connection.ref("tag").withSchema("tag"),
      ])
      .innerJoin(
        "orcid_account",
        "endorsement.userOrcid",
        "orcid_account.orcid",
      )
      .innerJoin("user", "orcid_account.userId", "user.id")
      .leftJoin(
        "endorsement_tags_tag",
        "endorsement.id",
        "endorsement_tags_tag.endorsementId",
      )
      .leftJoin("tag", "endorsement_tags_tag.tagId", "tag.id")
      .whereRaw("endorsement.pid = LOWER(:pid)", { pid: `doi:${doi}` });

    return groupBy(endorsementsFromDb, (row) => row.endorsementId.toString());
  });

  const endorsements: Array<Endorsement> = Object.entries(endorsementsById).map(
    ([id, endorsementRows]) => ({
      doi: doi,
      orcid: endorsementRows[0].orcid,
      userName: endorsementRows[0].userName,
      tags: endorsementRows.map((row) => row.tag),
    }),
  );

  const response: PostEndorsementResponse = {
    endorsements,
    user: { orcid: session.user.id, name: session.user.name ?? undefined },
  };

  after(() => {
    // Start sending a WebMention, but don't delay the response to do so,
    void sendWebmention({
      identifierType: identifierType,
      doi: doi,
      orcid: orcid,
    });
  });

  return NextResponse.json(response);
};
