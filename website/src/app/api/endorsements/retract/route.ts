import { NextRequest, NextResponse } from "next/server";
import { PostEndorsementRetractionResponse } from "../../../../backend/endpoints";
import { deleteEndorsement } from "../../../../backend/queries/deleteEndorsement";
import { isValidDoi, isValidOrcidId } from "../../../../backend/regexes";

export const GET = async (request: NextRequest) => {
  const searchParams = request.nextUrl.searchParams;
  const doi = searchParams.get("doi");
  const orcidId = searchParams.get("orcid");
  const overrideKey = searchParams.get("retraction_override_key");

  if (
    typeof doi === "undefined" ||
    !isValidDoi(doi) ||
    typeof orcidId === "undefined" ||
    !isValidOrcidId(orcidId) ||
    typeof overrideKey === "undefined" ||
    typeof process.env.RETRACTION_ADMIN_KEY === "undefined" ||
    process.env.RETRACTION_ADMIN_KEY.length < 36 ||
    process.env.RETRACTION_ADMIN_KEY !== overrideKey
  ) {
    return new NextResponse("Invalid parameters.", { status: 403 });
  }
  const identifierType = "doi";
  const pid = `${identifierType}:${doi}`;

  type Unpromisify<T> = T extends Promise<infer U> ? U : never;
  let results: Unpromisify<ReturnType<typeof deleteEndorsement>>;
  try {
    results = await deleteEndorsement(pid, orcidId, overrideKey);
  } catch (e: any) {
    return new NextResponse(e.message, { status: 400 });
  }

  const response: PostEndorsementRetractionResponse = {
    endorsements: results.endorsements,
    retraction: {
      pid: results.retraction.endorsementPid,
      originalEndorsementTimestamp:
        results.retraction.endorsementCreatedAt.toISOString(),
    },
  };

  return NextResponse.json(response);
};
