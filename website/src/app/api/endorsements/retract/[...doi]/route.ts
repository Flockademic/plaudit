import { NextRequest, NextResponse } from "next/server";
import { getServerSession } from "next-auth";
import { PostEndorsementRetractionResponse } from "../../../../../backend/endpoints";
import { deleteEndorsement } from "../../../../../backend/queries/deleteEndorsement";
import { isValidDoi, isValidOrcidId } from "../../../../../backend/regexes";
import { authOptions } from "../../../auth/[...nextauth]/authOptions";

type Params = {
  doi: string[];
};

export const POST = async (request: NextRequest, props: { params: Promise<Params> }) => {
  const session = await getServerSession(authOptions);
  if (!session?.user) {
    return new NextResponse(
      "Please log in to retract an endorsement made in the last five minutes.",
      { status: 403 },
    );
  }

  const orcidId = session.user.id;
  const doi = Array.isArray((await props.params).doi)
    ? (await props.params).doi.join("/").toLowerCase()
    : null;
  const identifierType = "doi";

  if (typeof orcidId !== "string" || !isValidOrcidId(orcidId)) {
    return new NextResponse("No session found.", { status: 401 });
  }
  if (typeof doi !== "string" || !isValidDoi(doi)) {
    return new NextResponse("Not a valid DOI.", { status: 400 });
  }
  const pid = `${identifierType}:${doi}`;

  type Unpromisify<T> = T extends Promise<infer U> ? U : never;
  let results: Unpromisify<ReturnType<typeof deleteEndorsement>>;
  try {
    results = await deleteEndorsement(pid, orcidId);
  } catch (e: any) {
    return new NextResponse(e.message, { status: 400 });
  }

  const response: PostEndorsementRetractionResponse = {
    endorsements: results.endorsements,
    retraction: {
      pid: results.retraction.endorsementPid,
      originalEndorsementTimestamp:
        results.retraction.endorsementCreatedAt.toISOString(),
    },
  };

  return NextResponse.json(response);
};
