import { NextRequest, NextResponse } from "next/server";
import { fetchDoiMetadata } from "../../../../backend/data/doi";
import {
  GetOrcidResponse,
  PostExternalEndorsementRequest,
  PostExternalEndorsementResponse,
} from "../../../../backend/endpoints";
import { isValidDoi, isValidOrcidId } from "../../../../backend/regexes";
import { fetchFromOrcid } from "../../../../backend/data/orcid";
import { getConnection } from "../../../../backend/database";
import { sendWebmention } from "../../../../backend/data/sendWebmention";

// To test:
// curl --request "POST" --header "Content-Type:application/json" --data '{ "secret_key": "<SCIELO_API_KEY HERE>", "doi": "10.1590/scielopreprints.7834", "orcid": "0000-0003-1863-9200" }' http://localhost:8000/api/v1/endorsements

export const POST = async (request: NextRequest) => {
  const data = parseExternalEndorsementBody(await request.json());

  if (data === null) {
    return NextResponse.json({ error: "Bad Request" }, { status: 400 });
  }

  const validKeys = [process.env.SCIELO_API_KEY];

  const hasValidSecretKey = validKeys.some(
    (validKey) =>
      typeof validKey === "string" &&
      validKey.length === 36 &&
      validKey === data.secret_key,
  );
  if (!hasValidSecretKey) {
    return NextResponse.json({ error: "Forbidden" }, { status: 403 });
  }

  const identifierType = "doi";

  const connection = await getConnection();

  // Ensure we know of this work:
  const work = await connection("work_data")
    .first()
    .where("pid", `doi:${data.doi}`);

  if (!work) {
    const fetchedMetadata = await fetchDoiMetadata(data.doi);
    if (fetchedMetadata instanceof Error) {
      return NextResponse.json({ error: "Not Found" }, { status: 404 });
    }
    const _savedWork = await connection("work_data")
      .insert({
        pid: `doi:${data.doi}`,
        url: fetchedMetadata.url || `https://doi.org/${data.doi}`,
        title: fetchedMetadata.title,
        version: 1,
      })
      .onConflict("pid")
      .ignore();
  }

  const endorsementRows = await connection.transaction(async (transaction) => {
    // Find a user by this ORCID iD. If none exists yet, fetch their name
    // from ORCID, then create a new user:
    const user = await transaction("user")
      .first()
      .innerJoin("orcid_account", "orcid_account.userId", "user.id")
      .where("orcid_account.orcid", data.orcid);

    if (!user) {
      const orcidResponse = await fetchFromOrcid("v2.1/" + data.orcid);
      const orcidData: GetOrcidResponse = await orcidResponse.json();

      const savedUserId = await transaction("user")
        .insert({
          name:
            orcidData.person.name === null
              ? data.orcid
              : orcidData.person.name["given-names"].value +
                (orcidData.person.name["family-name"]?.value
                  ? " " + orcidData.person.name["family-name"].value
                  : ""),
          version: 1,
        })
        .returning("id");
      const _savedOrcidAccount = await transaction("orcid_account")
        .insert({
          orcid: data.orcid,
          userId: savedUserId[0].id,
          accessToken: "Unset due to creation via API; will be set after login",
          refreshToken:
            "Unset due to creation via API; will be set after login",
          expiresAt: 0,
          version: 1,
        })
        .onConflict("orcid")
        .ignore();
    }

    const savedEndorsement = await transaction("endorsement")
      .insert({
        pid: `${identifierType}:${data.doi}`,
        userOrcid: data.orcid,
        version: 1,
        // TODO: If we have more than one third party connecting at some point in
        //       the future, we'll have to link their API keys to the source here:
        source: "scielo",
      })
      .onConflict(["pid", "userOrcid"])
      .ignore()
      .returning("id");

    const endorsementRows = await transaction("endorsement")
      .select(
        connection.ref("pid").withSchema("endorsement"),
        connection.ref("userOrcid").withSchema("endorsement").as("orcid"),
      )
      .where("pid", `doi:${data.doi}`);

    return endorsementRows;
  });

  const response: PostExternalEndorsementResponse = {
    endorsements: endorsementRows.map((endorsement) => ({
      doi: endorsement.pid.substring("doi:".length),
      orcid: endorsement.orcid,
    })),
  };

  // Start sending a WebMention, but don't delay the response to do so,
  // i.e. no `await`:
  void sendWebmention({
    identifierType: identifierType,
    doi: data.doi,
    orcid: data.orcid,
  });

  return NextResponse.json(response);
};

function parseExternalEndorsementBody(
  body: unknown | PostExternalEndorsementRequest,
): null | PostExternalEndorsementRequest {
  const source: any = body;
  if (
    typeof source.secret_key === "string" &&
    typeof source.doi === "string" &&
    typeof source.orcid === "string" &&
    isValidDoi(source.doi) &&
    isValidOrcidId(source.orcid)
  ) {
    return {
      secret_key: source.secret_key,
      doi: source.doi.toLowerCase(),
      orcid: source.orcid,
    };
  }
  return null;
}
