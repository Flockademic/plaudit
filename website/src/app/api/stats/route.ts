import { NextRequest, NextResponse } from "next/server";
import { GetStatsResponse } from "../../../backend/endpoints";
import { getConnection } from "../../../backend/database";

/**
 * Prevent Next.js from serving a cached version of this route
 *
 * There is nothing to indicate that this route should be dynamically rendered
 * to Next.js: there are no path parameters, no fetch requests with `{ cache:
 * "no-store" }`, … However, we don't want to show the results from the database
 * queries as executed at the time of the last deploy. Thus, we have to force
 * Next.js to dynamically render.
 * See https://nextjs.org/docs/app/building-your-application/caching#opting-out-1
 */
export const dynamic = "force-dynamic";

export const GET = async (request: NextRequest) => {
  const connection = await getConnection();
  const byPrefixQueryRows = (await connection("endorsement")
    .select([
      connection.raw("COUNT(*) as nr"),
      connection.raw(
        `split_part(split_part(endorsement.pid, ':', 2), '/', 1) as prefix`,
      ),
    ])
    .groupBy("prefix")
    .orderBy("nr", "desc")) as any as Array<{ nr: string; prefix: string }>;

  const endorsementsByPrefix = byPrefixQueryRows.map(({ nr, prefix }) => ({
    nr: parseInt(nr, 10),
    prefix: prefix,
  }));

  const endorsersByMonthQueryResults = (await connection("endorsement")
    .select([
      connection.raw('COUNT(DISTINCT "userOrcid") as nr'),
      connection.raw(`date_trunc('month', endorsement."createdAt") as month`),
    ])
    .groupBy("month")
    .orderBy("month", "desc")) as any as Array<{ nr: string; month: string }>;

  const endorsersByMonth = endorsersByMonthQueryResults.map((rawRow) => ({
    nr: parseInt(rawRow.nr, 10),
    month: new Date(rawRow.month).toISOString(),
  }));

  const endorsementsByMonthQueryResults = (await connection("endorsement")
    .select([
      connection.raw("COUNT(*) as nr"),
      connection.raw(`date_trunc('month', endorsement."createdAt") as month`),
    ])
    .groupBy("month")
    .orderBy("month", "desc")) as any as Array<{ nr: string; month: string }>;

  const endorsementsByMonth = endorsementsByMonthQueryResults.map((rawRow) => ({
    nr: parseInt(rawRow.nr, 10),
    month: new Date(rawRow.month).toISOString(),
  }));

  const nrOfEndorsements = endorsementsByMonth.reduce(
    (acc, month) => acc + month.nr,
    0,
  );

  const nrOfEndorsersQueryResult = (await connection("endorsement").first(
    connection.raw('COUNT(DISTINCT endorsement."userOrcid") as nr'),
  )) as { nr: string };
  const nrOfEndorsers = parseInt(nrOfEndorsersQueryResult.nr, 10);

  const nrOfEndorsedWorksQuery = (await connection("endorsement").first(
    connection.raw("COUNT(DISTINCT endorsement.pid) as nr"),
  )) as { nr: string };
  const nrOfEndorsedWorks = parseInt(nrOfEndorsedWorksQuery.nr, 10);

  const nrOfAdminRetractionsQueryResult = await connection(
    "retracted_endorsement",
  )
    .count("*", { as: "nr" })
    .where("source", "admin");
  const nrOfAdminRetractions =
    typeof nrOfAdminRetractionsQueryResult[0].nr === "string"
      ? parseInt(nrOfAdminRetractionsQueryResult[0].nr, 10)
      : nrOfAdminRetractionsQueryResult[0].nr;

  const nrOfUndosQueryResult = await connection("retracted_endorsement")
    .count("*", { as: "nr" })
    .where("source", "undo");
  const nrOfUndos =
    typeof nrOfUndosQueryResult[0].nr === "string"
      ? parseInt(nrOfUndosQueryResult[0].nr, 10)
      : nrOfUndosQueryResult[0].nr;

  const response: GetStatsResponse = {
    endorsementsByMonth: endorsementsByMonth,
    endorsementsByPrefix: endorsementsByPrefix,
    endorsersByMonth: endorsersByMonth,
    nrOfEndorsers: nrOfEndorsers,
    nrOfEndorsedWorks: nrOfEndorsedWorks,
    nrOfAdminRetractions: nrOfAdminRetractions,
    nrOfUndos: nrOfUndos,
    total: nrOfEndorsements,
  };

  return NextResponse.json(response);
};
