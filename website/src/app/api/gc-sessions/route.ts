import { NextRequest, NextResponse } from "next/server";
import { getConnection } from "../../../backend/database";

const sessionLifeTimeInDays = 14;

export const GET = async (request: NextRequest) => {
  try {
    const connection = await getConnection();

    const results = await connection("session")
      .del()
      .whereRaw(
        `"updatedAt" < now() - '${sessionLifeTimeInDays.toString()} days'::interval`,
      );

    return NextResponse.json(results);
  } catch (e) {
    return NextResponse.json(e, { status: 500 });
  }
};
