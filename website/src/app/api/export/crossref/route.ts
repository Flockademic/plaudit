import { NextRequest, NextResponse } from "next/server";
import {
  hasError,
  sendEndorsementsToCrossRefEventData,
} from "../../../../backend/data/crossref";
import { getConnection } from "../../../../backend/database";

export const GET = async (request: NextRequest) => {
  const connection = await getConnection();

  const latestExport = await connection("cross_ref_export")
    .first()
    .whereNotNull("cross_ref_export.exportedUntilId")
    .orderBy("cross_ref_export.id", "desc");

  const newEndorsementsQuery = connection("endorsement")
    .select([
      connection.ref("id").withSchema("endorsement"),
      connection.ref("pid").withSchema("endorsement"),
      connection.ref("createdAt").withSchema("endorsement"),
      connection.ref("userOrcid").withSchema("endorsement").as("orcid"),
      connection.ref("uuid").withSchema("endorsement"),
    ])
    .whereRaw("endorsement.\"createdAt\" < NOW() - INTERVAL '10 minutes'");

  if (typeof latestExport !== "undefined") {
    newEndorsementsQuery.andWhereRaw("endorsement.id > :latestId", {
      latestId: latestExport.exportedUntilId,
    });
  }

  const newEndorsements = await newEndorsementsQuery;

  const exportResults =
    await sendEndorsementsToCrossRefEventData(newEndorsements);
  const sentEndorsements = exportResults.sentEndorsements.sort(
    (a, b) => a.id - b.id,
  );

  const exportedUntilId =
    sentEndorsements[sentEndorsements.length - 1]?.id ||
    (latestExport ? latestExport.exportedUntilId : undefined);
  if (typeof exportedUntilId === "undefined") {
    console.log("Could not determine last exported endorsement.");
    return new NextResponse("Could not determine last exported endorsement.", {
      status: 500,
    });
  }
  await connection("cross_ref_export").insert({
    exportedEndorsements: sentEndorsements.length,
    exportedUntilId: exportedUntilId,
    version: 1,
  });

  if (hasError(exportResults)) {
    const errorMessage =
      exportResults.error.message && process.env.CROSSREF_CLIENT_SECRET
        ? exportResults.error.message.replace(
            process.env.CROSSREF_CLIENT_SECRET || "",
            "[masked]",
          )
        : "";
    console.log(
      "There was an error exporting endorsements to CrossRef Event Data.",
      errorMessage,
    );
    return new NextResponse(
      `There was an error exporting endorsements to CrossRef Event Data: [${errorMessage}]. The last successful export was at: [${latestExport?.createdAt.toISOString()}].\n\n` +
      `These endorsements were exported successfully: [\n${sentEndorsements.map(sentEndorsement => `\t- [${sentEndorsement.orcid}] - [${sentEndorsement.pid}]`).join("\n")}].`,
      {
        status: 500,
      },
    );
  }

  return NextResponse.json(exportResults);
};
