import { NextRequest, NextResponse } from "next/server";
import { getConnection } from "../../../../backend/database";

export const GET = async (request: NextRequest) => {
  const connection = await getConnection();

  const allEndorsements = await connection("endorsement")
    .select([
      connection.ref("pid").withSchema("endorsement"),
      connection.ref("userOrcid").withSchema("endorsement").as("orcid"),
    ])
    // Endorsers have ten minutes to retract their endorsement, so don't export those yet:
    .whereRaw("endorsement.\"createdAt\" < NOW() - (INTERVAL '10 minutes')");

  const table = allEndorsements.map((endorsement) => {
    return [endorsement.pid, endorsement.orcid].join(",");
  });

  const response = new NextResponse(table.join("\n"));
  response.headers.set("Content-Type", "text/csv");
  return response;
};
