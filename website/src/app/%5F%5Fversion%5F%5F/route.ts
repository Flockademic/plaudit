import { NextRequest, NextResponse } from "next/server";

export const GET = async (request: NextRequest) => {
  return NextResponse.json({
    source: "https://gitlab.com/Flockademic/plaudit/",
    commit: process.env.RENDER_GIT_COMMIT ?? null,
    node_version: process.env.NODE_VERSION ?? null,
    render_instance: process.env.RENDER_INSTANCE_ID ?? null,
    render_service_type: process.env.RENDER_SERVICE_TYPE ?? null,
  });
};
