// Note: this also adds the global stylesheet to the widget, which isn't great.
//       Might want to look at using CSS modules or something for the regular
//       website too.
import { Metadata } from "next";
import "../app.scss";

export const metadata: Metadata = {
  title: {
    template: "%s · Plaudit",
    default: "Plaudit",
  },
  description: process.env.NEXT_PUBLIC_APP_TITLE,
  keywords: process.env.NEXT_PUBLIC_APP_KEYWORDS?.split(",").join(", "),
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
