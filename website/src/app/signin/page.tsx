import { getServerSession } from "next-auth";
import { SignInRedirect } from "./SignInRedirect";
import { authOptions } from "../api/auth/[...nextauth]/authOptions";

const SignInPage = async () => {
  const session = await getServerSession(authOptions);

  return <SignInRedirect session={session} />;
};

export default SignInPage;
