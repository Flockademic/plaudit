"use client";

import { Session } from "next-auth";
import { signIn } from "next-auth/react";
import { useEffect } from "react";

interface Props {
  session?: Session | null;
}
export const SignInRedirect = (props: Props) => {
  useEffect(() => {
    if (!props.session) {
      signIn("orcid");
    }
    if (props.session) {
      window.close();
    }
  });

  if (props.session) {
    return (
      <>
        <p className="mx-auto my-10 max-w-screen-md text-xl text-blue-700">
          You are now signed in. Please close this page to complete the
          endorsement.
        </p>
      </>
    );
  }

  return <>Signing in&hellip;</>;
};
