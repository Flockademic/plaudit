import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Demo",
};

const DemoPage: React.FC = () => {
  const scriptTag = `<script async src="${process.env.NEXT_PUBLIC_WEBSITE_URL}/embed/endorsements.js"
  data-pid="doi:10.7554/elife.36263"
  data-embedder-id="plaudit"
></script>`;

  return (
    <>
      <header className="flex flex-col gap-y-3 mx-10 lg:mx-20 xl:mx-32 my-10 border-l-8 border-blue-500 pl-5">
        <h1 className="text-2xl md:text-3xl text-blue-500">Demo</h1>
      </header>
      <section className="flex flex-col gap-y-10 px-10 lg:px-20 xl:px-32 py-10">
        <div className="bg-slate-300 p-5 rounded-sm flex flex-col gap-5">
          <pre>{scriptTag}</pre>
          {/* dangerouslySetInnerHTML is used to avoid Next.js moving the <script> tag elsewhere on the page */}
          <div dangerouslySetInnerHTML={{ __html: scriptTag }} />
        </div>
      </section>
    </>
  );
};

export default DemoPage;
