import { Metadata } from "next";
import { fetchFromOrcid } from "../../../../backend/data/orcid";
import { GetOrcidResponse } from "../../../../backend/endpoints";
import { getEndorsementsForOrcid } from "../../../../backend/queries/endorsementsForOrcid";
import { isValidOrcidId } from "../../../../backend/regexes";
import { EndorserDetails } from "../../../../components/EndorserDetails";
import { formatName } from "../../../../functions/formatName";

type Params = {
  orcid: string;
};

export async function generateMetadata(props: {
  params: Promise<Params>;
}): Promise<Metadata> {
  const orcidId = (await props.params).orcid;
  if (!isValidOrcidId(orcidId)) {
    return {};
  }

  const endorsements = await getEndorsementsForOrcid(orcidId);
  const title = `Endorsements by ${formatName(endorsements.name ?? endorsements.orcid)}`;
  return {
    title: title,
    alternates: {
      types: {
        "application/rss+xml": `${process.env.REACT_APP_URL}/api/endorsers/${endorsements.orcid}.rss`,
      },
    },
  };
}

const EndorserPage = async (props: { params: Promise<Params> }) => {
  const orcidId = (await props.params).orcid;
  if (!isValidOrcidId(orcidId)) {
    return (
      <div className="px-10 lg:px-20 xl:px-32 py-10">
        <div className="bg-yellow-200 p-5">
          This endorser could not be found.
        </div>
      </div>
    );
  }

  try {
    const orcidResponse = await fetchFromOrcid("v2.1/" + orcidId);
    const orcidData: GetOrcidResponse = await orcidResponse.json();
    const endorsements = await getEndorsementsForOrcid(orcidId);
    const endorser = {
      orcid: endorsements.orcid,
      name: endorsements.name,
      endorsements: endorsements.endorsements.map((endorsement) => ({
        identifierType: endorsement.pid.substring(
          0,
          endorsement.pid.indexOf(":"),
        ) as "doi",
        identifier: endorsement.pid.substring(endorsement.pid.indexOf(":") + 1),
        title: endorsement.title,
        url: endorsement.url,
        tags: endorsement.tags,
        timestamp: endorsement.createdAt.toISOString(),
      })),
    };

    return <EndorserDetails endorser={endorser} orcidInfo={orcidData} />;
  } catch (e) {
    return (
      <div className="px-10 lg:px-20 xl:px-32 py-10">
        <div className="bg-yellow-200 p-5">
          This endorser could not be found.
        </div>
      </div>
    );
  }
};

export default EndorserPage;
