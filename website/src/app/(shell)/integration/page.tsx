import { Metadata } from "next";

const snippet = `<script async src="https://plaudit.pub/embed/endorsements.js"
        data-embedder-id="integrationToken"
></script>`;

export const metadata: Metadata = {
  title: "Integration",
};

const IntegrationPage: React.FC = () => {
  return (
    <>
      <header className="flex flex-col gap-y-3 mx-10 lg:mx-20 xl:mx-32 my-10 border-l-8 border-blue-500 pl-5">
        <h1 className="text-2xl md:text-3xl text-blue-500">Integration</h1>
        <span className="text-xl">
          How to add the Plaudit widget to your website
        </span>
      </header>
      <section className="px-10 lg:px-20 xl:px-32 py-10">
        <div className="text-lg leading-relaxed">
          Adding the Plaudit widget to webpages with academic works takes two
          steps:
          <ol className="list-decimal pl-10 py-5">
            <li>Obtain an integration token</li>
            <li>Insert an HTML snippet to the page</li>
          </ol>
        </div>
        <h2 className="text-xl md:text-2xl text-blue-500">
          Obtain an integration token
        </h2>
        <p className="text-lg py-4 max-w-screen-lg leading-relaxed">
          To make sure Plaudit can handle the load, we are gradually rolling out
          integrations. Thus, at this point in time it is required to{" "}
          <a href="mailto:integrate@plaudit.pub" title="Send us an email">
            contact us
          </a>{" "}
          if you are interested in integrating the Plaudit widget into your
          website. When your integration is approved, we will ask you for the
          host names you want to display the widget at (e.g.{" "}
          <code>example.com</code> and <code>dev.example.com</code>). You will
          then receive a token that can be used to integrate the widget at thost
          host names.
        </p>
        <h2 className="text-lg md:text-2xl text-blue-500">
          Insert the integration snippet
        </h2>
        <p className="text-lg py-4 max-w-screen-lg leading-relaxed">
          To add the snippet to a page, insert the following HTML:
        </p>
        <pre className="max-w-screen-lg p-5 bg-blue-50">
          <code>{snippet}</code>
        </pre>
        <p className="text-lg py-4 max-w-screen-lg leading-relaxed">
          Make sure to replace <code>integrationToken</code> with the
          integration token you obtained in the previous step.
        </p>
        <p className="text-lg py-4 max-w-screen-lg leading-relaxed">
          The widget will be placed in the DOM in front of this{" "}
          <code>&lt;script&gt;</code> tag. It will automatically detect which
          work the page is about, provided that its DOI is listed in the page&apos;s
          metadata in one of the formats{" "}
          <a
            href="https://www.npmjs.com/package/get-dois#user-content-supported-meta-tags"
            title="npm package `get-dois` - supported metadata formats"
          >
            supported by get-dois
          </a>
          . (This should cover most commonly-used formats.)
        </p>
      </section>
    </>
  );
};

export default IntegrationPage;
