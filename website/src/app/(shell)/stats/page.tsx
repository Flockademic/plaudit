import { Metadata } from "next";
import Link from "next/link";
import { formatName } from "../../../functions/formatName";
import { getRecentEndorsers } from "../../../backend/queries/recentEndorsements";
import {
  getEndorsementssByMonth,
  getEndorsersByMonth,
  getNumberOfAdminRetractions,
  getNumberOfEndorsedWorks,
  getNumberOfEndorsers,
  getNumberOfUndos,
} from "../../../backend/queries/stats";

/**
 * Prevent Next.js from serving a cached version of this page
 *
 * There is nothing to indicate that this page should be dynamically rendered to
 * Next.js: there are no path parameters, no fetch requests with `{ cache:
 * "no-store" }`, … However, we don't want to show the results from the database
 * queries as executed at the time of the last deploy. Thus, we have to force
 * Next.js to dynamically render.
 * See https://nextjs.org/docs/app/building-your-application/caching#opting-out-1
 */
export const dynamic = "force-dynamic";

export const metadata: Metadata = {
  title: "Stats",
};

const StatsPage = async () => {
  const [
    recentEndorsers,
    endorsementsByMonth,
    endorsersByMonth,
    nrOfEndorsedWorks,
    nrOfEndorsers,
    nrOfAdminRetractions,
    nrOfUndos,
  ] = await Promise.all([
    getRecentEndorsers(),
    getEndorsementssByMonth(),
    getEndorsersByMonth(),
    getNumberOfEndorsedWorks(),
    getNumberOfEndorsers(),
    getNumberOfAdminRetractions(),
    getNumberOfUndos(),
  ]);
  const nrOfEndorsements = endorsementsByMonth.reduce(
    (acc, month) => acc + month.nr,
    0,
  );

  return (
    <>
      <header className="flex flex-col gap-y-3 mx-10 lg:mx-20 xl:mx-32 my-10 border-l-8 border-blue-500 pl-5">
        <h1 className="text-2xl md:text-3xl text-blue-500">Statistics</h1>
      </header>
      <section className="flex flex-col gap-y-10 px-10 lg:px-20 xl:px-32 py-10">
        <div className="flex flex-wrap gap-5">
          <div className="basis-40 grow flex flex-col text-center items-center justify-center">
            <p className="text-sm uppercase">Endorsements (month/total)</p>
            <p className="text-3xl font-bold">
              {endorsementsByMonth[0]?.nr ?? 0} / {nrOfEndorsements}
            </p>
          </div>
          <div className="basis-40 grow flex flex-col text-center items-center justify-center">
            <p className="text-sm uppercase">Works endorsed</p>
            <p className="text-3xl font-bold">{nrOfEndorsedWorks}</p>
          </div>
          <div className="basis-40 grow flex flex-col text-center items-center justify-center">
            <p className="text-sm uppercase">Endorsers (month/total)</p>
            <p className="text-3xl font-bold">
              {endorsersByMonth[0]?.nr ?? 0} / {nrOfEndorsers}
            </p>
          </div>
          <div className="basis-40 grow flex flex-col text-center items-cente justify-center">
            <p className="text-sm uppercase">Retractions (admin/undo)</p>
            <p className="text-3xl font-bold">
              {nrOfAdminRetractions ?? 0} / {nrOfUndos ?? 0}
            </p>
          </div>
        </div>
        <div className="flex flex-col gap-y-5">
          <h2 className="text-xl md:text-2xl">Latest endorsers</h2>
          <table className="table-auto">
            <thead>
              <tr>
                <th className="border-2 p-2">Endorser</th>
                <th className="border-2 p-2">Endorsed work</th>
                <th className="border-2 p-2">Endorsed at</th>
              </tr>
            </thead>
            <tbody>
              {recentEndorsers.map((endorser) => {
                const dateFormatter = new Intl.DateTimeFormat("en-GB", {
                  dateStyle: "full",
                });
                return endorser.endorsements.map((endorsement, i) => {
                  if (i === 0) {
                    return (
                      <tr key={endorser.orcid + i}>
                        <td
                          className="border-2 p-2"
                          rowSpan={endorser.endorsements.length}
                        >
                          <Link href={`/endorser/${endorser.orcid}`}>
                            <bdi>
                              {formatName(endorser.name || endorser.orcid)}
                            </bdi>
                          </Link>
                        </td>
                        <td className="border-2 p-2">
                          <a href={`https://doi.org/${endorsement.identifier}`}>
                            {endorsement.identifier}
                          </a>
                          {endorsement.source === "scielo" ? (
                            <> (via SciELO)</>
                          ): endorsement.source === "website" ? <> (via website)</> : null}
                        </td>
                        <td className="border-2 p-2">
                          {dateFormatter.format(
                            new Date(endorsement.createdAt),
                          )}
                        </td>
                      </tr>
                    );
                  }
                  return (
                    <tr key={endorser.orcid + i}>
                      <td className="border-2 p-2">
                        <a href={`https://doi.org/${endorsement.identifier}`}>
                          {endorsement.identifier}
                        </a>
                      </td>
                      <td className="border-2 p-2">
                        {dateFormatter.format(new Date(endorsement.createdAt))}
                      </td>
                    </tr>
                  );
                });
              })}
            </tbody>
          </table>
        </div>
      </section>
    </>
  );
};

export default StatsPage;
