import { Metadata } from "next";
import Link from "next/link";
import { MdOutlineArrowDownward } from "react-icons/md";
import { TestimonialCarousel } from "../../components/TestimonialCarousel";

export const metadata: Metadata = {
  title: {
    absolute: "Plaudit · Open endorsements from the academic community",
  },
};

const HomePage: React.FC = () => {
  return (
    <>
      <header className="flex flex-col gap-10 py-10 md:py-24 px-10 lg:px-20 xl:px-32 max-w-screen-lg">
        <h1 className="text-4xl md:text-5xl font-bold text-blue-500">
          Open endorsements from the academic community
        </h1>
        <div className="flex flex-wrap items-center gap-5 max-w-screen-sm text-center text-lg font-bold">
          <Link
            href="/integration/"
            className="grow border-4 border-blue-500 bg-blue-500 text-xl text-white hover:bg-transparent hover:text-blue-500 focus:outline outline-4 outline-blue-500 outline-offset-2 py-2 px-10 rounded-md"
          >
            Integrate
          </Link>
          <a
            href="#concept"
            className="grow flex items-center justify-center gap-1 border-4 border-transparent text-xl text-blue-500 text-underline hover:border-blue-500 focus:outline outline-4 outline-blue-500 outline-offset-2 py-2 px-10 rounded-md"
          >
            Learn more <MdOutlineArrowDownward aria-hidden />
          </a>
        </div>
      </header>
      <div
        id="concept"
        className="flex flex-wrap gap-16 py-10 md:py-24 px-10 lg:px-20 xl:px-32"
      >
        <div className="grow basis-96 flex flex-wrap gap-16">
          <div className="grow basis-48 flex flex-col gap-2">
            <h2 className="text-2xl font-bold">Light-weight endorsements</h2>
            <p className="text-lg">
              Plaudit links researchers, identified by their{" "}
              <a
                href="https://orcid.org"
                className="border-b-2 border-dashed hover:border-solid"
              >
                ORCID
              </a>
              , to research they endorse, identified by its{" "}
              <a href="https://www.doi.org/">DOI</a>.
            </p>
          </div>
          <div className="grow basis-48 flex flex-col gap-2">
            <h2 className="text-2xl font-bold">Easy to integrate</h2>
            <p className="text-lg">
              Do you publish research? Your development team or service provider
              can trivially integrate Plaudit using{" "}
              <Link
                href="/integration"
                className="border-b-2 border-dashed hover:border-solid"
              >
                a simple code snippet
              </Link>
              .
            </p>
          </div>
        </div>
        <div className="grow basis-96 flex flex-wrap gap-16">
          <div className="grow basis-48 flex flex-col gap-2">
            <h2 className="text-2xl font-bold">Authoritative</h2>
            <p className="text-lg">
              Publisher-independent and provided by known and trusted members of
              the academic community, endorsements provide credibility for
              valuable research.
            </p>
          </div>
          <div className="grow basis-48 flex flex-col gap-2">
            <h2 className="text-2xl font-bold">Open data</h2>
            <p className="text-lg">
              Plaudit is built on open infrastructure, and endorsements are fed
              into{" "}
              <a
                href="https://www.eventdata.crossref.org/guide/sources/experimental/#experimental-source-plaudit"
                title="CrossRef Event Data documentation on Plaudit"
                className="border-b-2 border-dashed hover:border-solid"
              >
                CrossRef Event Data
              </a>{" "}
              and exported to{" "}
              <a
                href="https://archive.org/details/@plaudit_pub"
                className="border-b-2 border-dashed hover:border-solid"
              >
                the Internet Archive
              </a>
              .
            </p>
            <p className="text-lg">
              We&apos;re{" "}
              <a
                href="https://gitlab.com/Flockademic/plaudit/"
                title="Plaudit source code"
                className="border-b-2 border-dashed hover:border-solid"
              >
                open source
              </a>{" "}
              and not for profit.
            </p>
          </div>
        </div>
      </div>
      <div className="flex flex-wrap gap-16 py-10 md:py-24 px-10 lg:px-20 xl:px-32">
        <TestimonialCarousel
          testimonials={[
            {
              quote: (
                <p>
                  An important innovation in peer review and recommendation to
                  recognise the value of scholarly content.
                </p>
              ),
              author: (
                <>
                  <a
                    href="http://briannosek.com"
                    className="border-b-2 border-dashed hover:border-solid"
                  >
                    Brian Nosek
                  </a>
                  , Professor, University of Virginia, and Executive Director{" "}
                  <a
                    href="https://cos.io"
                    className="border-b-2 border-dashed hover:border-solid"
                  >
                    Center for Open Science
                  </a>
                  .
                </>
              ),
            },
            {
              quote: (
                <p>
                  Plaudit&apos;s decentralisation of research assessment is sorely
                  needed: finally researchers will be able to rely on
                  endorsements by colleagues whose interests and evaluations
                  align with their own.
                </p>
              ),
              author: (
                <>
                  <a
                    href="https://www.sydney.edu.au/science/about/our-people/academic-staff/alex-holcombe.html"
                    className="border-b-2 border-dashed hover:border-solid"
                  >
                    Alex Holcombe
                  </a>
                  , Professor, University of Sydney.
                </>
              ),
            },
            {
              quote: (
                <p>
                  This is the sort of small-scale, large impact open source for
                  Open Access project to which more people should be paying
                  attention.
                </p>
              ),
              author: (
                <>
                  <a
                    href="https://elifesciences.org/profiles/jjcp3cgs"
                    className="border-b-2 border-dashed hover:border-solid"
                  >
                    Giuliano Maciocci
                  </a>
                  , Head of Product and UX at{" "}
                  <a
                    href="https://elifesciences.org/"
                    className="border-b-2 border-dashed hover:border-solid"
                  >
                    eLife
                  </a>
                  .
                </>
              ),
            },
          ]}
        />
      </div>
      <div className="flex flex-col gap-8 py-10 md:py-24 px-10 lg:px-20 xl:px-32 max-w-screen-xl mx-auto">
        <h2 className="text-3xl font-bold">Work with us</h2>
        <p className="text-2xl">
          We&apos;re looking for innovators who want to work with us to{` `}
          <Link
            href="/integration/"
            title="High-level instructions on how to integrate Plaudit into your platform"
          >
            integrate
          </Link>
          {` `}
          Plaudit into their preprint server or journal, highlighting
          endorsements in the context of relevant academic content. Is this you?
        </p>
        <p>
          <a
            className="grow border-4 border-blue-500 bg-blue-500 text-white text-2xl hover:bg-transparent hover:text-blue-500 focus:outline outline-4 outline-blue-500 outline-offset-2 py-2 px-10 rounded-md"
            href="mailto:integrate@plaudit.pub"
          >
            Contact us
          </a>
        </p>
      </div>
    </>
  );
};

export default HomePage;
