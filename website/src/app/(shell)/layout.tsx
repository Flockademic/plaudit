import Layout from "../../components/Layout";
import { SessionProvider } from "../../components/contexts/SessionProvider";
import { getServerSession } from "../../functions/getServerSession";

export default async function ShellLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const session = await getServerSession();

  return <SessionProvider session={session}><Layout>{children}</Layout></SessionProvider>;
}
