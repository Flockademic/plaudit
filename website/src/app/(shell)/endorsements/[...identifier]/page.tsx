import { Metadata } from "next";
import { getServerSession } from "next-auth";
import { Doi } from "knex/types/tables";
import { getEndorsementsForDoi } from "../../../../backend/queries/endorsementsForDoi";
import { isValidDoi } from "../../../../backend/regexes";
import { WorkDetails } from "../../../../components/WorkDetails";
import { GetEndorsementsWithAuthorsResponse } from "../../../../backend/endpoints";
import { authOptions } from "../../../api/auth/[...nextauth]/authOptions";
import { isFetchError } from "../../../../functions/fetchError";

type Params = {
  identifier: string[];
};

function getDoiFromParams(params: Params): Doi | null {
  const identifierParam = params.identifier.join("/");
  const [identifierType, ...identifierParts] = identifierParam.split(
    encodeURIComponent(":"),
  );
  const identifier = identifierParts.join(":");

  if (identifierType !== "doi" || !identifier || !isValidDoi(identifier)) {
    return null;
  }

  return identifier;
}

export async function generateMetadata(props: {
  params: Promise<Params>;
}): Promise<Metadata> {
  const doi = getDoiFromParams((await props.params));

  if (doi === null) {
    return {};
  }

  const data = await getEndorsementsForDoi(doi);
  return {
    title: `Endorsements for "${data.workData.title}"`,
  };
}

const EndormentsPage = async (props: { params: Promise<Params> }) => {
  const doi = getDoiFromParams((await props.params));

  if (doi === null) {
    return (
      <div className="px-10 lg:px-20 xl:px-32 py-10">
        <div className="bg-yellow-200 p-5">This work could not be found.</div>
      </div>
    );
  }

  const session = await getServerSession(authOptions);

  const data = await getEndorsementsForDoi(doi, { withAuthors: true }).catch(async e => {
    if (isFetchError(e) && e.status === 404) {
      return null;
    }
    throw e;
    // return null;
  });

  if (data === null) {
    return (
      <>
        <header className="flex flex-col gap-y-3 mx-10 lg:mx-20 xl:mx-32 my-10 border-l-8 border-blue-500 pl-5">
          <h1 className="text-2xl md:text-3xl text-blue-500">Not found</h1>
        </header>
        <section className="px-10 lg:px-20 xl:px-32 py-10">
          Unfortunately, we could not retrieve data about this work. Please try{" "}
          <a
            href={`https://doi.org/${doi}`}
            className="text-blue-500 hover:text-blue-700 underline cursor-pointer"
          >
            the original publication
          </a>
          .
        </section>
      </>
    );
  }

  const workData: GetEndorsementsWithAuthorsResponse["workData"] = {
    ...data.workData,
    authors:
      data.workData.authors?.map((author) => ({
        orcid: author.orcid ?? undefined,
        name: author.name,
      })) ?? [],
  };

  return (
    <WorkDetails
      endorsements={data.endorsements}
      workData={workData}
      session={session ?? undefined}
      doi={doi}
    />
  );
};

export default EndormentsPage;
