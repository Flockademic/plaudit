import { Metadata } from "next";
import Link from "next/link";
import Layout from "../components/Layout";
import { SessionProvider } from "../components/contexts/SessionProvider";

export const metadata: Metadata = {
  title: "Page not found",
};

const NotFoundPage: React.FC = () => {
  return (
    <SessionProvider>
      <Layout>
        <section className="flex flex-col gap-16 py-10 md:py-24 px-10 lg:px-20 xl:px-32">
          <h2 className="text-2xl font-bold">Page not found</h2>
          <p>
            Unfortunately, this page could not be found. Perhaps you can find what
            you&apos;re looking for at <Link href="/">the homepage</Link>?
          </p>
        </section>
      </Layout>
    </SessionProvider>
  );
};

export default NotFoundPage;
