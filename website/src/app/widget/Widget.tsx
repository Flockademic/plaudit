"use client";

import "iframe-resizer/js/iframeResizer.contentWindow.min.js";
import { Session } from "next-auth";
import { SessionProvider } from "next-auth/react";
import { useState } from "react";
import {
  EndorsementType,
  GetEndorsementsResponse,
  PostEndorsementRequest,
  PostEndorsementResponse,
  PostEndorsementRetractionResponse,
} from "../../backend/endpoints";
import { View } from "../../components/widget/View";
import { Tag } from "../../components/widget/Tag";
import { SignIn } from "../../components/widget/SignIn";

export interface Props {
  doi: string | null;
  enforceAuthentication: boolean;
  user: GetEndorsementsResponse["user"] | null;
  endorsements: GetEndorsementsResponse["endorsements"] | null;
  session?: Session | null;
  workData?: GetEndorsementsResponse["workData"] | null;
}

export const Widget: React.FC<Props> = (props) => {
  const [phase, setPhase] = useState<
    | "default"
    | "tagging"
    | "awaitEndorsement"
    | "undoable"
    | "awaitUndo"
    | "endorsed"
  >("default");
  const [endorsements, setEndorsements] = useState(
    props.endorsements ?? undefined,
  );
  const [user, setUser] = useState(props.user ?? undefined);

  const doi = props.doi;

  if (
    !props.endorsements ||
    !props.workData ||
    endorsements === undefined ||
    !doi
  ) {
    return null;
  }

  const authenticate = async () => {
    if (typeof document.requestStorageAccess === "function") {
      try {
        await document.requestStorageAccess();
      } catch (e) {
        // Denied. Don't do anything, just not work.
        // TODO: Handle more gracefully.
      }
    }

    const authPromise = new Promise<void>((resolve) => {
      const authWindow = window.open(
        process.env.NEXT_PUBLIC_BACKEND_URL + "/signin",
      );

      if (!authWindow) {
        return;
      }

      // This is somewhat hackish, however:
      //  - We can't listen to the `unload` event, because that is fired as soon as we redirect to
      //    orcid.org.
      //  - We can't listen to the `close` event because the window API does not actually fire that:
      //    https://developer.mozilla.org/en-US/docs/Web/Events/close
      // While it is somewhat of a workaround, it is unlikely to stop working, unless the `.closed`
      // property gets removed - which I wouldn't expect to happen, given that it is the recommended
      // way to check for whether a window is open on MDN:
      // https://developer.mozilla.org/en-US/docs/Web/API/Window/open#FAQ
      const timer = setInterval(() => {
        if (authWindow.closed) {
          clearInterval(timer);

          // When the window is closed, the user has hopefully authenticated,
          // in which cases future requests to the server will include that user's details.
          resolve();
        }
      }, 200);
    });

    return authPromise;
  };

  const handleSignIn = async () => {
    if (user) {
      return;
    }

    await authenticate();

    // After authenticating, re-fetching endorsement data should include user details:
    const endorsementData = await fetchEndorsementData(doi);
    setUser(endorsementData?.user);
    setEndorsements(endorsementData?.endorsements);
  };

  if (props.enforceAuthentication && !user) {
    return <SignIn handleSignIn={handleSignIn} />;
  }

  const endorse = async (tags: EndorsementType[]) => {
    setPhase("awaitEndorsement");
    const request: PostEndorsementRequest = {
      tags: tags,
    };
    const response = await fetch(`/api/endorsements/${props.doi}`, {
      body: JSON.stringify(request),
      credentials: "same-origin",
      headers: new Headers({
        "Content-Type": "application/json",
      }),
      method: "POST",
    });

    if (!response.ok) {
      setPhase("default");
      return;
    }

    const data: PostEndorsementResponse = await response.json();

    setEndorsements(data.endorsements);
    setUser(data.user);
    setPhase((prevPhase) =>
      prevPhase === "awaitEndorsement" ? "undoable" : prevPhase,
    );

    setTimeout(
      () => {
        setPhase((prevPhase) =>
          prevPhase === "undoable" ? "endorsed" : prevPhase,
        );
        // The back-end will accept retractions for 10 minutes to avoid race conditions,
        // but we just allow undoing for 5 minutes:
      },
      5 * 60 * 1000,
    );
  };

  const undo = async () => {
    setPhase("awaitUndo");
    const response = await fetch(`/api/endorsements/retract/${props.doi}`, {
      credentials: "same-origin",
      headers: new Headers({
        "Content-Type": "application/json",
      }),
      method: "POST",
    });

    const data: PostEndorsementRetractionResponse = await response.json();

    setEndorsements(data.endorsements);
    setPhase((prevPhase) =>
      prevPhase === "awaitUndo" ? "default" : prevPhase,
    );
  };

  const setTags = async (tags: EndorsementType[]) => {
    if (!user) {
      await authenticate();

      // After authenticating, re-fetching endorsement data should include user details:
      const endorsementData = await fetchEndorsementData(doi);
      const user = endorsementData?.user;
      setUser(endorsementData?.user);
      setEndorsements(endorsementData?.endorsements);
      if (
        endorsementData &&
        user &&
        endorsementData.endorsements.findIndex(
          (e) => e.orcid === user.orcid,
        ) !== -1
      ) {
        // Already endorsed; do nothing.
        setPhase("endorsed");
        return;
      }
    }

    return endorse(tags);
  };

  if (phase === "tagging") {
    return <Tag setTags={setTags} />;
  }

  const handleEndorsement = () => {
    setPhase("tagging");
  };

  const handleUndo = async () => {
    if (!user) {
      await authenticate();
    }
    return undo();
  };

  return (
    <SessionProvider session={props.session}>
      <View
        endorsements={endorsements}
        user={user}
        workData={props.workData}
        handleEndorsement={handleEndorsement}
        handleUndo={phase === "undoable" ? handleUndo : undefined}
        justEndorsed={phase === "undoable" || phase === "endorsed"}
        doi={doi}
      />
    </SessionProvider>
  );
};

type FetchEndorsementDataOptions = Partial<{
  embedderId: string;
  referrer: string;
  cookie: string;
}>;
async function fetchEndorsementData(
  doi: string,
  options: FetchEndorsementDataOptions = {},
): Promise<GetEndorsementsResponse | null> {
  const queryParams: { [key: string]: string | undefined } = {
    embedder_id: options.embedderId,
    referrer: options.referrer,
  };
  const queryString = Object.keys(queryParams)
    .filter((key) => typeof queryParams[key] === "string")
    .map((key) => `${key}=${encodeURIComponent(queryParams[key]!)}`)
    .join("&");

  const endorsementsResponse = await fetch(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `/api/endorsements/${doi}?${queryString}`,
    {
      credentials: "same-origin",
      headers: options.cookie ? { Cookie: options.cookie } : undefined,
    },
  );

  return endorsementsResponse.ok ? await endorsementsResponse.json() : null;
}
