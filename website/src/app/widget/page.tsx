import { Session, getServerSession } from "next-auth";
import { GetEndorsementsResponse } from "../../backend/endpoints";
import { getEndorsementsForDoi } from "../../backend/queries/endorsementsForDoi";
import { isValidDoi } from "../../backend/regexes";
import { Widget } from "./Widget";
import { authOptions } from "../api/auth/[...nextauth]/authOptions";

type SearchParams = Partial<{
  force_auth?: "true";
  pid?: string;
}>;

const WidgetPage = async (props: { searchParams: Promise<SearchParams> }) => {
  const enforceAuthentication = (await props.searchParams).force_auth === "true";

  const pid = (await props.searchParams).pid;
  const doi = typeof pid === "string" ? pid.substring("doi:".length) : "";
  if (
    typeof pid !== "string" ||
    pid.substring(0, "doi:".length) !== "doi:" ||
    !isValidDoi(doi)
  ) {
    return null;
  }

  const session = await getServerSession(authOptions);
  const user: GetEndorsementsResponse["user"] | null = session?.user
    ? {
        orcid: session.user.id,
        name: getName(session.user) as string | undefined,
      }
    : null;
  if (user) {
    if (!user.name) {
      delete user.name;
    }
  }

  try {
    const endorsementData = await getEndorsementsForDoi(doi);
    return (
      <Widget
        doi={doi}
        user={user}
        enforceAuthentication={enforceAuthentication}
        endorsements={endorsementData.endorsements}
        session={session}
        workData={{
          ...endorsementData.workData,
          authors: (endorsementData.workData.authors ?? []).map((author) => {
            // Next.js can't serialise values explicitly set to `undefined`,
            // so we delete those values instead:
            if (typeof author.orcid === "undefined") {
              delete author.orcid;
            }
            return author;
          }),
        }}
      />
    );
  } catch (e) {
    return null;
  }
};

export default WidgetPage;

function getName(user: Session["user"]): string | void {
  if (user.name) {
    return user.name;
  }
  if (user.family_name) {
    if (user.given_name) {
      return `${user.given_name} ${user.family_name}`;
    }
    return user.family_name;
  }
  if (user.given_name) {
    return user.given_name;
  }
  return;
}
