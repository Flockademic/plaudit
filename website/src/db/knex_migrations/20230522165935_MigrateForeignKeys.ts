import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex("retracted_endorsement")
    .update("endorsementUserOrcid", "0000-0002-0911-7411")
    .whereNull("endorsementUserOrcid");
  await Promise.all([
    knex.schema.table("endorsement", (table) => {
      table.foreign("userOrcid").references("user.orcid").onDelete("CASCADE");
      table.foreign("pid").references("work_data.pid").onUpdate("CASCADE");
    }),
    knex.schema.table("endorsement_tags_tag", (table) => {
      table
        .foreign("endorsementId")
        .references("endorsement.id")
        .onDelete("CASCADE");
      table.foreign("tagId").references("tag.id").onDelete("CASCADE");
    }),
    knex.schema.table("retracted_endorsement", (table) => {
      table.foreign("endorsementUserOrcid").references("user.orcid");
      table
        .foreign("endorsementPid")
        .references("work_data.pid")
        .onUpdate("CASCADE");
      table.dropColumn("pid");
      table.dropNullable("endorsementUserOrcid");
    }),
    knex.schema.table("retracted_endorsement_endorsement_tags_tag", (table) => {
      table
        .foreign("retractedEndorsementId")
        .references("retracted_endorsement.id")
        .onDelete("CASCADE");
      table.foreign("tagId").references("tag.id").onDelete("CASCADE");
    }),
    knex.schema.table("cross_ref_export", (table) => {
      table.foreign("exportedUntilId").references("endorsement.id");
    }),
  ]);
}

export async function down(knex: Knex): Promise<void> {
  await Promise.all([
    knex.schema.table("cross_ref_export", (table) => {
      table.dropForeign("exportedUntilId");
    }),
    knex.schema.table("retracted_endorsement_endorsement_tags_tag", (table) => {
      table.dropForeign("retractedEndorsementId");
      table.dropForeign("tagId");
    }),
    knex.schema.table("retracted_endorsement", (table) => {
      table.setNullable("endorsementUserOrcid");
      table.string("pid").nullable();
      table.dropForeign("endorsementUserOrcid");
      table.dropForeign("endorsementPid");
    }),
    knex.schema.table("endorsement_tags_tag", (table) => {
      table.dropForeign("endorsementId");
      table.dropForeign("tagId");
    }),
    knex.schema.table("endorsement", (table) => {
      table.dropForeign("userOrcid");
      table.dropForeign("pid");
    }),
  ]);
  await knex("retracted_endorsement")
    .update("endorsementUserOrcid", null)
    .whereIn("id", [3, 4])
    .andWhere("endorsementUserOrcid", "0000-0002-0911-7411");
}
