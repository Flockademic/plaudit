import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  // Something went wrong with a CrossRef export at some point. See
  // https://api.eventdata.crossref.org/v1/events?mailto=playing_around@plaudit.pub&source=plaudit&from-collected-date=2024-04-07&rows=47
  // At the time of writing this migration, event 0 (0009-0002-9048-5955
  // endorsing 10.31235/osf.io/z94kf) was the last recorded endorsement in the
  // cross_ref_export table. That means that the next endorsement to be exported
  // (0000-0002-7569-8648 endorsing 10.31235/osf.io/wkgjp) was already known at
  // CrossRef (event 1 at the above link), causing a a 409 Conflict.
  // (The two events were also sent respectively 2024-04-07 and 2024-05-04;
  // possibly the API was experiencing downtime in between?)
  // The last event that was exported to the CrossRef API at the time of writing
  // was event 45 (0000-0003-3951-7990 endorsing 10.31235/osf.io/2y537), so
  // to make exports work again, this migration records that that event has already been
  // exported, so that the next migration will then continue with the next
  // endorsement.
  const lostExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0003-3951-7990")
    .andWhere("pid", "doi:10.31235/osf.io/2y537");

  if (lostExportedEndorsement) {
    await knex.table("cross_ref_export").insert({
      exportedEndorsements: 1,
      version: 1,
      exportedUntilId: lostExportedEndorsement.id,
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  const lostExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0003-3951-7990")
    .andWhere("pid", "doi:10.31235/osf.io/2y537");
  if (lostExportedEndorsement) {
    await knex
      .table("cross_ref_export")
      .delete()
      .where("exportedUntilId", lostExportedEndorsement.id);
  }
}
