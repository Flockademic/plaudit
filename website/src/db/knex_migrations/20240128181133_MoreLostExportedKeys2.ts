import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  // The endorsement in 20240128171202_MoreLostExportedKeys was one endorsement
  // too early - I looked at
  // https://api.eventdata.crossref.org/v1/events?mailto=playing_around@plaudit.pub&rows=6&source=plaudit&from-occurred-date=2023-12-20
  // but the events were not sorted by `occurred_at`, and so event 4 was the
  // last one, not event 5:
  const lostExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0003-2839-3144")
    .andWhere("pid", "doi:10.31235/osf.io/vrcdh");

  if (lostExportedEndorsement) {
    await knex.table("cross_ref_export").insert({
      exportedEndorsements: 1,
      version: 1,
      exportedUntilId: lostExportedEndorsement.id,
    });
  }

  const prevExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0001-7140-8933")
    .andWhere("pid", "doi:10.31234/osf.io/ta7m8");
  if (prevExportedEndorsement) {
    await knex
      .table("cross_ref_export")
      .update("exportedEndorsements", 391)
      .where("exportedUntilId", prevExportedEndorsement.id)
      .andWhere("exportedEndorsements", 392);
  }
}

export async function down(knex: Knex): Promise<void> {
  const lostExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0003-2839-3144")
    .andWhere("pid", "doi:10.31235/osf.io/vrcdh");
  if (lostExportedEndorsement) {
    await knex
      .table("cross_ref_export")
      .delete()
      .where("exportedUntilId", lostExportedEndorsement.id);
  }

  const prevExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0001-7140-8933")
    .andWhere("pid", "doi:10.31234/osf.io/ta7m8");
  if (prevExportedEndorsement) {
    await knex
      .table("cross_ref_export")
      .update("exportedEndorsements", 392)
      .where("exportedUntilId", prevExportedEndorsement.id)
      .andWhere("exportedEndorsements", 391);
  }
}
