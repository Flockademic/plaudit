import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("orcid_account", (table) => {
    table.string("orcid", 19).primary();
    table.integer("userId").notNullable();
    table.string("refreshToken").notNullable();
    table.string("accessToken").notNullable();
    table.bigInteger("expiresAt").notNullable();
    table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
    table.dateTime("updatedAt").notNullable().defaultTo(knex.fn.now());
    table.integer("version").notNullable().defaultTo(1);
  });
  await knex.schema.table("endorsement", (table) => {
    table.dropForeign("userOrcid");
  }),
    await knex.schema.table("retracted_endorsement", (table) => {
      table.dropForeign("endorsementUserOrcid");
    }),
    await knex.schema.alterTable("user", (table) => {
      table.dropPrimary();
    });
  await knex.schema.alterTable("user", (table) => {
    table.increments("id").primary();
    table.string("email").nullable();
  });
  await knex.schema.alterTable("orcid_account", (table) => {
    table.foreign("userId").references("user.id").onDelete("CASCADE");
  });
  await knex
    // Insert into the `orcid` table...
    .into(
      knex.raw(":tableName: (:col1:, :col2:, :col3:, :col4:, :col5:)", {
        tableName: "orcid_account",
        col1: "orcid",
        col2: "userId",
        col3: "refreshToken",
        col4: "accessToken",
        col5: "expiresAt",
      }),
    )
    // ...values taken from the `user` table:
    .insert(
      knex
        .select(
          "orcid",
          "id as userId",
          knex.raw(":value AS :alias:", {
            value: "Unset after migration; will be set after login",
            alias: "refreshToken",
          }),
          knex.raw(":value AS :alias:", {
            value: "Unset after migration; will be set after login",
            alias: "accessToken",
          }),
          knex.raw(":value AS :alias:", { value: 0, alias: "expiresAt" }),
        )
        .from("user"),
    );
  await knex.schema.table("endorsement", (table) => {
    table
      .foreign("userOrcid")
      .references("orcid_account.orcid")
      .onDelete("CASCADE");
  }),
    await knex.schema.table("retracted_endorsement", (table) => {
      table
        .foreign("endorsementUserOrcid")
        .references("orcid_account.orcid")
        .onDelete("CASCADE");
    }),
    await knex.schema.alterTable("user", (table) => {
      table.dropColumn("orcid");
    });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("user", (table) => {
    table.string("orcid", 19);
  });
  await knex.raw(
    "UPDATE :updateTable: SET :updateColumn: = :selectColumn: FROM :selectTable: WHERE :updateMatchColumn: = :selectMatchColumn:",
    {
      updateTable: "user",
      updateColumn: "orcid",
      selectTable: "orcid_account",
      selectColumn: "orcid_account.orcid",
      updateMatchColumn: "user.id",
      selectMatchColumn: "orcid_account.userId",
    },
  );
  await knex.schema.table("endorsement", (table) => {
    table.dropForeign("userOrcid");
  }),
    await knex.schema.table("retracted_endorsement", (table) => {
      table.dropForeign("endorsementUserOrcid");
    }),
    await knex.schema.alterTable("orcid_account", (table) => {
      table.dropForeign("userId");
    });
  await knex.schema.alterTable("user", (table) => {
    table.dropPrimary();
  });
  await knex.schema.alterTable("user", (table) => {
    table.primary(["orcid"]);
  });
  await knex.schema.table("endorsement", (table) => {
    table.foreign("userOrcid").references("user.orcid").onDelete("CASCADE");
  }),
    await knex.schema.table("retracted_endorsement", (table) => {
      table
        .foreign("endorsementUserOrcid")
        .references("user.orcid")
        .onDelete("CASCADE");
    }),
    await knex.schema.dropTableIfExists("orcid_account");
  await knex.schema.alterTable("user", (table) => {
    table.dropColumn("id");
    table.dropColumn("email");
  });
}
