import type { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  // Unfortunately Knex doesn't support altering enums. We could do a raw query,
  // but since our DB isn't particularly huge, we can probably also get away
  // with the inefficient but more readable "create a new column, copy over the
  // values from the old one, then delete the old one" strategy.
  // See https://knexjs.org/guide/schema-builder.html#enum-enu
  await knex.schema.alterTable("endorsement", async (table) => {
    await table.enum("source_new", ["widget", "website", "scielo"])
      .notNullable()
      .defaultTo("widget");
  });
  await knex.raw(`
    UPDATE endorsement
    SET source_new = CASE
      WHEN source = 'plaudit' THEN 'widget'
      ELSE source
    END;
  `);
  await knex.schema.alterTable("endorsement", async (table) => {
    await table.dropColumn("source");
  });
  await knex.schema.alterTable("endorsement", async (table) => {
    await table.renameColumn("source_new", "source");
  });
}

/**
 * Warning: reverting this migration is lossy! Endorsements made via the website
 *          will be counted as made via the widget after this migration.
 */
export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("endorsement", async (table) => {
    await table.enum("source_old", ["plaudit", "scielo"])
      .notNullable()
      .defaultTo("plaudit");
  });
  await knex.raw(`
    UPDATE endorsement
    SET source_old = CASE
      WHEN source = 'widget' THEN 'plaudit'
      WHEN source = 'website' THEN 'plaudit'
      ELSE source
    END;
  `);
  await knex.schema.alterTable("endorsement", async (table) => {
    table.dropColumn("source");
  });
  await knex.schema.alterTable("endorsement", async (table) => {
    table.renameColumn("source_old", "source");
  });
}
