import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  // Something went wrong with a CrossRef export at some point. See
  // https://api.eventdata.crossref.org/v1/events?mailto=playing_around@plaudit.pub&rows=3&source=plaudit&from-collected-date=2023-08-01
  // At the time of writing this migration, event 0 (0000-0003-2947-1386
  // endorsing 10.31235/osf.io/vcmf2) was the last recorded endorsement in the
  // cross_ref_export table. That means that the next endorsement to be exported
  // (0000-0002-5942-3273 endorsing 10.31234/osf.io/4uzjs) was already known at
  // CrossRef (event 1 at the above link), causing a a 409 Conflict.
  // Additionally, event 2 (0000-0002-2329-0333 endorsing 10.31234/osf.io/gra6s)
  // isn't known at all on the Plaudit side, not even among retracted
  // endorsements — presumably, that is what caused the export to not be
  // recorded in the first place. According to
  // https://gitlab.com/Flockademic/plaudit/-/jobs/4783560159, that endorsement
  // had an ID of 7412, which appears to be a different endorsement.
  // In any case, even though it's not clear where event 2 came from, to make
  // exports work again, this migration records that event 1 has already been
  // exported, so that the next migration will then continue with the next
  // endorsement.
  const lostExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0002-5942-3273")
    .andWhere("pid", "doi:10.31234/osf.io/4uzjs");

  if (lostExportedEndorsement) {
    await knex.table("cross_ref_export").insert({
      exportedEndorsements: 1,
      version: 1,
      exportedUntilId: lostExportedEndorsement.id,
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  const lostExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0002-5942-3273")
    .andWhere("pid", "doi:10.31234/osf.io/4uzjs");
  if (lostExportedEndorsement) {
    await knex
      .table("cross_ref_export")
      .delete()
      .where("exportedUntilId", lostExportedEndorsement.id);
  }
}
