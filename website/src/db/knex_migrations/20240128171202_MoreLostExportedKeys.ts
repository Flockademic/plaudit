import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  // After the migration from 20240128144702_InsertLostExportKey,
  // endorsements up to and including the one below were sent to CrossRef, and
  // then another error occurred, and the export did not get recorded for some
  // reason. Let's resume exporting the rest, and then inspect the error message
  // to see why it breaks down.
  const lostExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0001-7140-8933")
    .andWhere("pid", "doi:10.31234/osf.io/ta7m8");

  if (lostExportedEndorsement) {
    await knex.table("cross_ref_export").insert({
      exportedEndorsements: 392,
      version: 1,
      exportedUntilId: lostExportedEndorsement.id,
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  const lostExportedEndorsement = await knex
    .table("endorsement")
    .first("*")
    .where("userOrcid", "0000-0001-7140-8933")
    .andWhere("pid", "doi:10.31234/osf.io/ta7m8");
  if (lostExportedEndorsement) {
    await knex
      .table("cross_ref_export")
      .delete()
      .where("exportedUntilId", lostExportedEndorsement.id);
  }
}
