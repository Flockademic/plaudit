import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const createTableIfNotExists = async (
    ...args: Parameters<typeof knex.schema.createTable>
  ) => {
    const exists = await knex.schema.hasTable(args[0]);
    if (!exists) {
      return knex.schema.createTable(...args);
    }
  };

  await Promise.all([
    createTableIfNotExists("user", (table) => {
      table.string("orcid", 19).primary();
      table.string("name").nullable();
      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
      table.dateTime("updatedAt").notNullable().defaultTo(knex.fn.now());
      table.integer("version").notNullable().defaultTo(1);
    }),
    createTableIfNotExists("work_data", (table) => {
      table.string("pid").primary();
      table.text("url").notNullable();
      table.text("title").notNullable();
      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
      table.dateTime("updatedAt").notNullable().defaultTo(knex.fn.now());
      table.integer("version").notNullable().defaultTo(1);
    }),
    knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"'),
    createTableIfNotExists("endorsement", (table) => {
      table.increments("id").primary();
      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
      table.dateTime("updatedAt").notNullable().defaultTo(knex.fn.now());
      table.integer("version").notNullable().defaultTo(1);
      table.string("userOrcid", 19).notNullable();
      table.string("pid").notNullable();
      table
        .uuid("uuid")
        .notNullable()
        .defaultTo(knex.raw("uuid_generate_v4()"));
      table
        .enum("source", ["plaudit", "scielo"])
        .notNullable()
        .defaultTo("plaudit");
      table.unique(["userOrcid", "pid"]);
    }),
    createTableIfNotExists("tag", (table) => {
      table.increments("id").primary();
      table.string("tag").unique().notNullable();
      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
      table.dateTime("updatedAt").notNullable().defaultTo(knex.fn.now());
      table.integer("version").notNullable().defaultTo(1);
    }),
    createTableIfNotExists("endorsement_tags_tag", (table) => {
      table.integer("endorsementId").notNullable();
      table.integer("tagId").notNullable();
      table.unique(["endorsementId", "tagId"]);
    }),
    createTableIfNotExists("session", (table) => {
      table.string("sid").primary();
      table.dateTime("expires").nullable();
      table.json("data").notNullable();
      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
      table.dateTime("updatedAt").notNullable().defaultTo(knex.fn.now());
    }),
    createTableIfNotExists("retracted_endorsement", (table) => {
      table.increments("id").primary();
      table.enum("source", ["admin", "undo", "retraction"]).notNullable();
      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
      table.dateTime("updatedAt").notNullable().defaultTo(knex.fn.now());
      table.integer("version").notNullable().defaultTo(1);

      table.integer("endorsementId");
      table.dateTime("endorsementCreatedAt").notNullable();
      table.dateTime("endorsementUpdatedAt").notNullable();
      table.integer("endorsementVersion").notNullable();
      // In the previous schema, some values were accidentally left nullable.
      // Will set this to nonNullable() in a follow-up migration, after
      // restoring the backup.
      table.string("endorsementUserOrcid", 19).nullable();
      table.string("endorsementPid").notNullable();
      // This was accidentally added in the previous schema;
      // will remove in the next job
      table.string("pid").nullable();
      table.uuid("endorsementUuid").notNullable();
      table
        .enum("endorsementSource", ["plaudit", "scielo"])
        .notNullable()
        .defaultTo("plaudit");
      table.index(
        ["endorsementPid", "endorsementUserOrcid"],
        "idx_endorsementPid_endorsementUserOrcid",
      );
    }),
    createTableIfNotExists(
      "retracted_endorsement_endorsement_tags_tag",
      (table) => {
        table.integer("retractedEndorsementId").notNullable();
        table.integer("tagId").notNullable();
      },
    ),
    createTableIfNotExists("cross_ref_export", (table) => {
      table.increments("id").primary();
      table.integer("exportedEndorsements").notNullable();
      table.integer("exportedUntilId").nullable();
      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
      table.dateTime("updatedAt").notNullable().defaultTo(knex.fn.now());
      table.integer("version").notNullable().defaultTo(1);
    }),
  ]);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("cross_ref_export");
  await knex.schema.dropTableIfExists(
    "retracted_endorsement_endorsement_tags_tag",
  );
  await knex.schema.dropTableIfExists("retracted_endorsement");
  await knex.schema.dropTableIfExists("session");
  await knex.schema.dropTableIfExists("endorsement_tags_tag");
  await knex.schema.dropTableIfExists("tag");
  await knex.schema.dropTableIfExists("endorsement");
  await knex.schema.raw('DROP EXTENSION IF EXISTS "uuid-ossp"'),
    await knex.schema.dropTableIfExists("work_data");
  await knex.schema.dropTableIfExists("user");
}
