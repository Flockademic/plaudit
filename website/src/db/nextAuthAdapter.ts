import { OrcidId } from "knex/types/tables";
import { Adapter } from "next-auth/adapters";
import { getConnection } from "../backend/database";

export function getNextAuthAdapter(): Adapter {
  return {
    async createUser(user: { email: string }) {
      const [createdUser] = await (await getConnection())("user")
        .insert({
          version: 1,
          email: user.email,
          name: getName(user) ?? undefined,
        })
        .returning("*");

      return {
        id: createdUser.id.toString(),
        email:
          createdUser.email ??
          `${createdUser.id.toString()}@unknownemailaddress.plaudit.pub`,
        emailVerified: null,
        name: createdUser.name,
      };
    },
    async getUser(id) {
      const user = await (await getConnection())("user")
        .where({ id: Number.parseInt(id, 10) })
        .first();

      if (!user) {
        return null;
      }

      return {
        id: user.id.toString(),
        email:
          user.email ?? `${user.id.toString()}@notyetretrieved.plaudit.pub`,
        emailVerified: null,
        name: user.name,
      };
    },
    async getUserByEmail(email) {
      const user = await (await getConnection())("user").where({ email: email }).first();

      if (!user) {
        return null;
      }

      return {
        id: user.id.toString(),
        email:
          user.email ?? `${user.id.toString()}@notyetretrieved.plaudit.pub`,
        emailVerified: null,
        name: user.name,
      };
    },
    async getUserByAccount({ providerAccountId, provider }) {
      const user = await (await getConnection())("user")
        .select("*")
        .innerJoin("orcid_account", "orcid_account.userId", "user.id")
        .where("orcid_account.orcid", providerAccountId)
        .first();

      if (!user) {
        return null;
      }

      return {
        id: user.id.toString(),
        email:
          user.email ?? `${user.id.toString()}@notyetretrieved.plaudit.pub`,
        emailVerified: null,
        name: user.name,
      };
    },
    async updateUser(user) {
      const connection = await getConnection();
      const [updatedUser] = await connection("user")
        .update({
          name: user.name ?? undefined,
          updatedAt: connection.fn.now() as unknown as Date,
          version: connection.raw("version + 1") as unknown as number,
        })
        .where("id", user.id)
        .returning("*");

      return {
        id: updatedUser.id.toString(),
        email:
          updatedUser.email ??
          `${user.id.toString()}@notyetretrieved.plaudit.pub`,
        emailVerified: null,
        name: updatedUser.name,
      };
    },
    async deleteUser(userId) {
      throw new Error(
        "Deleting users not supported, both by Plaudit's NextAuthAdapter, as well as by Next-Auth, at the time of writing.",
      );
    },
    async linkAccount(account: {
      userId: `${number}`,
      providerAccountId: string,
      access_token: string,
      refresh_token: string,
      expires_at: number,
    }) {
      // TODO: Check that the ORCID iD is valid?
      await (await getConnection())("orcid_account").insert({
        userId: Number.parseInt(account.userId, 10),
        orcid: account.providerAccountId as OrcidId,
        accessToken: account.access_token!,
        refreshToken: account.refresh_token!,
        expiresAt: account.expires_at!,
        version: 1,
      });
      return;
    },
    async unlinkAccount({ }) {
      throw new Error(
        "Deleting users not supported, both by Plaudit's NextAuthAdapter, as well as by Next-Auth, at the time of writing.",
      );
    },
    async createSession({ sessionToken, userId, expires }) {
      throw new Error(
        "Database storage for sessions not supported. Set `session.strategy` to `jwt` in Next-Auth's AuthOptions.",
      );
    },
    async getSessionAndUser(sessionToken) {
      throw new Error(
        "Database storage for sessions not supported. Set `session.strategy` to `jwt` in Next-Auth's AuthOptions.",
      );
    },
    async updateSession({ sessionToken }) {
      throw new Error(
        "Database storage for sessions not supported. Set `session.strategy` to `jwt` in Next-Auth's AuthOptions.",
      );
    },
    async deleteSession(sessionToken) {
      throw new Error(
        "Database storage for sessions not supported. Set `session.strategy` to `jwt` in Next-Auth's AuthOptions.",
      );
    },
    async createVerificationToken({ identifier, expires, token }) {
      throw new Error("Passwordless signin not supported.");
    },
    async useVerificationToken({ identifier, token }) {
      throw new Error("Passwordless signin not supported.");
    },
  };
}

function getName(user: any): string | void {
  if (typeof user !== "object" || user === null) {
    return;
  }
  if (user.name) {
    return user.name;
  }
  if (user.family_name) {
    if (user.given_name) {
      return `${user.given_name} ${user.family_name}`;
    }
    return user.family_name;
  }
  if (user.given_name) {
    return user.given_name;
  }
  return;
}
