/**
 * Downsize all-caps names
 *
 * Many names in ORCID are in all-caps. When a string passed to this function is
 * found to be all-caps, this lower-cases every part of it except the first
 * letter of every (hyphenated) word.
 */
export function formatName(name: string): string {
  if (name.toLocaleUpperCase() === name) {
    return name
      .split(" ")
      .map((singleName) => {
        return singleName.split("-").map(capitalise).join("-");
      })
      .join(" ");
  }

  return name;
}

function capitalise(name: string): string {
  const firstCodePoint = name.codePointAt(0);
  if (!firstCodePoint) {
    return name;
  }
  const firstCharacter = String.fromCodePoint(firstCodePoint);
  const otherCharacters = name.substring(firstCharacter.length);
  return (
    firstCharacter.toLocaleUpperCase() + otherCharacters.toLocaleLowerCase()
  );
}
