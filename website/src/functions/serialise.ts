import { Session } from "next-auth";

export function serialiseSession(session?: Session | null) {
  if (session?.user) {
    if (!session.user.given_name) {
      delete session.user.given_name;
    }
    if (!session.user.family_name) {
      delete session.user.family_name;
    }
  }
  return session;
}
