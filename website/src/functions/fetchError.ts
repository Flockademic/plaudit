export class FetchError extends Error {
  public status: number;
  public statusText: string;

  constructor(message: string, response: Response) {
    super(message);

    this.status = response.status;
    this.statusText = response.statusText;
  }
}

export function isFetchError(value: unknown): value is FetchError {
  return value instanceof Error && typeof (value as FetchError).status === "number";
}
