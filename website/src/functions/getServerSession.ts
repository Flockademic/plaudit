import "server-only";
import { getServerSession as ogGetServerSession } from "next-auth";
import { authOptions } from "../app/api/auth/[...nextauth]/authOptions";

export function getServerSession() {
  return ogGetServerSession(authOptions);
}
