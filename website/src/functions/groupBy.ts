export function groupBy<T, K extends string>(
  arr: T[],
  selector: (item: T) => K,
): Record<K, T[]> {
  const result = {} as Record<K, T[]>;
  arr.forEach((item) => {
    const key = selector(item);
    if (!key) {
      return;
    }
    result[key] ??= [];
    result[key].push(item);
  });
  return result;
}
