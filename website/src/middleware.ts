import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export const config = {
  matcher: "/widget",
};

const scieloDomains = [
  "scielo.org",
  "www.scielo.org",
  "www.scielo.org.ar",
  "www.scielo.org.bo",
  "scielo.org.bo",
  "www.scielo.br",
  "scielo.br",
  "scielo.conicyt.cl",
  "www.scielo.sa.cr",
  "scielo.sld.cu",
  "www.scielo.sld.cu",
  "www.scielo.org.mx",
  "scielo.org.mx",
  "scielo.iics.una.py",
  "www.scielo.org.pe",
  "www.scielo.mec.pt",
  "scielo.mec.pt",
  "www.scielosp.org",
  "scielosp.org",
  "www.scielo.org.za",
  "scielo.isciii.es",
  "www.scielo.edu.uy",
  "scielo.edu.uy",
  "scielo.senescyt.gob.ec",
  "ve.scielo.org",
  "westindies.scielo.org",
  "books.scielo.org",
  "cienciaecultura.bvs.br",
];
const osfDomains = [
  "https://osf.io",
  "https://agrixiv.org",
  "https://arabixiv.org",
  "https://eartharxiv.org",
  "https://ecoevorxiv.org",
  "https://ecsarxiv.org",
  "https://edarxiv.org",
  "https://engrxiv.org",
  "https://frenxiv.org",
  "https://indiarxiv.org",
  "https://marxiv.org",
  "https://mediarxiv.org",
  "https://mindrxiv.org",
  "https://paleorxiv.org",
  "https://psyarxiv.com",
  "https://researchaz.org",
  "https://thesiscommons.org",
];
const maadRayanDomains = [
  "https://herbmedpharmacol.com",
  "https://nephropathol.com",
  "https://journalrip.com",
  "https://jnephropharmacology.com",
  "https://immunopathol.com",
  "https://jprevepi.com",
  "https://aimjournal.ir",
  "https://enterpathog.abzums.ac.ir",
  "https://ijbsm.zbmu.ac.ir",
  "https://ajcmi.umsha.ac.ir",
  "https://ajdr.umsha.ac.ir",
  "https://ajehe.umsha.ac.ir",
  "https://ajmb.umsha.ac.ir",
  "https://ajpr.umsha.ac.ir",
  "https://apb.tbzmed.ac.ir",
  "https://bi.tbzmed.ac.ir",
  "https://hpp.tbzmed.ac.ir",
  "https://jrcm.tbzmed.ac.ir",
  "https://jcs.tbzmed.ac.ir",
  "https://jcvtr.tbzmed.ac.ir",
  "https://joddd.tbzmed.ac.ir",
  "https://japid.tbzmed.ac.ir",
  "https://ps.tbzmed.ac.ir",
  "https://rdme.tbzmed.ac.ir",
  "https://iejm.hums.ac.ir",
  "https://j.skums.ac.ir",
  "https://ijpni.org",
  "https://ijmpes.com",
];
const embedderDomains: { [embedder_id: string]: string[] } = {
  library_carpentry: ["https://librarycarpentry.org"],
  prereview: [
    "https://beta.prereview.org",
    "https://v2.prereview.org",
    "https://www.prereview.org",
    "https://prereview.org",
  ],
  osf_preprints: [
    ...osfDomains,
    "https://staging.osf.io",
    "https://staging2.osf.io",
    "https://staging3.osf.io",
    "https://test.osf.io",
    "http://localhost:5000",
  ],
  essoar: [
    "https://esspp-test.literatumonline.com",
    "https://esspp-stag.literatumonline.com",
    "https://esspp-prod.literatumonline.com",
    "https://www.essoar.org",
  ],
  scipost: ["https://scipost.org", "https://www.scipost.org"],
  poltekkes: [
    "https://myjurnal.poltekkes-kdi.ac.id",
    "https://www.myjurnal.poltekkes-kdi.ac.id",
  ],
  maadrayan: maadRayanDomains,
  abralin: [
    "https://cadernos.abralin.org",
    "https://revista.abralin.org",
    "https://www.roseta.org.br",
  ],
  fiocruz: ["https://revistafitos.far.fiocruz.br"],
  psychopen: ["https://psychopen.eu", "https://*.psychopen.eu"],
  revistadaanpoll: ["https://revistadaanpoll.emnuvens.com.br"],
  rhd: ["https://revistas.uned.es"],
  "scielo-preprints": [
    "https://preprints.scielo.org",
    "https://homolog-preprints.scielo.org",
  ],
  rbmfc: ["https://rbmfc.org.br", "https://www.rbmfc.org.br"],
  bjbabs: ["https://bjbabs.org", "https://www.bjbabs.org"],
  rudea: ["https://revistas.udea.edu.co"],
  uoli: ["https://journals-uoli.com"],
  professionalsonline: [
    "https://journalalphacentauri.com",
    "https://www.journalalphacentauri.com",
    "https://editorialfondo.com",
    "https://www.editorialfondo.com",
  ],
  aijr: [
    "https://journals.aijr.org",
    "https://books.aijr.org",
    "https://preprints.aijr.org",
  ],
  ijcrsee: ["https://ijcrsee.com", "https://www.ijcrsee.com"],
  sachetas: ["https://sachetas.in", "https://www.sachetas.in"],
  egyhaztortenetiszemle: [
    "https://egyhaztortenetiszemle.hu",
    "https://www.egyhaztortenetiszemle.hu",
  ],
  icapsr: ["https://journals.icapsr.com"],
  revistardp: ["https://revistardp.org.br"],
  revistaalergia: ["https://revistaalergia.mx"],
  // Stichting Open Access platforms, https://openaccess.ac
  soap: [
    "https://bluepapers.nl",
    "https://bulletin.knob.nl",
    "https://cubicjournal.org",
    "https://docomomojournal.com",
    "https://jfde.eu",
    "https://overholland.ac",
    "https://journals.aesop-planning.eu",
    "https://projectbaikal.com",
    "https://rius.ac",
    "https://spool.ac",
    "https://transactions-journal.aesop-planning.eu",
  ],
  chapingo: ["https://revistas.chapingo.mx"],
  malayajournal: ["https://www.malayajournal.org"],
  hscience: ["https://hscience.org", "https://www.hscience.org"],
  aebmedicine: ["https://aebmedicine.com", "https://www.aebmedicine.com"],
  belitungraya: ["https://www.belitungraya.org"],
  thesciencebrigade: ["https://thesciencebrigade.com"],
  recreationcentral: [
    "https://journal.recreationcentral.eu",
    "https://www.journal.recreationcentral.eu",
  ],
  wgu: ["https://journals.wgu.edu.et"],
  iistr: ["https://journal.iistr.org"],
  immi: [
    "https://immi.se",
    "https://www.immi.se",
    "https://ijsser.com",
    "https://www.ijsser.com",
  ],
  ikippgriptk: ["https://jurnal.jsa.ikippgriptk.ac.id"],
  pucv: [
    "https://www.rdpucv.cl",
    "https://rdpucv.cl",
    "https://www.rehj.cl",
    "https://rehj.cl",
    "https://www.psicoperspectivas.cl",
    "https://psicoperspectivas.cl",
    "https://revistasignos.cl",
    "https://www.revistasignos.cl",
    "https://www.lajar.cl",
    "https://lajar.cl",
    "https://historia396.cl",
    "https://www.historia396.cl",
    "https://historia396.cl",
    "https://andeangeology.cl",
    "https://www.andeangeology.cl",
    "https://ejbiotechnology.info",
    "https://www.ejbiotechnology.info",
    "https://perspectivaeducacional.cl",
    "https://www.perspectivaeducacional.cl",
    "http://actoyforma.ucv.cl",
    "http://www.actoyforma.ucv.cl",
    "https://revistageografica.cl",
    "https://www.revistageografica.cl",
  ],
  thejas: [
    "https://www.thejas.com.pk",
    "https://thejas.com.pk",
    "https://www.thetherapist.com.pk",
    "https://thetherapist.com.pk",
    "https://www.markhorjournal.com",
    "https://markhorjournal.com",
    "https://www.pakistanbmj.com",
    "https://pakistanbmj.com",
    "https://www.nursearcher.com",
    "https://fbtjournal.com",
    "https://www.dietfactor.com.pk",
    "https://dietfactor.com.pk",
  ],
  ibia: ["https://ojs.ibia.mg.gov.br"],
  kmanpub: [
    "https://journals.kmanpub.com"
  ],
  pagepress: [
    "https://www.italjmed.org",
    "https://btvb.org",
    "https://pagepressjournals.org",
    "https://www.pagepressjournals.org",
  ],
  qauj: [
    "https://journal.qau.edu.ye",
  ],
  socratesjournal: [
    "https://www.socratesjournal.com",
  ],
  sljm: [
    "https://sljm.org",
  ],
  iirj: [
    "https://researth.iars.info",
  ],
  nnpub: [
    "https://ca-c.org",
    "https://cibgp.com",
    "https://turcomat.org",
    "https://www.turcomat.org",
  ],
  bonfire: [
    "https://bonfire-demo.tunnelto.dev",
    "https://openscience.network",
  ],
  ijaaeb: [
    "https://submissions.eminentscientists.com",
  ],
  inschool: [
    "https://publications.inschool.id",
  ],
  tjes: [
    "https://www.tj-es.com",
    "https://tj-es.com",
  ],
  ijcua: [
    "https://ijcua.com",
    "https://www.ijcua.com",
  ],
  bjmis: [
    "https://bjmis.du.ac.bd",
  ],
  rsyn: [
    "https://pubs.rsyn.org",
    "https://rsynresearch.org",
  ],
  imasj: [
    "https://imasj.com",
    "https://www.imasj.com",
  ],
  acad: [
    "https://acad.hu",
    "https://www.acad.hu",
  ],
  cims: [
    "https://cims.fti.dp.ua",
  ],
  keep_demo: [
    "https://demo.ojs.keep.pt",
  ],
  biocomjournal: [
    "https://biocomjournal.com",
  ],
  nzjabr: [
    "https://nzjabr.ac.nz",
    "https://www.nzjabr.ac.nz",
  ],
  web_ext: [
    ...osfDomains,
    ...scieloDomains,
    "https://www.essoar.org",
    "https://arxiv.org",
    "https://scipost.org",
    "https://f1000research.com",
    "https://wellcomeopenresearch.org",
    "https://gatesopenresearch.org",
    "https://mniopenresearch.org",
    "https://hrbopenresearch.org",
    "https://aasopenresearch.org",
    "https://amrcopenresearch.org",
    "https://isfopenresearch.org",
    "https://www.nature.com",
    "https://www.sciencedirect.com",
    "https://onlinelibrary.wiley.com",
    "https://*.onlinelibrary.wiley.com",
    "https://link.springer.com",
    "https://www.tandfonline.com",
    "https://academic.oup.com",
    "https://www.biorxiv.org",
    "https://www.medrxiv.org",
    "https://www.preprints.org",
    "https://www.zenodo.org",
    "https://zenodo.org",
    "https://www.mdpi.com",
    "https://mdpi.com",
    "https://www.ncbi.nlm.nih.gov",
    "https://www.pnas.org",
    "https://www.frontiersin.org",
    "https://journals.plos.org",
    "https://*.biomedcentral.com",
    "https://*.asm.org",
    "https://peerj.com",
    "https://*.pubpub.org",
    "https://collabra.org",
    "https://www.collabra.org",
    "https://www.cell.com",
    "https://papers.ssrn.com",
    "https://journals.sagepub.com",
    "https://openjournals.nl",
    "https://www.researchsquare.com",
    "https://repository.tudelft.nl",
    "moz-extension:",
    "chrome-extension:",
  ],
  arxiv: ["https://arxiv.org", "https://beta.arxiv.org"],
  flockademic: [
    "https://flockademic.com",
    "https://www.flockademic.com",
    "https://dev.flockademic.com",
  ],
  plaudit: [
    "https://plaudit.pub",
    process.env.APP_URL || "https://www.plaudit.pub",
  ],
};

// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
  let frameAncestors = "none";
  let frameOptions = "deny";

  const requestUrl = new URL(request.url);
  const embedderId = requestUrl.searchParams.get("embedder_id");

  if (
    typeof embedderId === "string" &&
    Array.isArray(embedderDomains[embedderId])
  ) {
    frameAncestors = embedderDomains[embedderId].join(" ");

    // Note: the only reason we add the X-Frame-Options header is because Internet Explorer
    //       does not support the Content-Security-Policy header. Unfortunately, it does not
    //       seem to be possible to add multiple domains for that header, which is why we check
    //       the Referer header for the domain to whitelist here. BUT: not having access to
    //       an installation of Internet Explorer, I have not been able to test whether it
    //       actually sets this header to the URL of the page that includes the iframe.
    //       Therefore, if the widget does not work in Internet Explorer, it might be better to
    //       remove X-Frame-Options and just live without the embed protection there.
    const referrer = request.headers.get("referer");
    if (typeof referrer === "string") {
      const referrerDomain = new URL(referrer).origin;
      const embedderToAllow = embedderDomains[embedderId].find(
        (domain) => domain === referrerDomain,
      );

      if (typeof embedderToAllow === "string") {
        frameOptions = `allow-from ${embedderToAllow}`;
      }
    }
  }

  const response = NextResponse.next();
  response.headers.set(
    "Content-Security-Policy",
    `frame-ancestors ${frameAncestors}`,
  );
  response.headers.set("X-Frame-Options", frameOptions);
  return response;
}
