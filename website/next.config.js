const { withSentryConfig } = require("@sentry/nextjs");

/** @type {import('next').NextConfig} */
const nextConfig = {
  // Prevent the following errors when calling out to Knex.js:
  //     Module not found: Can't resolve 'better-sqlite3'
  serverExternalPackages: ['commonjs', 'knex'],
};

module.exports = withSentryConfig(
  nextConfig,
  {
    // For all available options, see:
    // https://github.com/getsentry/sentry-webpack-plugin#options

    // Suppresses source map uploading logs during build
    silent: true,

    org: "plaudit",
    project: "javascript-nextjs",
  },
  {
    // For all available options, see:
    // https://docs.sentry.io/platforms/javascript/guides/nextjs/manual-setup/

    // Auth tokens can be obtained from https://sentry.io/settings/account/api/auth-tokens/
    // and need `project:releases` and `org:read` scopes
    authToken: process.env.SENTRY_AUTH_TOKEN,

    // Upload a larger set of source maps for prettier stack traces (increases build time)
    widenClientFileUpload: true,

    // Transpiles SDK to be compatible with IE11 (increases bundle size)
    transpileClientSDK: false,

    // Routes browser requests to Sentry through a Next.js rewrite to circumvent ad-blockers (increases server load)
    // Disabled; if people would prefer us not detecting errors they encounter,
    // it's probably fine to respect that - we'll probably still be notified of
    // it if others run into it.
    // tunnelRoute: "/monitoring",

    // Hides source maps from generated client bundles
    hideSourceMaps: false,

    // Automatically tree-shake Sentry logger statements to reduce bundle size
    disableLogger: true,
  }
);
