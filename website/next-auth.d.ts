import { DefaultSession } from "next-auth";
import { OrcidId } from "knex/types/tables";

declare module "next-auth" {
  /** The OAuth profile provided by ORCID */
  interface Profile {
    at_hash: string;
    aud: `APP-${string}`;
    sub: OrcidId;
    auth_time: number;
    iss: 'https://sandbox.orcid.org' | 'https://orcid.org';
    exp: number;
    given_name?: string;
    iat: number;
    family_name?: string;
    jti: string;
  }

  interface Session {
    user: {
      id: OrcidId;
      given_name?: string;
      family_name?: string;
    } & DefaultSession["user"];
  }
}

declare module "next-auth/jwt" {
  /** Returned by the `jwt` callback and `getToken`, when using JWT sessions */
  interface JWT {
    id: OrcidId;
    given_name?: string;
    family_name?: string;
  }
}
