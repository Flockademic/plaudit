module.exports = {
    "env": {
        "browser": true,
        "node": true
    },
    "extends": [
        "next",
        "next/core-web-vitals",
        "prettier",
    ],
    "settings": {
        "next": {
            "rootDir": "./"
        }
    },
    "overrides": [
        {
            "env": {
                "node": true
            },
            "files": [
                ".eslintrc.{js,cjs}"
            ],
            "parserOptions": {
                "sourceType": "script"
            }
        }
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "@typescript-eslint",
    ],
    "rules": {
        // TODO: Adopt react-icons and re-enable:
        "@next/next/no-img-element": "off",
    }
}
