import { test, expect } from '@playwright/test';

test('should show a 404 page on an unknown URL, from which you should be able to get back to the home page', async ({ page }) => {
  await page.goto('/page-that-does-not-exist/?utm_source=gitlab.com&utm_medium=ci&utm_campaign=integration_tests');

  await expect(await page.url()).toMatch("page-that-does-not-exist");

  const heading = await page.getByRole("heading", { name: "Page not found" });
  expect(heading).toBeInViewport();

  const footer = await page.getByRole("contentinfo");
  const homeLink = await footer.getByRole("link", { name: "Plaudit" });
  await homeLink.click();

  await page.waitForLoadState("networkidle");

  expect(await page.url()).not.toMatch("page-that-does-not-exist");
});

test('should provide integration instructions to those that know the link', async ({ page }) => {
  await page.goto('/integration/?utm_source=gitlab.com&utm_medium=ci&utm_campaign=integration_tests');

  const codeSnippet = await page.getByRole("code").filter({ hasText: "<script async" });
  await expect(codeSnippet).toBeVisible();
});
