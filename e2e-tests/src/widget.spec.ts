import { test, expect } from '@playwright/test';

test('should open ORCID when clicked', async ({ page, context }) => {
  await page.goto('/widget/?pid=doi:10.1101/318345');
  expect(context.pages()).toHaveLength(1);

  const endorseButton = await page.getByRole("button", { name: /Be the first to endorse/ });
  await endorseButton.click();

  const confirmButton = await page.getByRole("button", { name: "Submit your additional remarks" });
  const newPagePromise = context.waitForEvent("page");
  await confirmButton.click();

  const signInPage = await newPagePromise;
  await signInPage.waitForLoadState("networkidle");

  expect(context.pages()).toHaveLength(2);
  expect(signInPage.url()).toMatch(/https:\/\/(sandbox\.)?orcid.org\//);
});
